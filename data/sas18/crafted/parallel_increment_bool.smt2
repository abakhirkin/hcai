; Goal should be unreachable

(declare-rel P (Bool Int Int Int))
(declare-var x Int)
(declare-var y Int)
(declare-var z Int)
(declare-var b Bool)

(rule (P b 0 0 0))
(rule (=>
  (and (P b x y z) (< x 100))
  (P b
     (+ x 1)
     (ite b (+ y 1) y)
     (ite b z (+ z 1)))))
