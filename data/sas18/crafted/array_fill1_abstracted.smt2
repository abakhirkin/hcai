(declare-rel loop (Int Int Int Int)) ; n i k a[k]
(declare-rel write (Int Int Int Int)) ; n i k a[k]
(declare-rel incr (Int Int Int Int)) ; n i k a[k]
(declare-rel end (Int Int Int)) ; n k a[k]
(declare-rel error ())

(declare-var n Int)
(declare-var i Int)
(declare-var k Int)
(declare-var ak Int)

(rule
  (=> (and (<= 0 k) (< k n)) (loop n 0 k ak)))

(rule
  (=> (and (< i n) (loop n i k ak)) (write n i k ak)))

(rule
  (=> (and (distinct i k) (write n i k ak)) (incr n i k ak)))

(rule
  (=> (write n i k ak) (incr n i i 42)))

(rule
  (=> (incr n i k ak) (loop n (+ i 1) k ak)))

(rule
  (=> (and (>= i n) (loop n i k ak)) (end n k ak)))

(rule
  (=> (and (>= k 0) (< k n) (end n k ak) (distinct ak 42)) error))

(query error)
