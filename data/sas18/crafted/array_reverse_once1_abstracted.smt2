(declare-rel init (Int Int Int Int Int)) ; n k a[k] l a0[l]
(declare-rel loop (Int Int Int Int Int Int)) ; n i k a[k] l a0[l]
(declare-rel read1 (Int Int Int Int Int Int)) ; n i k a[k] l a0[l]
(declare-rel read2 (Int Int Int Int Int Int Int)) ; n i tmp1 k a[k] l a0[l]
(declare-rel write1 (Int Int Int Int Int Int Int Int)) ; n i tmp1 tmp2 k a[k] l a0[l]
(declare-rel write2 (Int Int Int Int Int Int Int)) ; n i tmp1 k a[k] l a0[l]
(declare-rel incr (Int Int Int Int Int Int)) ; n i k a[k] l a0[l]
(declare-rel end (Int Int Int Int Int)) ; n k a[k] l a0[l]
(declare-rel error ())

(declare-var n Int)
(declare-var i Int)
(declare-var j Int)
(declare-var k Int)
(declare-var ak Int)
(declare-var l Int)
(declare-var a0l Int)
(declare-var tmp1 Int)
(declare-var tmp2 Int)

(rule
  (=> (and (<= 0 k) (< k n))
      (init n k ak k ak)))

(rule
  (=> (and (<= 0 k) (< k n) (<= 0 l) (< l n) (distinct k l))
      (init n k ak l a0l)))

(rule
  (=> (init n k ak l a0l)
      (loop n 0 k ak l a0l)))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (< i j) (loop n i k ak l a0l))
        (read1 n i k ak l a0l))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (distinct i k)
             (read1 n i k ak l a0l)
             (read1 n i i tmp1 l a0l))
        (read2 n i tmp1 k ak l a0l))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (read1 n i i tmp1 l a0l)
        (read2 n i tmp1 i tmp1 l a0l))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (distinct j k)
             (read2 n i tmp1 k ak l a0l)
             (read2 n i tmp1 j tmp2 l a0l))
        (write1 n i tmp1 tmp2 k ak l a0l))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (read2 n i tmp1 j tmp2 l a0l))
        (write1 n i tmp1 tmp2 j tmp2 l a0l))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (write1 n i tmp1 tmp2 k ak l a0l) (distinct i k))
        (write2 n i tmp1 k ak l a0l))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (write1 n i tmp1 tmp2 i ak l a0l)
        (write2 n i tmp1 i tmp2 l a0l))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (write2 n i tmp1 k ak l a0l) (distinct j k))
        (incr n i k ak l a0l))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (write2 n i tmp1 j ak l a0l)
        (incr n i j tmp1 l a0l))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (incr n i k ak l a0l)
        (loop n (+ i 1) k ak l a0l))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (>= i j) (loop n i k ak l a0l))
        (end n k ak l a0l))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (>= i 0) (< i n) (end n i ak j a0l) (distinct ak a0l)) error)))

(query error)

;(assert (forall ((n Int) (i Int) (ai Int) (j Int) (a0j Int))
;  (not (end n i ai j a0j))))
 
