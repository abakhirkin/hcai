(declare-rel loop (Int Int Int Int Bool)) ; n i k a[k]
(declare-rel write (Int Int Int Int Bool)) ; n i k a[k]
(declare-rel incr (Int Int Int Int Bool)) ; n i k a[k]
(declare-rel end (Int Int Int Bool)) ; n k a[k]
(declare-rel error ())

(declare-var n Int)
(declare-var i Int)
(declare-var k Int)
(declare-var ak Int)
(declare-var b Bool)

(rule
  (=> (and (<= 0 k) (< k n)) (loop n 0 k ak false)))

(rule
  (=> (and (< i n) (loop n i k ak b)) (write n i k ak b)))

(rule
  (=> (and (not (= i k)) (write n i k ak b)) (incr n i k ak b)))

(rule
  (=> (write n i k ak b) (incr n i i 42 true)))

(rule
  (=> (incr n i k ak b) (loop n (+ i 1) k ak b)))

(rule
  (=> (and (>= i n) (loop n i k ak b)) (end n k ak b)))

(rule
  (=> (and (>= k 0) (< k n) (end n k ak b) (not (= ak 42))) error))

(query error)
