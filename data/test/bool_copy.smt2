; Goal should be reachable.

; For a naive path focusing, copying Boolean values may result in an exponential
; number of SMT queries, e.g., in this example it may enumerate all combinations
; of x and y.

(declare-rel P (Bool Bool))
(declare-rel Q (Bool Bool))

(declare-var x Bool)
(declare-var y Bool)

(rule (P x y))
(rule (=> (P x y) (Q x y)))

(query (Q true false))
