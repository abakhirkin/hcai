; A version of parallel increment that has multiplication in its syntax.
; Goal should be unreachable

(declare-rel P (Real Real))
(declare-var x Real)
(declare-var y Real)

(rule (P 0 0))
(rule (=>
  (and (P (* 3 x) (* 3 y)) (< (* 3 x) 100))
  (P (+ (* x 3) 1) (+ (* y 3) 1))))

(query (and (P x y) (distinct x y)))
