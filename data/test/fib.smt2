; Goal should be unreachable

(declare-rel F (Int Int))
(declare-rel Fc (Int))
(declare-rel E ())
(declare-var x Int)
(declare-var y Int)
(declare-var z Int)

(rule (Fc x))
(rule (=> (and (Fc x) (< x 1)) (F x 0)))
(rule (=> (and (Fc x) (= x 1)) (F x 1)))
(rule (=> (and (Fc x) (> x 1) (F (- x 1) y) (F (- x 2) z)) (F x (+ y z))))
(rule (=> (and (<= x 46) (F x y) (< y (- x 1))) E))

(query E)
