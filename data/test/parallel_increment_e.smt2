; Goal shold be "maybe reachable".

; A version of parallel_increment.smt2 with a separate error location
; that cannot currently be proven unreachable.

(declare-rel P (Int Int))
(declare-rel E ())
(declare-var x Int)
(declare-var y Int)

(rule (P 0 0))
(rule (=>
  (and (P x y) (distinct x 100))
  (P (+ x 1) (+ y 1))))
(rule (=> (and (P x y) (> x 200)) E))

(query E)
