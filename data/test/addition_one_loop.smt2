; Goal should be unreachable.

(declare-rel P (Int Int))
(declare-rel E ())
(declare-var x Int)
(declare-var y Int)

(rule (=> (= x 0) (P x y)))
(rule (=> (P x y) (P (+ x y) y)))
(rule (=> (and (P x y) (> x 0) (< y 0)) E))

(query E)

