; Goal should be unreachable.

; A version of parallel_inctement.smt2 with multiple predicates and a more precise goal.
(declare-rel P0 (Int Int))
(declare-rel P1 (Int Int))
(declare-rel P2 (Int Int))
(declare-rel P3 (Int Int))
(declare-rel P4 (Int Int))
(declare-rel P5 (Int Int))
(declare-var x Int)
(declare-var y Int)

(rule (P0 x y))
(rule (=> (P0 x y) (P1 0 0)))
(rule (=> (and (P1 x y) (<= x 99)) (P2 x y)))
(rule (=> (P2 x y) (P3 (+ x 1) y)))
(rule (=> (P3 x y) (P4 x (+ y 1))))
(rule (=> (P4 x y) (P1 x y)))
(rule (=> (and (P1 x y) (>= x 100)) (P5 x y)))

(query (and (P5 x y) (or (not (= x 100)) (not (= y 100)))))
