Basic Usage
===========

In the command line examples, we assume that you are in the root directory of 
the tool and you compiled the native version of the binary (hcai.native).

The tool accepts Horn clauses in the format of 
[SeaHorn](http://seahorn.github.io/).
To try and disprove reachability of the goal, run `./hcai.native file`, for 
example:
```bash
$ ./hcai.native examples/parallel_increment.smt2
P -> { [|-c!0+c!1=0; -c!0+100>=0; c!0>=0|] }
; Goals reachable: false
```

By default, the tool outputs the result of the last analysis. To output the 
whole iteration sequence, use the `-pf` option:
```bash
$ ./hcai.native -pf examples/parallel_increment.smt2
; Forward analysis w. narrowing stabilized:
P -> { [|-c!0+c!1=0; -c!0+100>=0; c!0>=0|] }
; Backward analysis w. narrowing stabilized:
P -> { [|-c!0+c!1=0; -c!0+100>=0; c!0>=0|] }
; Values stabilized.
; Goals reachable: false
```

The variables `c!0` and `c!1` were introduced by the analyzer and stand for the 
arguments of the predicate P. To see which variables correspond to the 
arguments of predicates, use `-ph` option:
```bash
$ ./hcai.native -ph examples/parallel_increment.smt2
; Predicates with their argument variables:
(P c!0 c!1)
...
```

By default, the tool runs forward analysis and then at most 2 alternations of 
backward and forward analysis (i.e., at most 5 analysis runs in total). To 
change the limit, use the `-ai <number>` option. For example, with
`-ai 4` option the tool will run at most 4 alternations of backward and forward 
analysis (at most analysis 9 runs in total), and with `-ai 0`, it will only run
forward analysis.

For the full list of options, run `./hcai.native -help`.

Input Format
------------

The tool accepts Horn clauses in the format of 
[SeaHorn](http://seahorn.github.io/), which is based on SMTLIB2.
To get an idea of the format, have a look at the few simple systems in 
`examples` folder.
For example, this is the contents of `examples/parallel_increment.smt2`:
```
(declare-rel P (Int Int))
(declare-var x Int)
(declare-var y Int)

(rule (P 0 0))
(rule (=>
  (and (P x y) (< x 100))
  (P (+ x 1) (+ y 1))))

(query (and (P x y) (distinct x y)))
```
The command `declare-rel` is used to declare an uninterpreted predicate. Here, 
we declare a predicate P with two integer arguments. To declare a predicate 
(say, Q) without arguments, we would write `(declare-rel Q ())`.

The command `declare-var` is used to declare a variable that is universally 
quantified in every clause.
Here, we declare two integer variables, x and y.

The command `rule` is used to declare a Horn clause.
Its argument can be:
1. An application of an uninterpreted predicate. For example, here,
   `(rule (P 0 0))` denotes the clause 'true => P(0, 0)'.
2. An implication, where the right-hand side is an application of an
   uninterpreted predicate and the left-hand side is a formula without
   uninterpreted predicates. For example, we could write the above clause as
   `(rule (=> (and (= x 0) (= y 0)) (P x y)))`, which denotes
   '∀ x y. (x = 0 ∧ y = 0) => P(x, y)'.
3. An implication, where both sides are applications of uninterpreted
   predicates. For example, to denote the formula '∀ x y. P(x, y) => Q(x, y)',
   we would write `(rule (=> (P x y) (Q x y)))`.
4. An implication where the right-hand side is an application of an
   uninterpreted predicate and the left-hand side is a conjunction, where every
   conjunct is either an application of an uninterpreted predicate or a formula
   without uninterpreted predicates.
   For example, `(rule (=> (and (P x y) (< x 100))(P (+ x 1) (+ y 1))))` denotes
   the formula '∀ x y. (P(x, y) ∧ x < 100) => P(x+1, y+1)'.
   
The command `query` is used to declare the goal, which we wish to prove
unreachable. Here, `(query (and (P x y) (distinct x y)))` denotes the formula
'∃ x y. P(x, y) ∧ x ≠ y', which we wish to prove unsatisfiable.