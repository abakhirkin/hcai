open ExceptionU
open Format

module Make
  (ClauseSet: ClauseSet.T)
  (Domain: Domains.T with module Solver = ClauseSet.Solver)
  (PrePostF: PrePosts.T)
  (FwdStrat: Strategies.T with type loc_t = ClauseSet.Pred.t)
  (BackwStrat: Strategies.T with type loc_t = ClauseSet.Pred.t) = struct  

  module Solver = ClauseSet.Solver
  module Pred = ClauseSet.Pred
  module PredHashtbl = Pred.Hashtbl  
  
  module Ctx = struct
    module ClauseSet = ClauseSet
    module Domain = Domain

    module PredData = struct
      type t = {
        t_pred: Pred.t;
        t_space: Domain.space_t
      }

      let domain pd = 
        pd.t_space

      let make pred space =
        { t_pred = pred; t_space = space }
    end

    type t = {
      t_clause_set: ClauseSet.t;
      t_domain: Domain.domain_t;
      t_fwd_strat: FwdStrat.strat_t;
      t_backw_strat: BackwStrat.strat_t;
      t_pred_data: PredData.t PredHashtbl.t
    }

    let clause_set ctx =
      ctx.t_clause_set

    let domain ctx =
      ctx.t_domain

    let solver ctx =
      ClauseSet.solver ctx.t_clause_set

    let pred_data ctx pred =
      PredHashtbl.find ctx.t_pred_data pred

    let pred_space ctx pred =
      PredData.domain (pred_data ctx pred)       

    (* TODO: This has to go somewhere. *)
    let mk_pred_domain clause_set dom pred =
      let solver = ClauseSet.solver clause_set in
      Domain.make_space
        dom
        (List.map (Solver.term_func_decl solver) (Pred.head_args pred))

    let make clause_set domain fwd_strat backw_strat =
      let pred_data = PredHashtbl.create (ClauseSet.pred_count clause_set) in
      ClauseSet.iter_preds
      (fun pred ->
        let dom = mk_pred_domain clause_set domain pred in
        PredHashtbl.replace pred_data pred (PredData.make pred dom))
      clause_set;
      { t_clause_set = clause_set;
        t_domain = domain;
        t_fwd_strat = fwd_strat;
        t_backw_strat = backw_strat;
        t_pred_data = pred_data }
  end

  module Run = struct
    module Ctx = Ctx
    module ClauseSet = ClauseSet
    module Domain = Domain

    module PredData = struct
      type t = {
        t_pred: Pred.t;
        t_space: Domain.space_t;
        mutable t_elem: Domain.elem_t;
        t_below_elem: Domain.elem_t
      }

      let make pred domain value below_elem = 
        { t_pred = pred; t_space = domain; t_elem = value; t_below_elem = below_elem }

      let pred pd =
        pd.t_pred

      let space pd = 
        pd.t_space

      let elem pd = 
        pd.t_elem

      let set_elem pd elem =
        pd.t_elem <- elem

      let below_elem pd =
        pd.t_below_elem
    end

    type t = {
      t_ctx: Ctx.t;    
      t_pred_data: PredData.t PredHashtbl.t    
    }

    let ctx run =
      run.t_ctx

    let pred_data run pred =
      PredHashtbl.find run.t_pred_data pred

    let domain run =
      Ctx.domain run.t_ctx

    let clause_set run =
      Ctx.clause_set run.t_ctx

    let solver run =
      Ctx.solver run.t_ctx

    let pred_elem run pred =
      PredData.elem (pred_data run pred)

    let set_pred_elem run pred elem =
      let pdata = pred_data run pred in
      PredData.set_elem pdata elem

    let pred_below_elem run pred =
      PredData.below_elem (pred_data run pred)

    let pred_space run pred =
      PredData.space (pred_data run pred)  

    let iter_pred_data fn run =
      PredHashtbl.iter (fun _ data -> fn data) run.t_pred_data 

    let make ctx pred_data =
      { t_ctx = ctx; t_pred_data = pred_data }   

    (* TODO: This has to go somewhere. *)
    let mk_pred_app_term run pred pred_args =    
      let solver = solver run in    
      let h_dom = pred_space run pred in
      let pred_elem = pred_elem run pred in
      let pred_term = Domain.term_of_elem h_dom pred_elem in
      Solver.substitute solver pred_term (Pred.head_args pred) pred_args
  end 

  module PrePost = PrePostF(Run) 

  module MakeFix
      (Strat: Strategies.T with type loc_t = Pred.t) = struct

    let update_with run wl pred cur_elem new_elem =
      let dom = Run.pred_space run pred in
      let below_elem = Run.pred_below_elem run pred in
      if Domain.leq dom new_elem cur_elem then begin
        Opt.do_if Opt.print_kleene (fun () ->
          eprintf "New value not greater, not updated@."
        );
        false
      end else begin
        let new_elem =
          if Strat.should_widen wl pred then begin
            Opt.do_if Opt.print_kleene (fun () ->
              eprintf "Applied widening@."
            );
            Domain.meet dom (Domain.widen dom cur_elem new_elem) below_elem
          end else begin
            Strat.did_not_widen wl pred;
            new_elem
          end
        in
        Run.set_pred_elem run pred new_elem;
        Opt.do_if Opt.print_kleene (fun () ->
          eprintf "Updated to: %a@." (Domain.format_elem dom) new_elem;
        );
        true
      end

    let join_with_trans pp run wl join_trans pred =            
      let cur_elem = Run.pred_elem run pred in
      Opt.do_if Opt.print_kleene (fun () ->
        let dom = Run.pred_space run pred in
        eprintf "Current value: %a@." (Domain.format_elem dom) cur_elem
      );      
      let new_elem = join_trans pp run pred cur_elem in
      update_with run wl pred cur_elem new_elem

    let replace_with_trans pp run join_trans pred =
      let dom = Run.pred_space run pred in
      let bot = Domain.mk_bot dom in
      let curr_elem = Run.pred_elem run pred in
      Opt.do_if Opt.print_kleene (fun () ->
        eprintf "Current value: %a@." (Domain.format_elem dom) curr_elem;
        eprintf "Computing a new value starting with bottom@."
      );
      let new_elem = join_trans pp run pred bot in
      if Domain.equal dom curr_elem new_elem then begin
        Opt.do_if Opt.print_kleene (fun () ->
          eprintf "Same value, not updated@."
        );
        false
      end else begin
        Opt.do_if Opt.print_kleene (fun () ->
          eprintf "Updated to: %a@." (Domain.format_elem dom) new_elem
        );
        Run.set_pred_elem run pred new_elem;
        true
      end

    let rec run_rec pp run wl join_trans =
      match Strat.remove_next wl with
      | None -> ()
      | Some pred ->
          Opt.do_if Opt.print_kleene (fun () ->
            let solver = Run.solver run in
            eprintf "Pick from worklist: %a@." (Pred.format solver) pred;
          );
          if join_with_trans pp run wl join_trans pred then
            Strat.add_dependent ~add_self:true wl pred
          else ();
          run_rec pp run wl join_trans

    let rec run_narrow_rec pp run wl join_trans =
      match Strat.remove_next wl with
      | None -> ()
      | Some pred ->
          Opt.do_if Opt.print_kleene (fun () ->
            let solver = Run.solver run in
            eprintf "Pick from worklist: %a@." (Pred.format solver) pred;
          );
          if replace_with_trans pp run join_trans pred then begin
            Strat.narrowed wl pred;
            Strat.add_dependent ~add_self:true wl pred
          end else ();
          run_narrow_rec pp run wl join_trans
  end

  module FwdFix = MakeFix (FwdStrat)
  module BackwFix = MakeFix (BackwStrat)

  let format_elem run dom fmt elem =
    if Opt.format_elems_smtlib () then begin
      let solver = Run.solver run in
      let term = Domain.term_of_elem dom elem in
      Solver.format_term solver fmt term
    end else
      Domain.format_elem dom fmt elem

  let format_vals fmt run =
    pp_open_box fmt 0;
    let first = ref true in
    Run.iter_pred_data
      (fun pdata ->
        let pred = Run.PredData.pred pdata in
        let elem = Run.PredData.elem pdata in
        let dom = Run.PredData.space pdata in
        if !first then first := false else pp_print_newline fmt ();        
        Pred.format (Run.solver run) fmt pred;
        pp_print_space fmt ();
        pp_print_string fmt "->";
        pp_print_space fmt ();
        format_elem run dom fmt elem)
      run;
      pp_close_box fmt ()

  let run_forward ?(narrow=true) pp run strat =
    let wl = FwdStrat.mk_wl strat in
    FwdFix.run_rec pp run wl PrePost.join_post;
    Opt.do_if Opt.print_kleene (fun () ->
      eprintf "; Forward analysis stabilized:@.%a@." format_vals run
    );
    if narrow then begin
      let wl = FwdStrat.mk_narrow_wl strat in
      FwdFix.run_narrow_rec pp run wl PrePost.join_post;
      Opt.do_if Opt.print_fixpoints (fun () ->
        eprintf "; Forward analysis w. narrowing stabilized:@.%a@." format_vals run
      )
    end else ()

  let run_backward ?(narrow=true) pp run strat =
    let wl = BackwStrat.mk_wl strat in
    BackwFix.run_rec pp run wl PrePost.join_pre;
    Opt.do_if Opt.print_kleene (fun () ->
      eprintf "; Backward analysis stabilized:@.%a@." format_vals run
    );
    if narrow then begin
      let wl = BackwStrat.mk_narrow_wl strat in
      BackwFix.run_narrow_rec pp run wl PrePost.join_pre;
      Opt.do_if Opt.print_fixpoints (fun () ->
        eprintf "; Backward analysis w. narrowing stabilized:@.%a@." format_vals run
      )
    end else ()  

  let mk_first_run ctx =    
    let clause_set = Ctx.clause_set ctx in
    let pred_data = PredHashtbl.create (ClauseSet.pred_count clause_set) in
    ClauseSet.iter_preds
      (fun pred ->
        let dom = Ctx.pred_space ctx pred in
        PredHashtbl.replace pred_data pred
          (Run.PredData.make pred dom (Domain.mk_bot dom) (Domain.mk_top dom)))
      clause_set;
    Run.make ctx pred_data

  let mk_next_run prev_run =    
    let pred_data = PredHashtbl.map
      (fun pred pdata ->
        let dom = Run.PredData.space pdata in
        Run.PredData.make pred dom (Domain.mk_bot dom) (Run.PredData.elem pdata))
      prev_run.Run.t_pred_data
    in
    { prev_run with Run.t_pred_data = pred_data }

  let mk_top_run ctx =
    let run = mk_first_run ctx in
    Run.iter_pred_data
      (fun pdata ->
        let dom = Run.PredData.space pdata in
        Run.PredData.set_elem pdata (Domain.mk_top dom))
      run;
    run

  exception Not_for_all
  let vals_equal run1 run2 = 
    (* TODO: check that runs are from the same ClauseSet. *)
    let clause_set = Run.clause_set run1 in
    try
      ClauseSet.iter_preds
        (fun pred -> 
          let dom = Run.pred_space run1 pred in
          if not (Domain.equal dom (Run.pred_elem run1 pred) (Run.pred_elem run2 pred)) then
            raise Not_for_all
          else ())
        clause_set;
      true
    with
    | Not_for_all -> false

  let run
    ?(start_forward=true)
    ?(end_forward=true)
    ?(iterations=2)
    ?(narrow=true)
    clause_set
    domain
    fwd_strat
    backw_strat =
    let ctx = Ctx.make clause_set domain fwd_strat backw_strat in
    let pp = PrePost.make ctx in
    let run_data =
      if start_forward then begin
        let run_data = mk_first_run ctx in
        run_forward ~narrow:narrow pp run_data fwd_strat;
        run_data
      end else
        mk_top_run ctx
    in
    let rec run_backw_rec prev_run_data iters_remaining =
      if iters_remaining <= 0 then
        prev_run_data
      else begin
        let run_data = mk_next_run prev_run_data in
        run_backward ~narrow:narrow pp run_data backw_strat;
        (* NOTE: If we don't start with forward analysis, we should not check
           for stabilization after the first backward run. *)
        if  (start_forward || (iters_remaining < iterations)) &&
            (vals_equal run_data prev_run_data) then begin
          Opt.do_if Opt.print_fixpoints (fun () ->
            eprintf "; Values stabilized.@."
          );
          run_data
        end else
          run_fwd_rec run_data iters_remaining
      end
    and run_fwd_rec prev_run_data iters_remaining =
      if iters_remaining > 1 || end_forward then begin
        let run_data = mk_next_run prev_run_data in
        run_forward ~narrow:narrow pp run_data fwd_strat;
        if vals_equal run_data prev_run_data then begin
          Opt.do_if Opt.print_fixpoints (fun () ->
            eprintf "; Values stabilized.@."
          );
          run_data
        end else
          run_backw_rec run_data (iters_remaining - 1)
      end else
        prev_run_data
    in
    run_backw_rec run_data iterations

end
