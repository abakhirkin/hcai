module type T = sig
  type key

  type 'a t

  val add: 'a t -> key -> 'a -> unit

  val find: 'a t -> key -> 'a

  val iter: (key -> 'a -> unit) -> 'a t -> unit
end

module Make(Ord: Set.OrderedType) = struct
  module Map = Map.Make(Ord)

  type key = Map.key

  type 'a t = {
    mutable t_map: 'a Map.t
  }

  let make () =
    { t_map = Map.empty }

  let add map key value =
    map.t_map <- Map.add key value map.t_map

  let find map key =
    Map.find key map.t_map

  let iter fn map =
    Map.iter fn map.t_map
  
end