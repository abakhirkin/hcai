open Format

module DynArray = Batteries.DynArray
module Hashtbl = Batteries.Hashtbl

module Make(G: Graph.Sig.I) = struct
  module VHashtbl = Hashtbl.Make(G.V)

  type data_t = int DynArray.t

  let hier_num data = data

  let is_reachable data =
    (DynArray.length data) > 0

  let is_nscc_head data =
    (DynArray.length data > 0) && (DynArray.last data < 0)    

  let make_hier_nums g roots =
    let next_dfn = ref 1 in
    let mk_next_dfn () =
      let n = !next_dfn in next_dfn := !next_dfn + 1; n
    in
    let dfns = VHashtbl.create (G.nb_vertex g) in
    let dfn v =
      VHashtbl.find dfns v in
    let set_dfn v n =
      VHashtbl.replace dfns v n;
    in
    G.iter_vertex (fun v -> set_dfn v 0) g;
    let stack = Stack.create () in
    let hier_nums = VHashtbl.create (G.nb_vertex g) in
    let add_hier_num v n =
      let hier_num = VHashtbl.find hier_nums v in
      DynArray.add hier_num n      
    in
    G.iter_vertex (fun v -> VHashtbl.replace hier_nums v (DynArray.create ())) g;
    let rec visit_component vertices =
      let next_sccn = ref 0 in
      let mk_next_sccn () =
        let n = !next_sccn in next_sccn := !next_sccn + 1; n
      in
      let rec visit v =
        Stack.push v stack;
        let v_dfn = mk_next_dfn () in
        set_dfn v v_dfn;
        let (head_dfn, on_loop) = G.fold_succ
          (fun succ_v (head_dfn, on_loop) ->
            let succ_dfn = dfn succ_v in
            let succ_dfn = if succ_dfn = 0 then visit succ_v else succ_dfn in
            if succ_dfn <= head_dfn then
              (succ_dfn, true)
            else
              (head_dfn, on_loop)
          )
          g v (v_dfn, false)
        in
        if head_dfn = v_dfn then (
          let sccn = mk_next_sccn () in
          let rec pop_scc () =
            let pop_v = Stack.pop stack in            
            if G.V.equal pop_v v then
              set_dfn pop_v max_int
            else (
              set_dfn pop_v 0;
              pop_scc ()
            );
            add_hier_num pop_v sccn
          in
          pop_scc ();
          if on_loop then begin
            visit_component (G.fold_succ (fun succ_v acc -> succ_v :: acc) g v []);
            add_hier_num v (- 1)
          end else ()
        )
        else ();
        head_dfn        
      in
      List.iter
        (fun v ->
          if (dfn v) = 0 then
            let _ = visit v in ()
          else ())
        vertices
    in
    visit_component roots;    
    (* TODO: Describe what exactly hier_nums are. *)
    hier_nums
end
