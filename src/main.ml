open Format
open FormatU
open Smt2
open ExceptionU

module Lexing = Batteries.Lexing

module ClauseSet = ClauseSet.Make(Z3Solver)
module ClauseBuilder = ClauseSet.Builder
module Pred = ClauseSet.Pred
module Solver = ClauseSet.Solver
module Reader = SeaReader.Make(ClauseSet)
module DepG = ClauseSet.DepG
module NSccStrategy = NestedSccStrategy.Make(DepG)
module FwdStrat = NSccStrategy.Forward
module BackwStrat = NSccStrategy.Backward
module PredHashtbl = ClauseSet.Pred.Hashtbl 
module PredHashset = ClauseSet.Pred.Hashset

module BddApronDomain = BddApronDomain.Mk(Solver)

module type DepFmtArgsT = sig
  val clause_set: ClauseSet.t
  val pred_hier_num: Pred.t -> int DynArray.t
end

module MakeDepFmt(Args: DepFmtArgsT) = struct
  include DepG  

  let clause_set = Args.clause_set
  let pred_hier_num = Args.pred_hier_num

  let graph_attributes _ = []
  let default_edge_attributes _ = []
  let edge_attributes _ = []
  let get_subgraph _ = None

  let default_vertex_attributes _ = [`Shape `Box]

  let pred_name pred =
    asprintf "%a" (Pred.format (ClauseSet.solver clause_set)) pred

  let vertex_name pred =
    let name = pred_name pred in
    let escaped_name = Str.global_replace
      (Str.regexp_string "\"") "\\\"" name in
    sprintf "\"%s\"" escaped_name
    
  let vertex_attributes pred =
    let label = asprintf "%s\\n[%a]"
      (pred_name pred)
      (format_dynarray ~print_sep:(string_sep "; ") pp_print_int) (pred_hier_num pred)
    in
    let attrs = [`Label label] in
    let attrs = if Pred.is_init pred then
      `Style `Filled :: `Fillcolor 0xc0c0c0 :: attrs
    else
      attrs
    in
    attrs
end

let print_dep_graph clause_set dep_graph pred_hier_num =
  let args_module = (module struct
      let clause_set = clause_set
      let pred_hier_num = pred_hier_num
    end : DepFmtArgsT)
  in
  let module Args = (val args_module) in
  let module Dot = Graph.Graphviz.Dot(MakeDepFmt (Args)) in
  printf "%a@." Dot.fprint_graph dep_graph

let parse_args () =
  Arg.parse Opt.opt_spec Opt.anon_spec Opt.usage

let read_file ?(int_as_real=false) file =
  let solver = Z3Solver.mk () in
  let builder = ClauseBuilder.make solver in
  let rd = Reader.make ~int_as_real:int_as_real solver builder in
  FileU.with_opt_file_in file (fun input ->
    let lexbuf = Lexing.from_input input in
    Reader.read_lexbuf rd lexbuf;
    ClauseBuilder.build builder)

(* TODO: This goes to a seprate module. *)
let replace_pred_apps clause_set mk_replace_pred_app term  =
  let solver = ClauseSet.solver clause_set in
  let rec replace_pred_apps_rec term =
    if Solver.is_interpreted_const solver term then
      term
    else if Solver.is_uninterpreted_const solver term then
      let func_decl = Solver.term_func_decl solver term in
      if ClauseSet.is_pred clause_set func_decl then
        let pred = ClauseSet.func_decl_pred clause_set func_decl in
        mk_replace_pred_app pred []
      else
        term
    else if Solver.is_apply solver term then
      let func_decl = Solver.term_func_decl solver term in
      let term_args = Solver.term_args solver term in
      if ClauseSet.is_pred clause_set func_decl then
        let pred = ClauseSet.func_decl_pred clause_set func_decl in
        mk_replace_pred_app pred term_args
      else
        let term_args = List.map replace_pred_apps_rec term_args in
        Solver.mk_apply solver func_decl term_args
    else
      invalid_term "Not supported." (Solver.term_to_string solver term)
  in
  replace_pred_apps_rec term

let analyse_clause_set_time = Counter.make_time_span "main.analyse_clause_set"

module Analysis = struct
  module Make (Domain : Domains.T with module Solver = Solver) = struct

    module AbstInt = AbstInt.Make(ClauseSet)(Domain)(PathFocusingPrePost.Make)(FwdStrat)(BackwStrat)

    let mk_goals_reachable_term run =
      let clause_set = AbstInt.Run.clause_set run in
      let solver = AbstInt.Run.solver run in
      let goal_terms = ClauseSet.fold_goal_terms
        (fun acc goal_term ->
          (replace_pred_apps
            clause_set
            (fun pred args -> AbstInt.Run.mk_pred_app_term run pred args)
            goal_term)
          :: acc
        )
        []
        clause_set
      in
      Solver.mk_or solver goal_terms
    
    let analyse_clause_set_impl clause_set =
      let solver = ClauseSet.solver clause_set in
      let domain = Domain.make_domain solver in
      Opt.do_if Opt.print_clauses (fun () ->
        printf "; Forward system:@.";
        printf "%a@." ClauseSet.format clause_set
      );
      Opt.do_if Opt.print_backward_clauses (fun () ->
        printf "; Backward system:@.%a@." ClauseSet.format_backw_clauses clause_set;
      );
      Opt.do_if Opt.print_heads (fun () ->
        printf "; Predicates with their argument variables:@.";
        ClauseSet.iter_preds
          (printf "%a@." (Pred.format_head solver))
          clause_set
      );
      (* TODO: Maybe move dependency graph stuff inside the NestedSccStrategy.
        We don't have other strategies anyway. *) 
      let dep_graph = ClauseSet.dep_graph clause_set in
      let init_preds = ClauseSet.fold_init_preds (fun acc init -> init :: acc) [] clause_set in
      let fwd_strat = FwdStrat.mk
        ~widening_delay:(Opt.widening_delay ())
        ~narrowing_updates:(Opt.narrowing_updates ())
        dep_graph
        init_preds
        init_preds
      in
      Opt.do_if Opt.print_dep_graph (fun () ->
        print_dep_graph clause_set dep_graph (FwdStrat.loc_priority fwd_strat)
      );
      let goals = ClauseSet.fold_goal_preds (fun acc goal -> goal :: acc) [] clause_set in
      (* TODO: Analysis options should be passed here in a structure; don't read them from Opt here. *)
      let backw_strat = BackwStrat.mk
        ~widening_delay:(Opt.widening_delay ())
        ~narrowing_updates:(Opt.narrowing_updates ())
        dep_graph
        init_preds
        goals
      in
      let result = if Opt.analyse () then begin
        let run = AbstInt.run
          ~start_forward:(Opt.start_forward ())
          ~iterations:(Opt.iterations ())
          clause_set
          domain
          fwd_strat
          backw_strat
        in
        if (Opt.print_result ()) && (not (Opt.print_fixpoints ())) then
          printf "%a@." AbstInt.format_vals run
        else ();
        let goals_reachable_term = mk_goals_reachable_term run in
        Opt.do_if Opt.print_smt_queries (fun () ->
          printf "%a@." (Solver.format_term solver) goals_reachable_term
        );
        let goals_reachable =
          match Solver.is_sat solver goals_reachable_term with
          | Solvers.Unsat -> Some false
          | _ -> None
        in
        let goals_reachable_s =
          match goals_reachable with
          | Some false -> "false"
          | _ -> "maybe"
        in
        Opt.do_if Opt.print_result (fun () ->
          printf "; Goals reachable: %s@." goals_reachable_s
        );
        goals_reachable
      end else
        None
      in
      result

    let analyse_clause_set clause_set =
      Opt.do_if Opt.clear_counters (fun () ->
        Counter.clear_all ();
      );
      let result = Counter.add_duration analyse_clause_set_time (fun () ->
        analyse_clause_set_impl clause_set)
      in
      (* TODO: Perhaps, printing counters goes somewhere up the call chain. *)
      Opt.do_if Opt.print_counters (fun () ->
        printf "; Counters:@.%a@." Counter.format ()
      );
      result

    let analyse_file ?(int_as_real=false) file =
      let clause_set = read_file ~int_as_real:int_as_real file in
      analyse_clause_set clause_set
  end
end

module BddApronAnalysis = Analysis.Make(BddApronDomain)
module BpolyAnalysis = Analysis.Make(BpolyDomain)

let analyse_file
  ?(domain=Opt.BddApron)
  ?(int_as_real=false)
  file =
  match domain with
  | Opt.BddApron -> BddApronAnalysis.analyse_file ~int_as_real:int_as_real file
  | Opt.Bpoly -> BpolyAnalysis.analyse_file ~int_as_real:int_as_real file