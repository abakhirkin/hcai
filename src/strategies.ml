module type T = sig
  type loc_t  

  type strat_t  

  type wl_t

  val mk_wl: strat_t -> wl_t

  val mk_narrow_wl: strat_t -> wl_t

  val remove_next: wl_t -> loc_t option

  val should_widen: wl_t -> loc_t -> bool

  val did_not_widen: wl_t -> loc_t -> unit

  val add_dependent: ?add_self:bool -> wl_t -> loc_t -> unit

  val narrowed: wl_t -> loc_t -> unit
end