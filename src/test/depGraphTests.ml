open OUnit2
open Format
open FormatU

module Lexing = Batteries.Lexing
module File = Batteries.File
module IO = Batteries.IO

module ClauseSet = ClauseSet.Make(Z3Solver)
module ClauseBuilder = ClauseSet.Builder
module Reader = SeaReader.Make(ClauseSet)
module Pred = ClauseSet.Pred
module DepG = ClauseSet.DepG

module TestGraph = struct
  type t = {
    t_vertices: string Hashset.t;
    t_edges: (string * string) Hashset.t
  }

  let of_arrays vertices edges =
    { t_vertices = Hashset.of_array vertices;
      t_edges = Hashset.of_array edges }

  let of_lists vertices edges =
    { t_vertices = Hashset.of_list vertices;
      t_edges = Hashset.of_list edges }

  let of_graph solver g =
    of_lists
      (DepG.fold_vertex
        (fun v acc -> Pred.to_string solver v :: acc)
        g
        [])
      (DepG.fold_edges
        (fun v1 v2 acc -> (Pred.to_string solver v1, Pred.to_string solver v2) :: acc)
        g
        [])

  let equal tg1 tg2 =
    (Hashset.equal tg1.t_vertices tg2.t_vertices) && (Hashset.equal tg1.t_edges tg2.t_edges)

  let format fmt tg =
    pp_open_box fmt 0;
    pp_print_string fmt "(";
    pp_print_string fmt "[";
    format_iter ~print_sep:(fun fmt -> pp_print_string fmt ";"; pp_print_space fmt ())
      pp_print_string
      Hashset.iter
      fmt
      tg.t_vertices;
    pp_print_string fmt "]";
    pp_print_string fmt ",";
    pp_print_space fmt ();  
    pp_print_string fmt "[";
    format_iter ~print_sep:(fun fmt -> pp_print_string fmt ";"; pp_print_space fmt ())
      (fun fmt (v1, v2) ->
        pp_print_string fmt "(";
        pp_print_string fmt v1;
        pp_print_string fmt ",";
        pp_print_space fmt ();
        pp_print_string fmt v2;
        pp_print_string fmt ")") 
      Hashset.iter
      fmt
      tg.t_edges;
    pp_print_string fmt "]";
    pp_print_string fmt ")";
    pp_close_box fmt ()

  let to_string tg =
    asprintf "%a" format tg
end

let test_files =
  [ ("parallel_increment.smt2",
     TestGraph.of_lists ["P"] [("P", "P")]);
     
    ("parallel_increment2.smt2",
     TestGraph.of_lists
      ["P0"; "P1"; "P2"; "P3"; "P4"; "P5"]
      [("P0", "P1"); ("P1", "P2"); ("P1", "P5"); ("P2", "P3"); ("P3", "P4"); ("P4", "P1")]);
      
    ("parallel_increment_e.smt2",
     TestGraph.of_lists ["P"; "E"] [("P", "P"); ("P", "E")]);

    ("parallel_increment_sea.smt2",
     TestGraph.of_lists
      ["verifier.error"; "main@entry"; "main@_bb"; "main@verifier.error.split"]
      [("main@entry","main@_bb"); ("main@_bb","main@_bb"); ("main@_bb","main@verifier.error.split")]);
      
    ("unused_locations.smt2",
     TestGraph.of_lists
      ["verifier.error"; "main@entry"; "main@postcall"; "main@precall.split"]
      [ ("main@entry", "main@postcall"); ("main@entry","main@precall.split");
        ("main@postcall","main@postcall"); ("main@postcall","main@precall.split") ]) ]

let test_dep_graph file_name expected =
  let solver = Z3Solver.mk () in  
  let actual = File.with_file_in ("data/test/" ^ file_name) (fun input ->
    let builder = ClauseBuilder.make solver in
    let lexbuf = Lexing.from_input input in
    let rd = Reader.make solver builder in
    Reader.read_lexbuf rd lexbuf;
    let clauses = ClauseBuilder.build builder in
    TestGraph.of_graph solver (ClauseSet.dep_graph clauses)) in  
  (* printf "Expected@.%s@.Actual:@.%s@." expected actual; *)
  assert_equal ~cmp:TestGraph.equal ~printer:TestGraph.to_string expected actual

let tests =
  "dep_graph" >:::
  (List.map
    (fun (file_name, expected) ->
      ("dep_graph_" ^ file_name) >:: (fun _ -> test_dep_graph file_name expected))
    test_files)