open OUnit2

let tests = "hcai" >:::
  [ Bpoly.BpolyTests.tests;
    BpolyAnalysisTests.tests;
    BddApronAnalysisTests.tests;
    ClauseBuilderTests.tests;
    Z3NnfTests.tests;
    DepGraphTests.tests;
    DomainTests.tests ]

let () =
  Opt.set_print_result false;
  run_test_tt_main tests
