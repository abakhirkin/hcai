open OUnit2

let test_files =
  (* TODO: Uncomment tests *)
  [ ("parallel_increment.smt2", Some false);
    (* ("parallel_increment2.smt2", Some false); *)
    ("parallel_increment3.smt2", Some false);
    ("parallel_increment_e.smt2", None);
    ("parallel_increment_noquery.smt2", Some false);
    (* ("parallel_increment_sea.smt2", Some false); *)
    ("bool_copy.smt2", None);
    (* ("fib.smt2", Some false); *)
    ("unused_locations.smt2", Some false);
    ("addition_loops.smt2", Some false);
    ("addition_loops2.smt2", Some false);
    ("addition_loops_sea.smt2", Some false);
    ("addition_one_loop.smt2", Some false);
    ("addition_one_loop_e.smt2", None) ]

let test_analyse_file file expected =
  assert_equal ~msg:file
    expected
    (** TODO: Figure out why this slows down parallel test execution. *)
    (Main.BpolyAnalysis.analyse_file (Opt.FileName ("data/test/" ^ file)))

let tests =
  "bpoly_analysis" >:::
  (List.map
    (fun (file, expected) ->
      ("analyse_" ^ file) >:: (fun _ -> test_analyse_file file expected))
    test_files)