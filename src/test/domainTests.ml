open OUnit2

module Solver = Z3Solver
module BddApronDomain = BddApronDomain.Mk(Solver)

module Make (Domain : Domains.T) = struct

  module Solver = Domain.Solver

  let test_move_rename make_solver _ =    
    let solver = make_solver () in
    let domain = Domain.make_domain solver in
    let bool_sort = Solver.mk_bool_sort solver in
    let int_sort = Solver.mk_int_sort solver in
    let b0 = Solver.mk_const_s solver bool_sort "b0" in
    let x0 = Solver.mk_const_s solver int_sort "x0" in
    let b1 = Solver.mk_const_s solver bool_sort "b1" in  
    let x1 = Solver.mk_const_s solver int_sort "x1" in  
    let i0 = Solver.mk_int solver 0 in
    let fd_b0 = Solver.term_func_decl solver b0 in
    let fd_x0 = Solver.term_func_decl solver x0 in
    let fd_b1 = Solver.term_func_decl solver b1 in
    let fd_x1 = Solver.term_func_decl solver x1 in
    let space0 = Domain.make_space domain [fd_b0; fd_x0] in
    let space1 = Domain.make_space domain [fd_b1; fd_x1] in
    let p0 = Domain.elem_of_expl_terms
      space0
      [b0; Solver.mk_geq solver [x0; i0]]
    in
    let p1 = Domain.elem_of_expl_terms
      space1
      [b1; Solver.mk_geq solver [x1; i0]]
    in
    let p0to1 = Domain.move_rename_array
      space0
      p0
      [| (fd_b0, fd_b1); (fd_x0, fd_x1) |]
      space1
    in
    assert_equal ~cmp:(Domain.equal space1) ~printer:(Domain.to_string space1)
      p1
      p0to1

  let tests make_solver =
    [ "move_rename" >:: test_move_rename make_solver ]
end

module BddApronDomainTests = Make(BddApronDomain)
module BpolyDomainTests = Make(BpolyDomain)

let tests =
  let make_solver () = Solver.mk () in
  "domain" >:::
    [ "bddapron" >::: BddApronDomainTests.tests make_solver;
      "bpoly" >::: BpolyDomainTests.tests make_solver ]