open OUnit2
open Format

module Lexing = Batteries.Lexing
module File = Batteries.File
module IO = Batteries.IO

module ClauseSet = ClauseSet.Make(Z3Solver)
module ClauseBuilder = ClauseSet.Builder

module Reader = SeaReader.Make(ClauseSet)

let read_test_files =
  [ ("parallel_increment.smt2", "parallel_increment.builder.txt");
    ("addition_loops.smt2", "addition_loops.builder.txt") ]

let backw_clauses_test_files =
  [ ("parallel_increment.smt2", "parallel_increment.backw.txt");
    ("addition_loops.smt2", "addition_loops.backw.txt") ]

let test_read_file file_name expected_name =
  let solver = Z3Solver.mk () in
  let expected = File.with_file_in ("data/test/" ^ expected_name) IO.read_all in  
  let actual = File.with_file_in ("data/test/" ^ file_name) (fun input ->
    let builder = ClauseBuilder.make solver in
    let lexbuf = Lexing.from_input input in
    let rd = Reader.make solver builder in
    Reader.read_lexbuf rd lexbuf;
    let clauses = ClauseBuilder.build builder in
    ClauseSet.to_string clauses) in  
  assert_equal ~msg:file_name ~printer:(fun s->"\n"^s) expected actual

let test_backw_clauses file_name expected_name =
  let solver = Z3Solver.mk () in
  let expected = File.with_file_in ("data/test/" ^ expected_name) IO.read_all in
  let actual = File.with_file_in ("data/test/" ^ file_name) (fun input ->
    let builder = ClauseBuilder.make solver in
    let lexbuf = Lexing.from_input input in
    let rd = Reader.make solver builder in
    Reader.read_lexbuf rd lexbuf;    
    let clauses = ClauseBuilder.build builder in
    ClauseSet.backw_clauses_to_string clauses) in  
  assert_equal ~msg:file_name ~printer:(fun s->"\n"^s) expected actual

let tests =
  "clause_builder" >:::
  (List.map
    (fun (file_name, expected_name) ->
      ("read_" ^ file_name) >:: (fun _ -> test_read_file file_name expected_name))
    read_test_files) @
  (List.map
    (fun (file_name, expected_name) ->
      ("backw_clauses_" ^ file_name) >:: (fun _ -> test_backw_clauses file_name expected_name))
    backw_clauses_test_files)