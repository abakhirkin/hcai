open OUnit2

let z3 =
  Z3Solver.mk ()

let mk_int_const_s name =
  Z3Solver.mk_const_s z3 (Z3Solver.mk_int_sort z3) name  

let mk_bool_const_s name =
  Z3Solver.mk_const_s z3 (Z3Solver.mk_bool_sort z3) name

let test_terms =
  let b1 = mk_bool_const_s "b1" in
  let b2 = mk_bool_const_s "b2" in
  let x1 = mk_int_const_s "x1" in
  let x2 = mk_int_const_s "x2" in
  let x3 = mk_int_const_s "x3" in
  let x4 = mk_int_const_s "x4" in
  let z3name0 = mk_int_const_s "z3name!0" in
  let z3name1 = mk_int_const_s "z3name!1" in
    [ ("num_distinct",
       Z3Solver.mk_distinct z3 [x1; x2],
       Z3Solver.mk_or z3 [
          Z3Solver.mk_lt z3 [x1; x2];
          Z3Solver.mk_gt z3 [x1; x2]
       ],
       false);

      ("num_neq",
       Z3Solver.mk_not z3 (Z3Solver.mk_eq z3 [x1; x2]),
       Z3Solver.mk_or z3 [
          Z3Solver.mk_lt z3 [x1; x2];
          Z3Solver.mk_gt z3 [x1; x2]
       ],
       false);

      ("num_not_distinct",
       Z3Solver.mk_not z3 (Z3Solver.mk_distinct z3 [x1; x2; x3]),
       OperationsU.pairwise_of_binop
        (Z3Solver.mk_or z3)
        (fun t1 t2 -> Z3Solver.mk_eq z3 [t1; t2])
        [x1; x2; x3],
       false);

      ("num_not_not_distinct",
       Z3Solver.mk_not z3 (Z3Solver.mk_not z3 (Z3Solver.mk_distinct z3 [x1; x2])),
       Z3Solver.mk_or z3 [
          Z3Solver.mk_lt z3 [x1; x2];
          Z3Solver.mk_gt z3 [x1; x2]
       ],
       false);

      ("bool_eq",
       Z3Solver.mk_eq z3 [b1; b2],       
       Z3Solver.mk_and z3 [
          Z3Solver.mk_or z3 [Z3Solver.mk_not z3 b1; b2];
          Z3Solver.mk_or z3 [b1; Z3Solver.mk_not z3 b2]
       ],
       false);

      ("bool_iff",
       Z3.Boolean.mk_iff z3 b1 b2,       
       Z3Solver.mk_and z3 [
          Z3Solver.mk_or z3 [Z3Solver.mk_not z3 b1; b2];
          Z3Solver.mk_or z3 [b1; Z3Solver.mk_not z3 b2]
       ],
       false); 

      ("bool_not_eq",
       Z3Solver.mk_not z3 (Z3Solver.mk_eq z3 [b1; b2]),
       Z3Solver.mk_and z3 [
          Z3Solver.mk_or z3 [b1; b2];
          Z3Solver.mk_or z3 [Z3Solver.mk_not z3 b1; Z3Solver.mk_not z3 b2]
       ],
       false);

      ("bool_distinct",
       Z3Solver.mk_distinct z3 [b1; b2],
       Z3Solver.mk_xor z3 [ b1; b2 ],
       false);

      ("bool_not_distinct",
       Z3Solver.mk_not z3 (Z3Solver.mk_distinct z3 [b1; b2]),
       Z3Solver.mk_and z3 [
          Z3Solver.mk_or z3 [Z3Solver.mk_not z3 b1; b2];
          Z3Solver.mk_or z3 [b1; Z3Solver.mk_not z3 b2]
       ],
       false);

      ("distinct_ite",
       Z3Solver.mk_distinct z3 [
        (Z3Solver.mk_ite z3 b1 x1 x2);
        (Z3Solver.mk_ite z3 b2 x3 x4)
      ],
      Z3Solver.mk_and z3 [
        Z3Solver.mk_or z3 [
          Z3Solver.mk_lt z3 [z3name0; z3name1];
          Z3Solver.mk_gt z3 [z3name0; z3name1]
        ];
        Z3Solver.mk_or z3 [
          Z3Solver.mk_not z3 b2;
          Z3Solver.mk_eq z3 [z3name1; x3]
        ];
        Z3Solver.mk_or z3 [
          b2;
          Z3Solver.mk_eq z3 [z3name1; x4]
        ];
        Z3Solver.mk_or z3 [
          Z3Solver.mk_not z3 b1;
          Z3Solver.mk_eq z3 [z3name0; x1]
        ];
        Z3Solver.mk_or z3 [
          b1;
          Z3Solver.mk_eq z3 [z3name0; x2]
        ]
      ],
      true)
    ]

let test_term_nnf ?(compare_strings=false) name term expected_nnf =
  let nnf = Z3Solver.to_nnf z3 term in
  let cmp t1 t2 =
    if compare_strings then
      (Z3Solver.term_to_string z3 t1) = (Z3Solver.term_to_string z3 t2)
    else
      Z3Solver.term_equal z3 t1 t2
  in
  assert_equal
    ~msg:name    
    ~cmp:cmp
    ~printer:(Z3Solver.term_to_string z3)
    expected_nnf nnf

let tests =
  "z3_nnf" >:::
  (List.map
    (fun (name, term, expected_nnf, compare_strings) ->
      ("z3_nnf_" ^ name) >:: (fun _ -> test_term_nnf ~compare_strings:compare_strings name term expected_nnf))
    test_terms)