open Batteries.Lexing
open Format

exception Parse_error of string

let lexbuf_pos_string lexbuf =
  Printf.sprintf ""

let parse_error msg_1 lexbuf =
  let curr_p = lexbuf.lex_curr_p in
  let msg = sprintf "%s. File \"%s\", line %d, char %d."
    msg_1 curr_p.pos_fname curr_p.pos_lnum (curr_p.pos_cnum - curr_p.pos_bol + 1)
  in
  raise (Parse_error msg)

let wrap_parse_error fn lexbuf =
  try
    fn ()
  with
  | Smt2Parser.Error -> parse_error "Parser error" lexbuf
  | Smt2Lexer.Error msg -> parse_error msg lexbuf
  | Failure msg -> parse_error msg lexbuf

let parse_term lexbuf =
  wrap_parse_error
    (fun () -> Smt2Parser.term Smt2Lexer.mk_lexer lexbuf)
    lexbuf

let parse_sea_command lexbuf =
  wrap_parse_error
    (fun () -> Smt2Parser.sea_command Smt2Lexer.mk_sea_lexer lexbuf)
    lexbuf
