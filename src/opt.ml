open Format

type domain_t = BddApron | Bpoly

type file_t = NoFile | FileName of string | Stdin

type opt_t = {
  mutable opt_file: file_t;
  mutable opt_analyse: bool;
  mutable opt_print_clauses: bool;
  mutable opt_print_dep_graph: bool;
  mutable opt_print_backward_clauses: bool;
  mutable opt_print_smt_queries: bool;
  mutable opt_print_kleene: bool;
  mutable opt_print_fixpoints: bool;
  mutable opt_print_counters: bool;
  mutable opt_start_forward: bool;
  mutable opt_iterations: int;
  mutable opt_widening_delay: int;
  mutable opt_narrowing_updates: int;
  mutable opt_print_result: bool;
  mutable opt_solver_timeout: int;
  mutable opt_format_elems_smtlib: bool;
  mutable opt_print_heads: bool;
  mutable opt_print_explanations: bool;
  mutable opt_int_as_real: bool;
  mutable opt_domain: domain_t;
  mutable opt_clear_counters: bool
}

let default_iterations = 2
let default_widening_delay = 2
let default_narrowing_updates = 2
let default_solver_timeout = 30
let default_domain = BddApron

let opt =
  { opt_file = NoFile;
    opt_analyse = true;
    opt_print_clauses = false;
    opt_print_dep_graph = false;
    opt_print_backward_clauses = false;
    opt_print_smt_queries = false;
    opt_print_kleene = false;
    opt_print_fixpoints = false;
    opt_print_counters = false;
    opt_start_forward = true;
    opt_iterations = default_iterations;
    opt_widening_delay = default_widening_delay;
    opt_narrowing_updates = default_narrowing_updates;
    opt_print_result = true;
    opt_solver_timeout = default_solver_timeout;
    opt_format_elems_smtlib = false;
    opt_print_heads = false;
    opt_print_explanations = false;
    opt_int_as_real = false;
    opt_domain = default_domain;
    opt_clear_counters = true
}

let file () =
  opt.opt_file

let analyse () =
  opt.opt_analyse

let print_clauses () =
  opt.opt_print_clauses

let print_dep_graph () =
  opt.opt_print_dep_graph

let print_backward_clauses () =
  opt.opt_print_backward_clauses

let print_smt_queries () =
  opt.opt_print_smt_queries

let print_kleene () =
  opt.opt_print_kleene

let print_fixpoints () =
  opt.opt_print_fixpoints

let print_counters () =
  opt.opt_print_counters

let start_forward () =
  opt.opt_start_forward

let iterations () =
  opt.opt_iterations

let widening_delay () =
  opt.opt_widening_delay

let narrowing_updates () =
  opt.opt_narrowing_updates

let print_result () =
  opt.opt_print_result

let format_elems_smtlib () =
  opt.opt_format_elems_smtlib

let print_heads () =
  opt.opt_print_heads

let print_explanations () =
  opt.opt_print_explanations

let solver_timeout () =
  opt.opt_solver_timeout

let int_as_real () =
  opt.opt_int_as_real

let domain () =
  opt.opt_domain

let clear_counters () =
  opt.opt_clear_counters

let set_file_name name =
  opt.opt_file <- FileName name

let set_stdin () =
  opt.opt_file <- Stdin

let set_analyse b = 
  opt.opt_analyse <- b

let set_print_clauses b =
  opt.opt_print_clauses <- b

let set_print_dep_graph b =
  opt.opt_print_dep_graph <- b

let set_print_backward_clauses b =
  opt.opt_print_backward_clauses <- b

let set_print_smt_queries b =
  opt.opt_print_smt_queries <- b

let set_print_kleene b =
  opt.opt_print_kleene <- b

let set_print_fixpoints b =
  opt.opt_print_fixpoints <- b

let set_print_counters b =
  opt.opt_print_counters <- b

let set_start_forward b =
  opt.opt_start_forward <- b

let set_iterations iters =
  opt.opt_iterations <- iters

let set_widening_delay delay =
  opt.opt_widening_delay <- delay

let set_narrowing_updates updates =
  opt.opt_narrowing_updates <- updates

let set_print_result print_result =
  opt.opt_print_result <- print_result

let set_format_elems_smtlib b =
  opt.opt_format_elems_smtlib <- b

let set_print_heads b =
  opt.opt_print_heads <- b

let set_solver_timeout timeout =
  opt.opt_solver_timeout <- timeout

let set_print_explanations b =
  opt.opt_print_explanations <- b

let set_int_as_real int_as_real =
  opt.opt_int_as_real <- int_as_real

let set_domain domain =
  opt.opt_domain <- domain

let set_clear_counters b =
  opt.opt_clear_counters <- b

let format_domain fmt domain =
  match domain with
  | BddApron -> pp_print_string fmt "bddapron"
  | Bpoly -> pp_print_string fmt "bpoly"

let domain_of_string str =
  match str with
  | "bddapron" -> BddApron
  | "bpoly" -> Bpoly
  | _ -> raise (Arg.Bad (sprintf "Unknown domain: \"%s\"" str))

let set_domain_s str =
  set_domain (domain_of_string str)

let opt_spec =
  [ ("-stdin", Arg.Unit set_stdin,
      "\tRead input from stdin.");
    ("-ps", Arg.Unit (fun () -> set_print_clauses true),
      "\tPrint internal representation of the system.");
    ("-pd", Arg.Unit (fun () -> set_print_dep_graph true),
      "\tPrint dependency graph in dot format.");
    ("-pb", Arg.Unit (fun () -> set_print_backward_clauses true),
      "\tPrint representation for the system for backward analysis.");
    ("-pq", Arg.Unit (fun () -> set_print_smt_queries true),
      "\tPrint smt queries.");
    ("-pf", Arg.Unit (fun () -> set_print_fixpoints true),
      "\tPrint intermediate fixpoints.");
    ("-pk", Arg.Unit (fun () -> set_print_kleene true; set_print_fixpoints true),
      "\tPrint chaotic iteration events (value updates, widening, etc). Implies -pf.");
    ("-ph", Arg.Unit (fun () -> set_print_heads true),
      "\tPrint a list of predicates together with variables\
         that stand for their arguments.");
    ("-pe", Arg.Unit (fun () -> set_print_explanations true; set_print_kleene true),
      "\tPrint some path focusing information (models, explanations, etc). \
         Implies -pk.");
    ("-pc", Arg.Unit (fun () -> set_print_counters true),
      "\tPrint performance counters.");
    ("-fs", Arg.Unit (fun () -> set_format_elems_smtlib true),
      "\tPrint domain elements in smtlib2 format.");
    ("-ai", Arg.Int set_iterations, (sprintf
      "<number>\tNumber of backward-forward alternations (default: %d)."
      default_iterations));
    ("-awd", Arg.Int set_widening_delay, (sprintf
      "<number>\tWidening delay (default: %d)."
      default_widening_delay));
    ("-anu", Arg.Int set_narrowing_updates, (sprintf
      "<number>\tNumber of narrowing updates (default: %d)."
      default_narrowing_updates));
    ("-ast", Arg.Int set_solver_timeout,
      (sprintf "<number>\tSolver timeout in seconds (default: %d)."
        default_solver_timeout));
    ("-ar", Arg.Unit (fun () -> set_int_as_real true),
      ("\tReplace interger variables with real variables."));
    ("-ad", Arg.String set_domain_s, (asprintf
      "<domain>\tAbstract domain. Possible values:\n\
       \tbddapron\tCombination of BDDs and Apron polyhedra\n\
       \tbpoly\tConjunctions of implications (experimantal)\n\
       \t(default: %a)" format_domain default_domain));
    ("-nasf", Arg.Unit (fun () -> set_start_forward false),
      "\tDo not run forward analysis before first backward analysis.");
    ("-na", Arg.Unit (fun () -> set_analyse false),
      "\tDo not run the analysis, but print requested information about the input.") ]

let anon_spec arg =
  set_file_name arg

let usage =
  "hcai [options] <file>\noptions are:"

let do_if opt_f do_f =
  if opt_f () then do_f () else ()
