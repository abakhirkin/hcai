module type T = functor (Run: AbstIntRun.T) -> sig    

    type t

    val make: Run.Ctx.t -> t

    val join_post: t -> Run.t -> Run.ClauseSet.Pred.t -> Run.Domain.elem_t -> Run.Domain.elem_t

    val join_pre: t -> Run.t -> Run.ClauseSet.Pred.t -> Run.Domain.elem_t -> Run.Domain.elem_t
end
