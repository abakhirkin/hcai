open Format
open ExceptionU
open U

open Bpoly

module DynArray = Batteries.DynArray

module Solver = Z3Solver

module FuncDeclHashtbl = Solver.FuncDeclHashtbl

module Rvar = Var.R
module Bvar = Var.B
module RvarHashtbl = Var.Rhashtbl
module RvarHashset = Var.Rhashset
module BvarHashtbl = Var.Bhashtbl
module BvarHashset = Var.Bhashset

let join_count = Counter.make_int "bpoly_domain.join_count"
let join_time = Counter.make_time_span "bpoly_domain.join_time"
let leq_count = Counter.make_int "bpoly_domain.leq_count"
let leq_time = Counter.make_time_span "bpoly_domain.leq_time"
let equal_count = Counter.make_int "bpoly_domain.equal_count"
let equal_time = Counter.make_time_span "bpoly_domain.equal_time"
let meet_count = Counter.make_int "bpoly_domain.meet_count"
let meet_time = Counter.make_time_span "bpoly_domain.meet_time"
let widen_count = Counter.make_int "bpoly_domain.widen_count"
let widen_time = Counter.make_time_span "bpoly_domain.widen_time"
let eliminate_r_count = Counter.make_int "bpoly_domain.eliminate_rational_count"
let eliminate_r_var_count = Counter.make_int "bpoly_domain.eliminate_rational_var_count"
let eliminate_r_time = Counter.make_time_span "bpoly_domain.eliminate_rational_time"
let eliminate_b_count = Counter.make_int "bpoly_domain.eliminate_bool_count"
let eliminate_b_var_count = Counter.make_int "bpoly_domain.eliminate_bool_var_count"
let eliminate_b_time = Counter.make_time_span "bpoly_domain.eliminate_bool_time"
let elem_of_expl_count = Counter.make_int "bpoly_domain.elem_of_explanation_count"
let elem_of_expl_time = Counter.make_time_span "bpoly_domain.elem_of_explanation_time"

type domain_t = {
  domain_ctx: Ctx.t;
  domain_rvar_terms: Z3.Expr.expr RvarHashtbl.t;
  domain_bvar_terms: Z3.Expr.expr BvarHashtbl.t;
  domain_term_rvars: Rvar.t FuncDeclHashtbl.t;
  domain_term_bvars: Bvar.t FuncDeclHashtbl.t;
  mutable domain_next_rvar: int;
  mutable domain_next_bvar: int
}

let domain_ctx domain =
  domain.domain_ctx

let domain_solver domain =
  Ctx.z3 domain.domain_ctx

let new_temp_rvar domain var =
  let z3 = domain_solver domain in
  let term = Z3.Expr.mk_fresh_const z3
    (sprintf "tx%d" (-(Rvar.id var) - 1))
    (Z3.Arithmetic.Real.mk_sort z3)
  in
  RvarHashtbl.add domain.domain_rvar_terms var term;
  FuncDeclHashtbl.add domain.domain_term_rvars (Z3.Expr.get_func_decl term) var;
  term

let new_temp_bvar domain var =
  let z3 = domain_solver domain in
  let term = Z3.Expr.mk_fresh_const z3
    (sprintf "tb%d" (-(Bvar.id var) - 1))
    (Z3.Boolean.mk_sort z3)
  in
  BvarHashtbl.add domain.domain_bvar_terms var term;
  FuncDeclHashtbl.add domain.domain_term_bvars (Z3.Expr.get_func_decl term) var;
  term

let rvar_term domain var =
  try
    RvarHashtbl.find domain.domain_rvar_terms var
  with
  | Not_found ->
      if Rvar.is_temp var then
        new_temp_rvar domain var
      else
        invalid_arg (sprintf "Rational variable with id %d does not map to a term." (Rvar.id var))

let bvar_term domain var =
  try
    BvarHashtbl.find domain.domain_bvar_terms var
  with
  | Not_found ->
      if Bvar.is_temp var then
        new_temp_bvar domain var
      else
        invalid_arg (sprintf "Boolean variable with id %d does not map to a term." (Bvar.id var))

let make_domain z3 =
  let ctx = Ctx.make ~z3:z3 () in
  let domain =
    { domain_ctx = ctx;
      domain_rvar_terms = RvarHashtbl.create 16;
      domain_bvar_terms = BvarHashtbl.create 16;
      domain_term_rvars = FuncDeclHashtbl.create 16;
      domain_term_bvars = FuncDeclHashtbl.create 16;
      domain_next_rvar = 0;
      domain_next_bvar = 0 }  
  in
  Ctx.set_var_terms ctx
    (Ctx.make_var_terms
      (fun var -> rvar_term domain var)
      (fun var -> bvar_term domain var));
  domain

let func_decl_rvar domain func_decl =
  FuncDeclHashtbl.find domain.domain_term_rvars func_decl

let func_decl_bvar domain func_decl =
  FuncDeclHashtbl.find domain.domain_term_bvars func_decl

let new_func_decl_rvar domain func_decl =
  let z3 = domain_solver domain in
  let var = Rvar.make domain.domain_next_rvar in
  domain.domain_next_rvar <- domain.domain_next_rvar + 1;
  RvarHashtbl.add
    domain.domain_rvar_terms
    var
    (Z3.Expr.mk_const_f z3 func_decl);
  FuncDeclHashtbl.add domain.domain_term_rvars func_decl var;
  var

let new_func_decl_bvar domain func_decl =
  let z3 = domain_solver domain in
  let var = Bvar.make domain.domain_next_bvar in
  domain.domain_next_bvar <- domain.domain_next_bvar + 1;
  BvarHashtbl.add
    domain.domain_bvar_terms
    var
    (Z3.Expr.mk_const_f z3 func_decl);
  FuncDeclHashtbl.add domain.domain_term_bvars func_decl var;
  var

let make_func_decl_rvar domain func_decl =
  try
    func_decl_rvar domain func_decl
  with
  | Not_found ->
      new_func_decl_rvar domain func_decl

let make_func_decl_bvar domain func_decl =
  try
    func_decl_bvar domain func_decl
  with
  | Not_found ->
      new_func_decl_bvar domain func_decl

type space_t = {
  space_domain: domain_t;
  space_rvars: RvarHashset.t;
  space_bvars: BvarHashset.t 
}

let space_domain space =
  space.space_domain

let space_ctx space =
  domain_ctx (space_domain space)

let space_solver space =
  domain_solver (space_domain space)

let make_space domain func_decls = 
  let z3 = domain_solver domain in
  let rvars = RvarHashset.create 16 in
  let bvars = BvarHashset.create 16 in
  List.iter
    (fun fd ->
      if Solver.is_real_func_decl z3 fd || Solver.is_int_func_decl z3 fd then
        let var = make_func_decl_rvar domain fd in
        RvarHashset.add rvars var
      else if Solver.is_bool_func_decl z3 fd then
        let var = make_func_decl_bvar domain fd in
        BvarHashset.add bvars var
      else
        invalid_arg (sprintf
          "Only Int, Real, and Bool variables are supported. Got: \"%s\"."
          (Z3.FuncDecl.to_string fd)))
    func_decls;
  { space_domain = domain;
    space_rvars = rvars;
    space_bvars = bvars }

let space_union space1 space2 =
  let domain = space_domain space1 in
  check_arg
    (domain == (space_domain space2))
    "Spaces should be from the same domain";
  let rvars = RvarHashset.copy space1.space_rvars in
  RvarHashset.iter (RvarHashset.add rvars) space2.space_rvars;
  let bvars = BvarHashset.copy space1.space_bvars in
  BvarHashset.iter (BvarHashset.add bvars) space2.space_bvars;
  { space_domain = domain;
    space_rvars = rvars;
    space_bvars = bvars }

type elem_t = Poly.t

let mk_bot space =
  Poly.make_bot (space_ctx space)

let mk_top space =
  Poly.make_top (space_ctx space)

let join _ elem1 elem2 =
  Counter.inc_int join_count;
  Counter.add_duration join_time (fun () ->
    Poly.convex_hull elem1 elem2)

let meet _ elem1 elem2 =
  Counter.inc_int meet_count;
  Counter.add_duration meet_time (fun () ->
    Poly.assume_poly elem1 elem2)

let leq _ elem1 elem2 =
  Counter.inc_int leq_count;
  Counter.add_duration leq_time (fun () ->
  Poly.leq elem1 elem2)

let equal space elem1 elem2 =
  Counter.inc_int equal_count;
  Counter.add_duration equal_time (fun () ->
    Poly.equiv elem1 elem2)

let widen space elem1 elem2 =
  Counter.inc_int widen_count;
  Counter.add_duration widen_time (fun () ->
    Poly.widen elem1 elem2)

let format_space fmt space =
  pp_open_box fmt 0;
  pp_print_string fmt "()";
  pp_close_box fmt ()

(* NOTE: Modifies rvars_from, bvars_from. *)
let move_project_mutable_rvars_bvars rvars_from bvars_from builder space_to =  
  RvarHashset.iter (RvarHashset.remove rvars_from) space_to.space_rvars;
  BvarHashset.iter (BvarHashset.remove bvars_from) space_to.space_bvars;
  if RvarHashset.length rvars_from > 0 then begin
    Counter.inc_int eliminate_r_count;
    Counter.add_int eliminate_r_var_count (RvarHashset.length rvars_from);
    Counter.add_duration eliminate_r_time (fun () ->
      Builder.eliminate_iter_r builder RvarHashset.iter rvars_from)
  end else ();
  if BvarHashset.length bvars_from > 0 then begin
    Counter.inc_int eliminate_b_count;
    Counter.add_int eliminate_b_var_count (BvarHashset.length bvars_from);
    Counter.add_duration eliminate_b_time (fun () ->
      Builder.eliminate_iter_b builder BvarHashset.iter bvars_from)
  end else ();
  Builder.to_poly builder

let move_project space_from poly_from space_to =
  check_arg 
    ((space_domain space_from) == (space_domain space_to))
    "Spaces should be from the same domain";   
  let builder_from = Builder.of_poly poly_from in
  move_project_mutable_rvars_bvars
    (RvarHashset.copy space_from.space_rvars)
    (BvarHashset.copy space_from.space_bvars)
    builder_from
    space_to  

let move_rename_array space_from poly_from renames space_to =
  let domain = space_domain space_from in
  check_arg
    (domain == (space_domain space_to))
    "Spaces should be from the same domain";
  let solver = domain_solver domain in
  let rvar_renames = RvarHashtbl.create (Array.length renames) in
  let bvar_renames = BvarHashtbl.create (Array.length renames) in
  Array.iter
    (fun (fd_from, fd_to) ->
      if Solver.is_real_func_decl solver fd_from || Solver.is_int_func_decl solver fd_from then
        let var_from = func_decl_rvar domain fd_from in
        let var_to = func_decl_rvar domain fd_to in
        RvarHashtbl.add rvar_renames var_from var_to
      else if Solver.is_bool_func_decl solver fd_from then
        let var_from = func_decl_bvar domain fd_from in
        let var_to = func_decl_bvar domain fd_to in
        BvarHashtbl.add bvar_renames var_from var_to
      else
        invalid_arg (sprintf
          "Only Int, Real, and Bool variables are supported. Got: \"%s\"."
          (Z3.FuncDecl.to_string fd_from)))
    renames;  
  let renamed_rvars = RvarHashset.create (RvarHashset.length space_from.space_rvars) in
  let renamed_bvars = BvarHashset.create (BvarHashset.length space_from.space_bvars) in
  RvarHashset.iter
    (fun var ->
      let var_to =
        try
          RvarHashtbl.find rvar_renames var
        with
        | Not_found -> var
      in
      if not (RvarHashset.mem renamed_rvars var_to) then
        RvarHashset.add renamed_rvars var_to
      else
        invalid_arg "Renaming maps two variables to the same one.")
    space_from.space_rvars;
  BvarHashset.iter
    (fun var ->
      let var_to =
        try
          BvarHashtbl.find bvar_renames var
        with
        | Not_found -> var
      in
      if not (BvarHashset.mem renamed_bvars var_to) then
        BvarHashset.add renamed_bvars var_to
      else
        invalid_arg "Renaming maps two variables to the same one.")
    space_from.space_bvars;
  let rename_rvar var =
    try
      Some (RvarHashtbl.find rvar_renames var)
    with
    | Not_found -> Some var
  in
  let rename_bvar var =
    try
      Some (BvarHashtbl.find bvar_renames var)
    with
    | Not_found -> Some var
  in
  let builder_from = Builder.of_poly poly_from in
  Builder.rename_r builder_from rename_rvar;
  Builder.rename_b builder_from rename_bvar;
  move_project_mutable_rvars_bvars renamed_rvars renamed_bvars builder_from space_to

let term_of_elem _ elem =
  Poly.terms_poly (Poly.ensure_terms elem)

let format_elem _ fmt elem =
  Poly.format fmt elem

let to_string space elem =
    asprintf "%a" (format_elem space) elem

type cond_t =
  | EqCond of Eq.t
  | IneqCond of Ineq.t
  | BoolCond of BoolExpr.t
  | ArrayCond of cond_t DynArray.t

let cond_bool_expr cond =
  match cond with
  | BoolCond bool_expr -> bool_expr
  | _ -> invalid_arg "Condition should be Boolean."

let make_true_bool_cond ctx =
  BoolCond (BoolExpr.make_true ctx)

let make_false_bool_cond ctx =
  BoolCond (BoolExpr.make_false ctx)

let make_and_bool_cond cond1 cond2 =
  BoolCond (BoolExpr.make_and (cond_bool_expr cond1) (cond_bool_expr cond2))

let make_or_bool_cond cond1 cond2 =
  BoolCond (BoolExpr.make_or (cond_bool_expr cond1) (cond_bool_expr cond2))

let make_not_cond cond =
  match cond with
  | EqCond _ ->
    invalid_arg "Equality should not be under negation."
  | IneqCond ineq ->
    IneqCond (Ineq.neg ineq)
  | BoolCond bool_expr ->
    BoolCond (BoolExpr.make_not bool_expr)
  | ArrayCond _ ->
    invalid_arg "A term that produces a conjunction of conditions should not be under negation"

let rec aff_expr_of_term domain term =
  let solver = domain_solver domain in
  if Solver.is_int_numeral solver term then
    (* TODO: Support rational numerals *)
    AffExpr.of_const (Z.of_string (Solver.term_numeral_s solver term))
  else if Solver.is_real_to_int solver term then
    (* Just ignoring the conversion operator *)
    let args = Solver.term_args solver term in
    match args with
    | [arg] -> aff_expr_of_term domain arg
    | _ -> invalid_term "to_int should have one argument" (Solver.term_to_string solver term)
  else if Solver.is_int_to_real solver term then
    (* Just ignoring the conversion operator *)
    let args = Solver.term_args solver term in
    match args with
    | [arg] -> aff_expr_of_term domain arg
    | _ -> invalid_term "to_real should have one argument" (Solver.term_to_string solver term)
  else if Solver.is_unary_minus solver term then
    let args = Solver.term_args solver term in
    match args with
    | [arg] ->
      AffExpr.neg (aff_expr_of_term domain arg)
    | _ ->
      invalid_term "Unary minus should have one argument" (Solver.term_to_string solver term)
  else if Solver.is_plus solver term then
    let args = Solver.term_args solver term in
    match args with
    | head_arg :: tail_args ->
        List.fold_left
          (fun acc arg ->
            AffExpr.plus acc (aff_expr_of_term domain arg))
          (aff_expr_of_term domain head_arg)
          tail_args
    | _ ->
      invalid_term "Plus should have at least one argument" (Solver.term_to_string solver term)
  else if Solver.is_minus solver term then
    let args = Solver.term_args solver term in
    match args with
    | head_arg :: tail_args ->
        List.fold_left
          (fun acc arg ->
            AffExpr.minus acc (aff_expr_of_term domain arg))
          (aff_expr_of_term domain head_arg)
          tail_args
    | _ ->
      invalid_term "Minus should have at least one argument" (Solver.term_to_string solver term)
  else if Solver.is_uninterpreted_const solver term then
    (* A standalone uninterpreted constant is a Boolean variable. *)
    let func_decl = Solver.term_func_decl solver term in
    AffExpr.of_var (func_decl_rvar domain func_decl)
  else if Solver.is_mult solver term then
    let args = Solver.term_args solver term in
    match args with
    | head_arg :: tail_args ->
        List.fold_left
          (fun acc arg ->
            AffExpr.mul acc (aff_expr_of_term domain arg))
          (aff_expr_of_term domain head_arg)
          tail_args
    | _ ->
      invalid_term "Multiplicaton should have at least one argument" (Solver.term_to_string solver term)
  (** TODO: Multiplication. *)
  else
      invalid_term "Not supported" (Solver.term_to_string solver term)

let rec cond_of_expl_term domain term =
  let solver = domain_solver domain in  
  let ctx = domain_ctx domain in  
  let lin_cons_of_term make_lin_cons term =
    match Solver.term_args solver term with
    | [arg1; arg2] ->
        make_lin_cons (aff_expr_of_term domain arg1) (aff_expr_of_term domain arg2)
    | _ -> raise (Not_supported "Term should have two arguments")
  in  
  if Solver.is_true solver term then
    BoolCond (BoolExpr.make_true ctx)
  else if Solver.is_false solver term then
    BoolCond (BoolExpr.make_false ctx)
  else if Solver.is_uninterpreted_const solver term then
    (* A standalone uninterpreted constant is a Boolean variable. *)
    let func_decl = Solver.term_func_decl solver term in
    BoolCond (BoolExpr.of_var ctx (func_decl_bvar domain func_decl))
  else if Solver.is_not solver term then
    let not_arg = match Solver.term_args solver term with
    | [arg] -> arg
    | _ -> raise (Invalid_argument "Negation should have one argument")
    in
    make_not_cond (cond_of_expl_term domain not_arg)
  else if Solver.is_and solver term then
    List.fold_left
      (fun acc term -> make_and_bool_cond acc (cond_of_expl_term domain term))
      (make_true_bool_cond ctx)
      (Solver.term_args solver term)
  else if Solver.is_or solver term then
    List.fold_left
      (fun acc term -> make_or_bool_cond acc (cond_of_expl_term domain term))
      (make_false_bool_cond ctx)
      (Solver.term_args solver term)
  else if Solver.is_eq solver term then
    EqCond (lin_cons_of_term Eq.of_operands term)
  else if Solver.is_leq solver term then
    IneqCond (lin_cons_of_term (fun ae1 ae2 -> Ineq.of_operands ae2 ae1 Ineq.Geq) term)
  else if Solver.is_lt solver term then
    IneqCond (lin_cons_of_term (fun ae1 ae2 -> Ineq.of_operands ae2 ae1 Ineq.Gt) term)
  else if Solver.is_geq solver term then
    IneqCond (lin_cons_of_term (fun ae1 ae2 -> Ineq.of_operands ae1 ae2 Ineq.Geq) term)
  else if Solver.is_gt solver term then
    IneqCond (lin_cons_of_term (fun ae1 ae2 -> Ineq.of_operands ae1 ae2 Ineq.Gt) term)
  else
    invalid_term "Not supported." (Solver.term_to_string solver term)
      
let elem_of_expl_terms space terms =
  Counter.inc_int elem_of_expl_count;
  Counter.add_duration elem_of_expl_time (fun () ->
    let domain = space_domain space in
    let ctx = space_ctx space in
    let builder = Builder.make ctx in
    let rec assume_cond cond =
      match cond with
      | EqCond eq ->
          Builder.assume_eq builder (ImplEq.of_eq ctx eq)
      | IneqCond ineq ->
          Builder.assume_ineq builder (ImplIneq.of_ineq ctx ineq)
      | BoolCond bool_expr ->
          Builder.assume_b builder bool_expr
      | ArrayCond conds ->
          DynArray.iter assume_cond conds
    in
    List.iter
      (fun term -> assume_cond (cond_of_expl_term domain term))
      terms;
    Builder.to_poly builder
  )

let is_bot space elem =
  Poly.is_bot elem