open Batteries.DynArray

let for_all pred arr =
  let len = length arr in
  let rec for_all_rec i =
    if i >= len then
      true
    else
      (pred (get arr i)) && (for_all_rec (i + 1))
  in
  for_all_rec 0

let exists pred arr =
  let len = length arr in
  let rec exists_rec i =
    if i >= len then
      false
    else
      (pred (get arr i)) || (exists_rec (i + 1))
  in
  exists_rec 0

let iter_triangle fn arr =
  let len = length arr in
  for i = 0 to len - 1 do
    let ai = get arr i in
    for j = i + 1 to len - 1 do
      fn ai (get arr j)
    done
  done

let iter_cartesian fn arr1 arr2 =
  for i = 0 to (length arr1) - 1 do
    for j = 0 to (length arr2) - 1 do
      fn (get arr1 i) (get arr2 j)
    done
  done

let set_equal elem_equal arr1 arr2 =
  (length arr1 = length arr2) &&
  (for_all (fun elem1 -> exists (fun elem2 -> elem_equal elem1 elem2) arr2) arr1)

let swap_remove arr i =
  let len = length arr in
  if i <> len - 1 then
    set arr i (get arr (len - 1))
  else ();
  delete_last arr

let fold_lefti fn init arr =
  let acc = ref init in
  for i = 0 to (length arr - 1) do
    acc := fn !acc i (get arr i)
  done;
  !acc

let count pred arr =
  let len = length arr in
  let rec count_loop acc i =
    if i >= len then
      acc
    else begin
      let elem = get arr i in
      if pred elem then
        count_loop (acc + 1) (i + 1)
      else
        count_loop acc (i + 1)
    end
  in
  count_loop 0 0

let of_iter iter col =
  let result = create () in
  iter (fun elem -> add result elem) col;
  result

let sort cmp dynarray =
  (* TODO: This should sort inplace. Perhaps copy an array sorting function here. *)
  let array = to_array dynarray in
  Array.sort cmp array;
  for i = 0 to Array.length array - 1 do
    set dynarray i array.(i)
  done