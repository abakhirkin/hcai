(** *)

open Num
open Format
open FormatU

type symbol_t = {
  sym_name: string;
  sym_simple: bool
}

let mk_simple_symbol name = { sym_name = name; sym_simple = true }

let mk_quoted_symbol name = { sym_name = name; sym_simple = false }

type spec_const_t =
  | NumC of num
  | StrC of string

type index_t =
  | NumI of num
  | SymI of symbol_t

type ident_t = {
  id_name : symbol_t;
  id_indices : index_t list;
}

let mk_ident name indices =
  { id_name = name; id_indices = indices }

let mk_sym_ident name = mk_ident name []

let mk_simple_ident name =
  { id_name = mk_simple_symbol name; id_indices = [] }

module IdentOrd = struct
  type t = ident_t

  let compare = Pervasives.compare
end

module IdentHash = struct
  type t = ident_t

  let equal = (=)

  let hash = Hashtbl.hash
end

module IdentMap = Map.Make(IdentOrd)

module IdentHashtbl = Hashtbl.Make(IdentHash)

type sort_t = {
  sort_name: ident_t;
  sort_args: sort_t list
}

let mk_simple_sort name =
  { sort_name = mk_simple_ident name; sort_args = [] }

let bool_sort = mk_simple_sort "Bool"
let int_sort = mk_simple_sort "Int"
let string_sort = mk_simple_sort "String"
let real_sort = mk_simple_sort "Real"

type var_binding_t = {
  bind_name: symbol_t;
  bind_term: term_t
}
and sorted_var_t = {
  var_name: symbol_t;
  var_sort: sort_t
}
and apply_t = {
  apply_head: ident_t;
  apply_tail: term_t list;
}
and let_t = {
  let_binds: var_binding_t list;
  let_body: term_t;
}
and quantifier_t = {
  q_vars: sorted_var_t list;
  q_body: term_t;
}
and term_structure_t =
  | NumT of num
  | StrT of string
  | IdentT of ident_t
  | ApplyT of apply_t
  | LetT of let_t
  | ForallT of quantifier_t
  | ExistsT of quantifier_t
and term_t = {
  term_struct: term_structure_t;
}

let term_struct term = term.term_struct

let mk_term structure =
  { term_struct = structure }

let mk_num_term n = mk_term (NumT n) 
let mk_str_term s = mk_term (StrT s) 

let mk_const_term const =
  match const with
  | NumC n -> mk_num_term n
  | StrC s -> mk_str_term s

let mk_ident_term ident = mk_term (IdentT ident)

let mk_apply_term head tail =
  mk_term (ApplyT { apply_head = head; apply_tail = tail })

let mk_let_term binds body =
  mk_term (LetT { let_binds = binds; let_body = body })

let mk_forall_term vars body =
  mk_term (ForallT {q_vars = vars; q_body = body})

let mk_exists_term vars body =
  mk_term (ExistsT {q_vars = vars; q_body = body})

let true_term = mk_ident_term (mk_simple_ident "true")

let false_term = mk_ident_term (mk_simple_ident "false")

type attr_val_t =
  | NumAV of num
  | StrAV of string
  | SymAV of symbol_t

type attr_t = {
  attr_name: symbol_t;
  attr_value: attr_val_t option
}

type rel_t = {
  rel_name: symbol_t;
  rel_sorts: sort_t list
}

type fun_t = {
  fun_name: symbol_t;
  fun_sorts_from: sort_t list;
  fun_sort_to: sort_t
}

let fun_of_rel rel =
  { fun_name = rel.rel_name; fun_sorts_from = rel.rel_sorts; fun_sort_to = bool_sort }

let fun_of_var var =
  { fun_name = var.var_name; fun_sorts_from = []; fun_sort_to = var.var_sort }

type command_t =
  (* Common smtlib2 commands *)
  | SetInfoCmd of attr_t
  (* Smtlib2-only commands *)
  | AssertCmd of term_t
  (* Seahorn-only commands *)
  | DeclareRelCmd of rel_t
  | DeclareVarCmd of sorted_var_t
  | DeclareFunCmd of fun_t
  | RuleCmd of term_t
  | QueryCmd of term_t

let mk_const_attr_val const =
  match const with
  | NumC n -> NumAV n
  | StrC s -> StrAV s

let true_id = mk_simple_ident "true"
let false_id = mk_simple_ident "false"
let not_id = mk_simple_ident "not"
let impl_id = mk_simple_ident "=>"
let and_id = mk_simple_ident "and"
let or_id = mk_simple_ident "or"
let xor_id = mk_simple_ident "xor"
let eq_id = mk_simple_ident "="
let distinct_id = mk_simple_ident "distinct"
let ite_id = mk_simple_ident "ite"

let minus_id = mk_simple_ident "-"
let plus_id = mk_simple_ident "+"
let mult_id = mk_simple_ident "*"
let leq_id = mk_simple_ident "<="
let lt_id = mk_simple_ident "<"
let geq_id = mk_simple_ident ">="
let gt_id = mk_simple_ident ">"

(** {2 Formatting } *)


let string_of_symbol s =
  if s.sym_simple then
    s.sym_name
  else
    String.concat "" ["|"; s.sym_name; "|"]

let format_symbol f s =
  pp_open_box f 0;
  if s.sym_simple then
    pp_print_string f s.sym_name
  else (
    pp_print_string f "|";
    pp_print_string f s.sym_name;
    pp_print_string f "|"
  );
  pp_close_box f ()

let format_index f index =
  pp_open_box f 0;
  (match index with
  | NumI n -> pp_print_string f (string_of_num n)
  | SymI s -> format_symbol f s
  );
  pp_close_box f ()

let format_ident f ident =
  pp_open_box f 0;
  if List.length ident.id_indices = 0 then
    format_symbol f ident.id_name
  else (
    pp_print_string f "(";
    pp_print_string f "_";
    pp_print_space f ();
    format_symbol f ident.id_name;
    pp_print_space f ();
    format_list format_index f ident.id_indices;
    pp_print_string f ")"
  );
  pp_close_box f ()

let format_string f s =
  pp_open_box f 0;
  pp_print_string f "\"";
  let quote_rx = Str.regexp "\"" in
  pp_print_string f (Str.global_replace quote_rx "\"\"" s);
  pp_print_string f "\"";
  pp_close_box f ()

let rec format_sort f sort =
  pp_open_box f 0;
  (match sort.sort_args with
  | [] -> format_ident f sort.sort_name
  | _ ->
      pp_print_string f "(";
      format_ident f sort.sort_name;
      pp_print_space f ();
      format_list format_sort f sort.sort_args;
      pp_print_string f ")"
  );
  pp_close_box f ()

let string_of_sort sort =
  asprintf "%a" format_sort sort

let format_sorted_var f var =
  pp_open_box f 0;
  pp_print_string f "(";
  format_symbol f var.var_name;
  pp_print_space f ();
  format_sort f var.var_sort;
  pp_print_string f ")";
  pp_close_box f ()

let rec format_term f t =
  pp_open_box f 0;
  (match term_struct t with
  | IdentT ident -> format_ident f ident
  | NumT n -> pp_print_string f (string_of_num n)
  | StrT s -> format_string f s
  | ApplyT apply ->
      pp_open_box f 1;
      pp_print_string f "(";
      format_ident f apply.apply_head;
      pp_print_string f " ";
      format_list format_term f apply.apply_tail;
      pp_print_string f ")";
      pp_close_box f ()
  | LetT l ->
      pp_open_box f 5;
      pp_print_string f "(";
      pp_print_string f "let";
      pp_print_string f " ";
      pp_print_string f "(";
      format_list format_binding f l.let_binds;
      pp_print_string f ")";
      pp_print_space f ();
      format_term f l.let_body;
      pp_print_string f ")";
      pp_close_box f ()
  | ForallT q ->
      pp_open_box f 8;
      pp_print_string f "(";
      pp_print_string f "forall";
      pp_print_string f " ";
      pp_print_string f "(";
      format_list format_sorted_var f q.q_vars;
      pp_print_string f ")";
      pp_print_space f ();
      format_term f q.q_body;
      pp_print_string f ")";
      pp_close_box f ()
  | ExistsT q ->
      pp_open_box f 8;
      pp_print_string f "(";
      pp_print_string f "exists";
      pp_print_string f " ";
      pp_print_string f "(";
      format_list format_sorted_var f q.q_vars;
      pp_print_string f ")";
      pp_print_space f ();
      format_term f q.q_body;
      pp_print_string f ")";
      pp_close_box f ()
  );
  pp_close_box f ()

and format_binding f b =
  pp_open_box f 0;
  pp_print_string f "(";
  format_symbol f b.bind_name;
  pp_print_string f " ";
  format_term f b.bind_term;
  pp_print_string f ")";
  pp_close_box f ()

let format_attr_val f value =
  pp_open_box f 0;
  (match value with
  | NumAV n -> pp_print_string f (string_of_num n)
  | StrAV s -> format_string f s
  | SymAV s -> format_symbol f s
  );
  pp_close_box f ()

let format_attr f attr =
  pp_open_box f 0;
  (match attr.attr_value with
  | None ->
      pp_print_string f ":";
      format_symbol f attr.attr_name
  | Some value -> 
      pp_print_string f ":";
      format_symbol f attr.attr_name;
      pp_print_space f ();
      format_attr_val f value
  );
  pp_close_box f ()

let format_rel f rel =
  pp_open_box f 0;
  format_symbol f rel.rel_name;
  pp_print_space f ();
  pp_print_string f "(";
  format_list format_sort f rel.rel_sorts;
  pp_print_string f ")";
  pp_close_box f ()

let format_fun f fn =
  pp_open_box f 0;
  format_symbol f fn.fun_name;
  pp_print_space f ();
  pp_print_string f "(";
  format_list format_sort f fn.fun_sorts_from;
  pp_print_string f ")";
  pp_print_space f ();
  format_sort f fn.fun_sort_to;
  pp_close_box f ()

let format_command f cmd =
  pp_open_box f 0;
  pp_print_string f "(";
  (match cmd with
  | SetInfoCmd attr ->
      pp_print_string f "set-info";
      pp_print_space f ();
      format_attr f attr
  | AssertCmd term ->
      pp_print_string f "assert";
      pp_print_string f " ";
      format_term f term
  | DeclareRelCmd rel ->
      pp_print_string f "declare-rel";
      pp_print_string f " ";
      format_rel f rel
  | DeclareVarCmd var ->
      pp_print_string f "declare-var";
      pp_print_string f " ";
      format_symbol f var.var_name;
      pp_print_space f ();
      format_sort f var.var_sort
  | DeclareFunCmd fn ->
      pp_print_string f "declare-fun";
      pp_print_string f " ";
      format_fun f fn
  | RuleCmd term ->
      pp_print_string f "rule";
      pp_print_string f " ";
      format_term f term
  | QueryCmd term ->
      pp_print_string f "query";
      pp_print_string f " ";
      format_term f term
  );
  pp_print_string f ")";
  pp_close_box f ()

let string_of_term term =
  let buf = Buffer.create 16 in
  let f = Format.formatter_of_buffer buf in
  Format.fprintf f "%a@?" format_term term;
  Buffer.contents buf
