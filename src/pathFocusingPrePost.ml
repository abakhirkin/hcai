open Format
open ExceptionU

module Array = Batteries.Array

module Make(Run: AbstIntRun.T) = struct

  module Ctx = Run.Ctx
  module ClauseSet = Run.ClauseSet
  module Domain = Run.Domain
  module Pred = ClauseSet.Pred
  module PredHashtbl = Pred.Hashtbl
  module PredApp = ClauseSet.PredApp
  module HornClause = ClauseSet.HornClause
  module BackwClause = ClauseSet.BackwClause
  module Solver = ClauseSet.Solver  
  module FuncDeclHashtbl = Solver.FuncDeclHashtbl
  module FuncDeclHashset = Hashset.Make(Solver.FuncDeclOrdHash)
  
  type expl_t = {
    expl_is_expl: bool;
    expl_terms: Solver.term_t list;
    (* Whether the term is pure Boolean. *)
    expl_is_bool: bool
  }

  let expl_is_expl expl =
    expl.expl_is_expl

  let expl_terms expl =
    expl.expl_terms

  let expl_is_bool expl =
    expl.expl_is_bool

  let mk_expl is_expl terms is_bool =
    { expl_is_expl = is_expl; expl_terms = terms; expl_is_bool = is_bool }

  let iter_expl_terms f expl =
    List.iter f expl.expl_terms

  let rec explain solver model term =
    if Solver.is_true solver term then
      mk_expl true [] true
    else if Solver.is_false solver term then
      mk_expl false [] true
    else if Solver.is_not solver term then begin
      let arg_term = match Solver.term_args solver term with
      | [arg] -> arg
      | _ -> raise (Invalid_argument "Negation should have one argument")
      in
      if Solver.is_true solver arg_term then
        mk_expl false [] true
      else if Solver.is_false solver arg_term then
        mk_expl true [] true
      else
        (* An uninterpreted const is a Boolean atom, and everything else
           we assume to be a numeric constraint. *)
        let is_bool = Solver.is_uninterpreted_const solver arg_term in
        match Solver.eval_bool  ~completion:true solver model arg_term with
        | Some false -> mk_expl true [term] is_bool
        | Some true -> mk_expl false [term] is_bool
        | None -> failwith "Model completion should be enabled."
    end else if Solver.is_and solver term then begin
      let arg_terms = Solver.term_args solver term in
      let arg_expls = List.map (explain solver model) arg_terms in
      let all_are_bool = List.for_all expl_is_bool arg_expls in
      let all_are_expl = List.for_all expl_is_expl arg_expls in
      if all_are_expl then
        mk_expl true (List.concat (List.map expl_terms arg_expls)) all_are_bool
      else if all_are_bool then
        mk_expl false [term] true
      else
        mk_expl false [] false
    end else if Solver.is_or solver term then begin
      (* For a disjunction, we find the first argument that is 'true' in the
         model. If it is not pure Boolean, we return its explanation. If it is
         pure boolean, we return a disjunction of all pure Boolean arguments of
         the disjunction. *)
      let arg_terms = Solver.term_args solver term in
      let arg_terms_expls = List.map
        (fun term -> (term, (explain solver model term)))
        arg_terms
      in
      try
        let (_, first_expl) = List.find
          (fun (t, e) -> expl_is_expl e)
          arg_terms_expls
        in        
        if expl_is_bool first_expl then
          let rec bool_terms acc terms_expls =
            match terms_expls with
            | [] -> acc
            | (t, e) :: te_tail ->
                if expl_is_bool e then
                  bool_terms (t::acc) te_tail
                else
                  bool_terms acc te_tail
          in
          let arg_bool_terms = bool_terms [] arg_terms_expls in
          mk_expl true [Solver.mk_or solver arg_bool_terms] true
        else
          first_expl      
      with
      | Not_found ->
        let all_are_bool = List.for_all
          (fun (_, e) -> expl_is_bool e)
          arg_terms_expls
        in
        if all_are_bool then
          mk_expl false [term] true
        else
          mk_expl false [] false
    end else
      (* An uninterpreted const is a Boolean atom, and everything else
         we assume to be a numeric constrain. *)
      let is_bool = Solver.is_uninterpreted_const solver term in
      match Solver.eval_bool ~completion:true solver model term with
      | Some true -> mk_expl true [term] is_bool
      | Some false -> mk_expl false [term] is_bool
      | None -> failwith "Model completion should be enabled."  

  let term_free_vars solver term =
    let free_vars = FuncDeclHashtbl.create 16 in
    let rec collect_free_vars term =
      if Solver.is_interpreted_const solver term then
        ()
      else if Solver.is_uninterpreted_const solver term then
        FuncDeclHashtbl.replace free_vars (Solver.term_func_decl solver term) ()
      else if Solver.is_apply solver term then
        List.iter collect_free_vars (Solver.term_args solver term)
      else
        raise (Not_supported "Only constants and applications are supported.")      
    in
    collect_free_vars term;
    free_vars

  let terms_free_vars solver terms =
    let free_vars = FuncDeclHashset.create 16 in
    List.iter
      (fun term ->
        FuncDeclHashtbl.iter
          (fun func_decl _ ->
            FuncDeclHashset.add free_vars func_decl)
          (term_free_vars solver term))
      terms;
    FuncDeclHashset.to_list free_vars

  let space_of_expl domain expl =
    let solver = Domain.domain_solver domain in
    let free_vars = terms_free_vars solver (expl_terms expl) in
    Domain.make_space domain free_vars

  let elem_of_expl space expl =
    if expl_is_expl expl then
      Domain.elem_of_expl_terms space (expl_terms expl)
    else
      raise (Invalid_argument "Explanation says that the term evaluates to false")

  let project_on_pred run pred e_dom elem =    
    let h_dom = Run.pred_space run pred in
    Domain.move_project e_dom elem h_dom

  (* TODO: These functions should accept Run.PredData.t instead of Pred.t. *)
  let mk_pred_app_term_1 run pred pred_args pred_elem =    
    let solver = Run.solver run in    
    let h_dom = Run.pred_space run pred in
    let pred_term = Domain.term_of_elem h_dom pred_elem in
    Solver.substitute solver pred_term (Pred.head_args pred) pred_args

  let mk_pred_app_term ctx pred_app pred_elem =
    mk_pred_app_term_1 ctx (PredApp.pred pred_app) (PredApp.args pred_app) pred_elem

  let mk_pred_term run pred pred_elem =
    let h_dom = Run.pred_space run pred in
    Domain.term_of_elem h_dom pred_elem

  let mk_clause_false_term ?(below=None) run hc head_elem =
    let solver = Run.solver run in
    let below_term =
      match below with
      | None -> Solver.mk_true solver
      | Some below_elem -> mk_pred_term run (HornClause.head hc) below_elem
    in
    let body_app_terms = HornClause.fold_body_apps
      (fun acc pred_app ->
        let elem = Run.pred_elem run (PredApp.pred pred_app) in
        mk_pred_app_term run pred_app elem :: acc)
      []
      hc
    in
    let nhead_term = Solver.mk_not solver (mk_pred_term run (HornClause.head hc) head_elem) in
    let body_apps_term = Solver.mk_and solver (below_term :: body_app_terms) in
    let constr_term = HornClause.constr hc in
    (body_apps_term, constr_term, nhead_term)

  let mk_backward_clause_false_term run backw_clause head_elem =
    (* TODO: Need to document how this works and interacts with mk_backward_clauses. *)
    let solver = Run.solver run in
    let hc = BackwClause.clause backw_clause in
    let fwd_hc_o = BackwClause.forward_clause backw_clause in
    let below_app_terms =
      match fwd_hc_o with
      | Some fwd_hc ->
        HornClause.fold_body_apps
          (fun acc pred_app ->
            let elem = Run.pred_below_elem run (PredApp.pred pred_app) in
            mk_pred_app_term run pred_app elem :: acc)
          []
          fwd_hc
      | None ->
          let pred = (HornClause.head hc) in 
          let elem = Run.pred_below_elem run pred in
          [mk_pred_term run pred elem]
    in
    (* NOTE: Backward clauses have at most one predicate application in the body. *)
    let body_app_terms = HornClause.fold_body_apps
      (fun acc pred_app ->
        let elem = Run.pred_elem run (PredApp.pred pred_app) in
        mk_pred_app_term run pred_app elem :: acc)
      []
      hc
    in
    let nhead_term = Solver.mk_not solver (mk_pred_term run (HornClause.head hc) head_elem) in
    let body_apps_term =
      Solver.mk_and solver (body_app_terms @ below_app_terms)
    in
    let constr_term = HornClause.constr hc in
    (body_apps_term, constr_term, nhead_term)

  let mk_pred_elems_term run pred_elems =
    let solver = Run.solver run in
    let clause_set = Run.clause_set run in
    let terms = ClauseSet.fold_preds
      (fun acc pred ->
        let dom = Run.pred_space run pred in
        let elem = PredHashtbl.find pred_elems pred in
        (Solver.mk_eq
          solver
          [ (Solver.mk_apply solver (Pred.func_decl pred) (Pred.head_args pred));
            (Domain.term_of_elem dom elem) ]) :: acc
      )
      []
      clause_set
    in
    Solver.mk_and solver terms

(*
  let mk_term_fragment run check_term explain_term pred =
    let domain = Run.domain run in
    let solver = Run.solver run in
    Opt.do_if Opt.print_smt_queries (fun () ->
      eprintf "%a@." (Solver.format_term solver) check_term
    );
    match Solver.get_model solver check_term with
    | Solvers.SatM model ->
        Opt.do_if Opt.print_smt_queries (fun () -> eprintf "; sat@.");
        Opt.do_if Opt.print_explanations (fun() ->
          eprintf "Model: %a@." (Solver.format_model solver) model
        );
        (* TODO: Figure out if this needs to be check_term or explain_term. *)
        (* Using check_term may make widening more precise, but maybe need a setting. *)
        (* TODO: Perhaps the numeric constraints of clauses can be pre-converted to
           nnf, and when conjoined with interpretations of predicates, they will
           still be in nnf. *)
        let nnf_explain_term = Solver.to_nnf solver explain_term in
        Opt.do_if Opt.print_explanations (fun() ->
          eprintf "Term to explain: %a@." (Solver.format_term solver) nnf_explain_term;
        );
        let expl = explain solver model nnf_explain_term in
        Opt.do_if Opt.print_explanations (fun() ->
          eprintf "Completed model: %a@." (Solver.format_model solver) model
        );
        Opt.do_if Opt.print_explanations (fun() ->
          eprintf "Explanation terms:@.";
          iter_expl_terms
            (fun expl_term -> eprintf "%a@." (Solver.format_term solver) expl_term)
            expl
        );
        let e_dom = space_of_expl domain expl in
        let e_elem = elem_of_expl e_dom expl in        
        Opt.do_if Opt.print_explanations (fun() ->
          eprintf "Explanation elem: %a@." (Domain.format_elem e_dom) e_elem
        );
        let p_elem = project_on_pred run pred e_dom e_elem in
        let p_dom = Run.pred_space run pred in
        (* TODO: This may not be needed here, only for unknown result. *)
        let below_elem = Run.pred_below_elem run pred in
        Some (Domain.meet p_dom p_elem below_elem)
    | Solvers.UnsatM ->
        Opt.do_if Opt.print_smt_queries (fun () -> eprintf "; unsat@.");
        None
    | Solvers.UnknownM ->
        (* Solver is confused or timed out, so just return top. *)
        (* TODO: Log this and/or have a setting to treat unknowns as errors. *)
        Opt.do_if Opt.print_smt_queries (fun () -> eprintf "; unknown@.");
        let below_elem = Run.pred_below_elem run pred in
        Some below_elem *)

  let make_expl_elem run check_term explain_term head_pred =
    let domain = Run.domain run in
    let solver = Run.solver run in
    Opt.do_if Opt.print_smt_queries (fun () ->
      eprintf "%a@." (Solver.format_term solver) check_term
    );
    match Solver.get_model solver check_term with
    | Solvers.SatM model ->
        Opt.do_if Opt.print_smt_queries (fun () -> eprintf "; sat@.");
        Opt.do_if Opt.print_explanations (fun() ->
          eprintf "Model: %a@." (Solver.format_model solver) model
        );
        (* TODO: Perhaps explain_term is already in nnf. *)
        let nnf_explain_term = Solver.to_nnf solver explain_term in
        Opt.do_if Opt.print_explanations (fun() ->
          eprintf "Term to explain: %a@." (Solver.format_term solver) nnf_explain_term;
        );
        let expl = explain solver model nnf_explain_term in
        Opt.do_if Opt.print_explanations (fun() ->
          eprintf "Completed model: %a@." (Solver.format_model solver) model
        );
        Opt.do_if Opt.print_explanations (fun() ->
          eprintf "Explanation terms:@.";
          iter_expl_terms
            (fun expl_term -> eprintf "%a@." (Solver.format_term solver) expl_term)
            expl
        );
        let e_space = space_of_expl domain expl in
        let e_elem = elem_of_expl e_space expl in        
        Opt.do_if Opt.print_explanations (fun() ->
          eprintf "Explanation elem: %a@." (Domain.format_elem e_space) e_elem
        );
        Some (e_space, e_elem)        
    | Solvers.UnsatM ->
        Opt.do_if Opt.print_smt_queries (fun () -> eprintf "; unsat@.");
        None
    | Solvers.UnknownM ->
        (* Solver is confused or timed out, so just return top. *)
        (* TODO: Log this and/or have a setting to treat unknowns as errors. *)
        Opt.do_if Opt.print_smt_queries (fun () -> eprintf "; unknown@.");
        let below_elem = Run.pred_below_elem run head_pred in
        Some (Run.pred_space run head_pred, below_elem)

  let pred_app_space domain pred_app =
    let solver = Domain.domain_solver domain in
    let arg_func_decls = List.map
      (Solver.term_func_decl solver)
      (PredApp.args pred_app)
    in
    Domain.make_space domain arg_func_decls

  let pred_app_elem run space pred_app =
    let solver = Run.solver run in
    let pred = PredApp.pred pred_app in              
    let elem = Run.pred_elem run pred in              
    let renames = Array.map2
      (fun head_arg app_arg ->
        (Solver.term_func_decl solver head_arg, Solver.term_func_decl solver app_arg))
      (Array.of_list (Pred.head_args pred))
      (Array.of_list (PredApp.args pred_app))
    in              
    Domain.move_rename_array (Run.pred_space run pred) elem renames space
  
  let pred_app_below_elem run space pred_app =
    let solver = Run.solver run in
    let pred = PredApp.pred pred_app in              
    let elem = Run.pred_below_elem run pred in              
    let renames = Array.map2
      (fun head_arg app_arg ->
        (Solver.term_func_decl solver head_arg, Solver.term_func_decl solver app_arg))
      (Array.of_list (Pred.head_args pred))
      (Array.of_list (PredApp.args pred_app))
    in              
    Domain.move_rename_array (Run.pred_space run pred) elem renames space

  let post_fragment_1 run hc head_pred head_elem =
    let domain = Run.domain run in
    let solver = Run.solver run in
    let below_elem = Run.pred_below_elem run head_pred in
    let (body_apps_term, constr_term, nhead_term) =
      mk_clause_false_term
        ~below:(Some below_elem) run hc head_elem in    
    match make_expl_elem
      run
      (Solver.mk_and solver [body_apps_term; constr_term; nhead_term])
      constr_term
      head_pred
    with
    | None -> None
    | Some (expl_space, expl_elem) ->
        let head_space = Run.pred_space run head_pred in
        let hc_space = ref (Domain.space_union expl_space head_space) in
        (* TODO: Perhaps some part of this space can be pre-computed, here and in pre_fragment_1. *)
        HornClause.iter_body_apps
          (fun pred_app ->
            hc_space := Domain.space_union !hc_space (pred_app_space domain pred_app))
          hc;
        let hc_space = !hc_space in
        let hc_elem = ref
          (Domain.meet hc_space
            (Domain.move_project expl_space expl_elem hc_space)
            (Domain.move_project head_space below_elem hc_space))
        in
        HornClause.iter_body_apps
          (fun pred_app ->                            
            hc_elem := Domain.meet hc_space !hc_elem (pred_app_elem run hc_space pred_app))
          hc;        
        let result = Domain.move_project hc_space !hc_elem head_space in
        if Domain.is_bot head_space result then
          failwith "Path focusig found a satisfiable flowpipe, but domain produced an empty element."
        else ();
        Some result

  (* TODO: Need to review this and confirm that this is correct. *)
  let pre_fragment_1 run backw_clause head_pred pred_elem =
    (* TODO: Need to document how this works and interacts with mk_backward_clauses. *)
    let domain = Run.domain run in
    let solver = Run.solver run in
    let (body_apps_term, constr_term, nhead_term) =
      mk_backward_clause_false_term run backw_clause pred_elem
    in
    match make_expl_elem
      run
      (Solver.mk_and solver [body_apps_term; constr_term; nhead_term])
      constr_term
      head_pred
    with
    | None -> None
    | Some (expl_space, expl_elem) ->
        let head_space = Run.pred_space run head_pred in
        let hc_space = ref (Domain.space_union expl_space head_space) in
        (* NOTE: Backward clause has at most one predicate applicatoin in the body. *)
        HornClause.iter_body_apps
          (fun pred_app ->
            hc_space := Domain.space_union !hc_space (pred_app_space domain pred_app))
          (BackwClause.clause backw_clause);        
        begin match BackwClause.forward_clause backw_clause with
        | None -> ()
        | Some fwd_clause ->
            HornClause.iter_body_apps
              (fun pred_app ->
                hc_space := Domain.space_union !hc_space (pred_app_space domain pred_app))
              fwd_clause        
        end;
        let hc_space = !hc_space in
        let hc_elem = ref (Domain.move_project expl_space expl_elem hc_space) in        
        HornClause.iter_body_apps
          (fun pred_app ->
            let pred_app_elem = pred_app_elem run hc_space pred_app in            
            hc_elem := Domain.meet hc_space !hc_elem pred_app_elem)
          (BackwClause.clause backw_clause);
        begin match BackwClause.forward_clause backw_clause with
        | None -> ()
        | Some fwd_clause ->
            HornClause.iter_body_apps
              (fun pred_app ->
                let pred_app_elem = pred_app_below_elem run hc_space pred_app in                
                hc_elem := Domain.meet hc_space !hc_elem pred_app_elem)
              fwd_clause
        end;        
        Some (Domain.move_project hc_space !hc_elem head_space)   

  type t = unit

  let make _ = ()

  let join_post () run pred pred_elem =
    let clause_set = Run.clause_set run in
    let dom = Run.pred_space run pred in
    let below_elem = Run.pred_below_elem run pred in   
    let acc_elem = ref pred_elem in
    ClauseSet.iter_head_clauses
      (fun clause ->
        let rec join_fragment_rec () =
          match post_fragment_1 run clause pred !acc_elem with
          | Some elem ->
            acc_elem := Domain.meet dom (Domain.join dom !acc_elem elem) below_elem;
            Opt.do_if Opt.print_kleene (fun () ->
              eprintf "Join with: %a@." (Domain.format_elem dom) elem;
              eprintf "New value: %a@." (Domain.format_elem dom) !acc_elem
            );
            join_fragment_rec ()
          | None -> ()
        in
        join_fragment_rec ())      
      clause_set pred;
    !acc_elem

  let join_pre () run pred pred_elem =
    let clause_set = Run.clause_set run in
    let dom = Run.pred_space run pred in
    let below_elem = Run.pred_below_elem run pred in
    let acc_elem = ref pred_elem in    
    ClauseSet.iter_backw_head_clauses
      (fun clause ->
        let rec join_fragment_rec () =
          match pre_fragment_1 run clause pred !acc_elem with
          | Some elem ->
            acc_elem := Domain.meet dom (Domain.join dom !acc_elem elem) below_elem;
            Opt.do_if Opt.print_kleene (fun () ->
              eprintf "Join with: %a@." (Domain.format_elem dom) elem;
              eprintf "New value: %a@." (Domain.format_elem dom) !acc_elem
            );
            join_fragment_rec ()
          | None -> ()
        in
        join_fragment_rec ())      
      clause_set pred;
    !acc_elem
end