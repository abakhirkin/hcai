open ExceptionU
open Bddapron
open Format
open FormatU
open U

module StringHashtbl = StringU.StringHashtbl

module Mk(SolverA: Solvers.T) = struct
  module Solver = SolverA

  type term_t = Solver.term_t

  (* TODO: Try to have func_decl_t as symbol instead. *)
  type bddapron_symbol_t = string

  type apron_man_t = Polka.strict Polka.t

  type domain_t = {
    domain_solver: Solver.t;
    domain_cudd: Cudd.Man.v Cudd.Man.t;
    domain_cond: bddapron_symbol_t Cond.t;    
    domain_apron_m: apron_man_t Apron.Manager.t;
    domain_m: (bddapron_symbol_t, apron_man_t) Bdddomain0.man;
    domain_var_func_decls: Solver.func_decl_t StringHashtbl.t
  }

  let domain_solver domain =
    domain.domain_solver

  let domain_cudd domain =
    domain.domain_cudd

  let domain_cond domain =
    domain.domain_cond

  let domain_m domain = 
    domain.domain_m

  let domain_apron_m domain =
    domain.domain_apron_m
  
  let func_decl_var domain func_decl =
    Solver.func_decl_name (domain_solver domain) func_decl

  let var_func_decl domain var =
    StringHashtbl.find domain.domain_var_func_decls var

  let make_func_decl_var domain func_decl =
    let var = func_decl_var domain func_decl in
    try
      let fd = StringHashtbl.find domain.domain_var_func_decls var in
      if not (Solver.func_decl_equal fd func_decl) then
        invalid_arg "Variable with the same name and different sort was created before."
      else ();
    with
    | Not_found ->
      StringHashtbl.replace domain.domain_var_func_decls var func_decl

  let make_domain solver =
    let cudd = Cudd.Man.make_v () in
    let cond = Cond.make Env.string_symbol cudd in
    let apron_m = Polka.manager_alloc_strict () in
    let m = Bdddomain0.make_man apron_m in
    { domain_solver = solver;
      domain_cudd = cudd;
      domain_cond = cond;
      domain_apron_m = apron_m;
      domain_m = m;
      domain_var_func_decls = StringHashtbl.create 16 }

  type space_t = {
    space_domain: domain_t;
    space_env: bddapron_symbol_t Env.t;
  }

  let space_domain space = 
    space.space_domain

  let space_env space =
    space.space_env
  
  let space_solver space =
    domain_solver (space_domain space)

  type elem_t = apron_man_t Bdddomain0.t  
  
  let term_of_var space var =
    Solver.mk_const (space_solver space) (var_func_decl (space_domain space) var)

  let make_space3 domain int_func_decls real_func_decls bool_func_decls =    
    let env = Env.make Env.string_symbol (domain_cudd domain) in    
    List.iter (make_func_decl_var domain) int_func_decls;
    List.iter (make_func_decl_var domain) real_func_decls;
    List.iter (make_func_decl_var domain) bool_func_decls;
    let int_vars = List.map (fun fd -> (func_decl_var domain fd, `Int)) int_func_decls in
    let real_vars = List.map (fun fd -> (func_decl_var domain fd, `Real)) real_func_decls
    in
    let bool_vars = List.map (fun fd -> (func_decl_var domain fd, `Bool)) bool_func_decls
    in
    let _ = Env.add_vars_with env (List.concat [int_vars; real_vars; bool_vars]) in    
    { space_domain = domain;
      space_env = env }

  let make_space ctx vars =    
    let solver = domain_solver ctx in
    let int_vars = List.filter (Solver.is_int_func_decl solver) vars in
    let real_vars = List.filter (Solver.is_real_func_decl solver) vars in
    let bool_vars = List.filter (Solver.is_bool_func_decl solver) vars in
    make_space3 ctx int_vars real_vars bool_vars

  let space_union space1 space2 =    
    let domain = space_domain space1 in
    check_arg
      (domain == (space_domain space2))
      "Spaces should be from the same domain";
    let env1 = space_env space1 in
    let env2 = space_env space2 in
    let env = Env.copy env1 in    
    PSette.iter
      (fun var ->
        if not (Env.mem_var env var) then
          ignore (Env.add_vars_with env [(var, Env.typ_of_var env2 var)])
        else ())
      (Env.vars env2);
    { space_domain = domain;
      space_env = env }

  let mk_bot space =
    let domain = space_domain space in
    Bdddomain0.bottom (domain_m domain) (space_env space)

  let mk_top space =
    let domain = space_domain space in
    Bdddomain0.top (domain_m domain) (space_env space)

  let join space elem1 elem2 =
    let domain = space_domain space in
    Bdddomain0.join (domain_m domain) elem1 elem2

  let meet space elem1 elem2 =
    let domain = space_domain space in
    Bdddomain0.meet (domain_m domain) elem1 elem2

  let widen space elem_smaller elem_greater =
    let domain = space_domain space in
    Bdddomain0.widening (domain_m domain) elem_smaller elem_greater 

  let leq space elem_smaller elem_greater =
    let domain = space_domain space in
    Bdddomain0.is_leq (domain_m domain) elem_smaller elem_greater

  let equal space elem_smaller elem_greater =
    let domain = space_domain space in
    Bdddomain0.is_eq (domain_m domain) elem_smaller elem_greater

  let mk_int_s dom s =
    let coeff = Apron.Coeff.s_of_mpqf (Mpqf.of_string s) in
    Apron.Texpr0.cst coeff

  let mk_arith_var space var_name =
    let dim = Env.aprondim_of_var (space_env space) var_name in
    Apron.Texpr0.dim dim

  let rec binop_of_list mk_binop args =
    match args with
    | [] | [_] ->
        raise (Invalid_argument "Binary operation should have at least two arguments.")
    | [arg1; arg2] -> mk_binop arg1 arg2
    | arg::tail_args -> mk_binop arg (binop_of_list mk_binop tail_args)

  let mk_texpr_binop op texpr1 texpr2 =
    (* This is what Bddapron does by default:
       real type with 'Rnd' rounding *)
    Apron.Texpr0.binop op Apron.Texpr0.Real Apron.Texpr0.Rnd texpr1 texpr2

  let mk_texpr_unop op texpr =
    Apron.Texpr0.unop op texpr Apron.Texpr0.Real Apron.Texpr0.Rnd

  let mk_plus dom texprs =
    binop_of_list (mk_texpr_binop Apron.Texpr0.Add) texprs

  let mk_minus dom texprs =
    binop_of_list (mk_texpr_binop Apron.Texpr0.Sub) texprs

  let mk_unary_minus dom texpr =
    mk_texpr_unop Apron.Texpr0.Neg texpr

  let mk_mult dom texprs =
    binop_of_list (mk_texpr_binop Apron.Texpr0.Mul) texprs

  let mk_eq dom texpr1 texpr2 =
    Apron.Tcons0.make (mk_minus dom [texpr1; texpr2]) Apron.Tcons0.EQ

  let mk_leq dom texpr1 texpr2 =
    Apron.Tcons0.make (mk_minus dom [texpr2; texpr1]) Apron.Tcons0.SUPEQ
    
  let mk_lt dom texpr1 texpr2 =
    Apron.Tcons0.make (mk_minus dom [texpr2; texpr1]) Apron.Tcons0.SUP

  let mk_geq dom texpr1 texpr2 =
    Apron.Tcons0.make (mk_minus dom [texpr1; texpr2]) Apron.Tcons0.SUPEQ

  let mk_gt dom texpr1 texpr2 =
    Apron.Tcons0.make (mk_minus dom [texpr1; texpr2]) Apron.Tcons0.SUP

  let mk_bool_var space var_name =
    let domain = space_domain space in  
    Bddapron.Expr0.Bool.var (space_env space) (domain_cond domain) var_name

  let format_space f space =
    Env.print f (space_env space)

  let rec expr_of_term space term =
    let domain = space_domain space in
    let solver = domain_solver domain in
    if Solver.is_int_numeral solver term then
      (* TODO: Support rational numerals *)
      let s = Solver.term_numeral_s solver term in
      mk_int_s space s
    else if Solver.is_uninterpreted_const solver term then
      let func_decl = Solver.term_func_decl solver term in
      let name = func_decl_var domain func_decl in
      mk_arith_var space name
    else if Solver.is_unary_minus solver term then
      let arg = List.hd (Solver.term_args solver term) in
      mk_unary_minus space (expr_of_term space arg)
    else if Solver.is_plus solver term then
      let args = Solver.term_args solver term in
      mk_plus space (List.map (expr_of_term space) args)
    else if Solver.is_minus solver term then
      let args = Solver.term_args solver term in
      mk_minus space (List.map (expr_of_term space) args)
    else if Solver.is_mult solver term then
      let args = Solver.term_args solver term in
      mk_mult space (List.map (expr_of_term space) args)
    else if Solver.is_int_to_real solver term then
      (* Just ignoring the conversion operator.
         Currently, expressions completely ignore the sorts ('typ's in Apron terms. *)
      let args = Solver.term_args solver term in
      expr_of_term space (List.hd args)
    else if Solver.is_real_to_int solver term then
      (* Just ignoring the conversion operator *)
      let args = Solver.term_args solver term in
      expr_of_term space (List.hd args)
    else
      invalid_term "Not supported." (Solver.term_to_string solver term)

  type num_cond_t = Apron.Tcons0.t

  type bool_cond_t = bddapron_symbol_t Expr0.Bool.t

  type cond_t =
    | NumC of num_cond_t
    | BoolC of bool_cond_t
  
  let unwrap_bool_cond cond =
    match cond with
    | BoolC bool_cond -> bool_cond
    | _ -> raise (Invalid_argument "Condition should be Boolean")

  let mk_not_cond space cond =
    let domain = space_domain space in
    match cond with
    | NumC tcons ->
        let texpr = tcons.Apron.Tcons0.texpr0 in
        let neg_tcons =
          (match tcons.Apron.Tcons0.typ with
          (* TODO: Maybe throw on EQ and DISEQ. *)
          | Apron.Tcons0.EQ ->
              Apron.Tcons0.make texpr Apron.Tcons0.DISEQ
          | Apron.Tcons0.DISEQ ->
              Apron.Tcons0.make texpr Apron.Tcons0.EQ
          | Apron.Tcons0.SUPEQ ->
              Apron.Tcons0.make (mk_unary_minus space texpr) Apron.Tcons0.SUP
          | Apron.Tcons0.SUP ->
              Apron.Tcons0.make (mk_unary_minus space texpr) Apron.Tcons0.SUPEQ
          | _ ->
              raise (Not_supported "Supported operators are =, =/=, >, and >=")
          )
        in
        NumC neg_tcons
    | BoolC bool_cond ->
        BoolC (Bddapron.Expr0.Bool.dnot (space_env space) (domain_cond domain) bool_cond)

  let mk_and_bool_cond space conds =
    let domain = space_domain space in
    let true_cond = Bddapron.Expr0.Bool.dtrue (space_env space) (domain_cond domain) in
    BoolC (List.fold_left
      (fun acc cond ->
        let bool_cond = unwrap_bool_cond cond in
        Bddapron.Expr0.Bool.dand (space_env space) (domain_cond domain) acc bool_cond)
      true_cond
      conds)

  let mk_or_bool_cond space conds =
    let domain = space_domain space in
    let false_cond = Bddapron.Expr0.Bool.dfalse (space_env space) (domain_cond domain) in
    BoolC (List.fold_left
      (fun acc cond ->
        let bool_cond = unwrap_bool_cond cond in
        Bddapron.Expr0.Bool.dor (space_env space) (domain_cond domain) acc bool_cond)
      false_cond
      conds)
      
  let rec cond_of_expl_term space term =
    let domain = space_domain space in
    let solver = domain_solver domain in
    let mk_bin_cond mk_cond t =
      (* TODO: Support more than two arguments for comparisons. *)
      match Solver.term_args solver t with
      | [arg1; arg2] ->
          NumC (mk_cond
            (expr_of_term space arg1)
            (expr_of_term space arg2))
      | _ -> raise (Not_supported "Term should have two arguments")
    in
    if Solver.is_true solver term then
      BoolC (Bddapron.Expr0.Bool.dtrue (space_env space) (domain_cond domain))
    else if Solver.is_false solver term then
      BoolC (Bddapron.Expr0.Bool.dfalse (space_env space) (domain_cond domain))
    else if Solver.is_uninterpreted_const solver term then
      let func_decl = Solver.term_func_decl solver term in
      let name = func_decl_var domain func_decl in
      BoolC (mk_bool_var space name)
    else if Solver.is_not solver term then
      let term_arg = match Solver.term_args solver term with
      | [arg] -> arg
      | _ -> raise (Invalid_argument "Negation should have one argument")
      in
      mk_not_cond space (cond_of_expl_term space term_arg)
    else if Solver.is_and solver term then
      mk_and_bool_cond
        space
        (List.map (cond_of_expl_term space) (Solver.term_args solver term))
    else if Solver.is_or solver term then
      mk_or_bool_cond
        space
        (List.map (cond_of_expl_term space) (Solver.term_args solver term))
    else if Solver.is_eq solver term then
      mk_bin_cond (mk_eq space) term
    else if Solver.is_leq solver term then
      mk_bin_cond (mk_leq space) term
    else if Solver.is_lt solver term then
      mk_bin_cond (mk_lt space) term
    else if Solver.is_geq solver term then
      mk_bin_cond (mk_geq space) term
    else if Solver.is_gt solver term then
      mk_bin_cond (mk_gt space) term
    else
      invalid_term "Not supported." (Solver.term_to_string solver term)      

  let elem_of_expl_terms space expl_terms =  
    let domain = space_domain space in
    let bool_top = Bdddomain0.top (domain_m domain) (space_env space) in
    let (bool_elem, tconss) =
      List.fold_left
        (fun (bool_elem, tconss) term -> 
          match cond_of_expl_term space term with
          | NumC tcons ->
              (bool_elem, tcons::tconss)
          | BoolC bool_cond ->
              let new_elem = Bdddomain0.meet_condition
                (domain_m domain) (space_env space) (domain_cond domain) bool_elem bool_cond
              in
              (new_elem, tconss)
        )
        (bool_top,[])
        expl_terms
    in
    let apron_env = Env.apron (space_env space) in
    let (int_vars, real_vars) = Apron.Environment.vars apron_env in
    let apron_elem = Apron.Abstract0.of_tcons_array
      (domain_apron_m domain)
      (Array.length int_vars)
      (Array.length real_vars)
      (Array.of_list tconss) in
    let num_elem = Bdddomain0.of_apron (domain_m domain) (space_env space) apron_elem in
    Bdddomain0.meet (domain_m domain) bool_elem num_elem

  let format_env_elem env fmt elem =
    Bdddomain0.print env fmt elem

  let format_elem space fmt elem =
    format_env_elem (space_env space) fmt elem

  let move_project_env env_from elem_from space_to =    
    let domain = space_domain space_to in
    let env_to = space_env space_to in
    let m = domain_m domain in
    let change = Env.compute_change env_from env_to in
    (* According to Bertrand, bottom should come from the target domain. *)
    let bot_to = Bdddomain0.bottom m env_to in
    let result = Bdddomain0.apply_change bot_to m elem_from change in
    (* eprintf "move_project_env:@.env_from:@.%a@.elem_from: %a@.env_to:@.%a@.result: %a@."
      Env.print env_from
      (format_env_elem env_from) elem_from
      Env.print env_to
      (format_env_elem env_to) result; *)
    result
  
  let move_project space_from elem_from space_to =    
    check_arg
      ((space_domain space_from) == (space_domain space_to))
      "Spaces should be from the same domain.";
    let env_from = space_env space_from in
    move_project_env env_from elem_from space_to    

  let move_rename_array space_from elem_from renames space_to =    
    let domain = space_domain space_from in
    check_arg
      (domain == (space_domain space_to))
      "Spaces should be from the same domain.";    
    let renames_list = Array.fold_left
      (fun acc (fd_from, fd_to) -> (func_decl_var domain fd_from, func_decl_var domain fd_to) :: acc)
      []
      renames
    in    
    let env_from = space_env space_from in    
    let renamed_env = Env.copy env_from in
    let perm = Env.rename_vars_with renamed_env renames_list in
    let renamed_elem = Bdddomain0.apply_permutation (domain_m domain) elem_from perm in    
    move_project_env renamed_env renamed_elem space_to    

  let to_string space elem =
    asprintf "%a" (format_elem space) elem

  let term_of_cube space cube =
    (* Adapted from Bdd.Expr0.Expr.conjunction_of_minterm *)
    let solver = space_solver space in
    let env = (space_env space) in
    let lits = PMappe.fold
      (fun var tid acc ->
        match Env.typ_of_var env var with
        | `Bool ->
          let id = tid.(0) in
          let v = cube.(id) in
          let var_term = term_of_var space var in
          if v = Cudd.Man.True then
            var_term :: acc
          else if v = Cudd.Man.False then
            (Solver.mk_not solver var_term) :: acc
          else
            acc
        | _ -> acc)
      env.Bdd.Env.vartid
      []
    in
    Solver.mk_and solver lits

  let term_of_bdd space bdd =
    let solver = space_solver space in
    if Cudd.Bdd.is_true bdd then
      Solver.mk_true solver
    else if Cudd.Bdd.is_false bdd then
      Solver.mk_false solver
    else
      let terms = ref [] in
      Cudd.Bdd.iter_cube
        (fun cube ->
          let cube_term = term_of_cube space cube in
          terms := cube_term::!terms)
        bdd;
      Solver.mk_or solver !terms

  let term_of_coeff space coeff =
    Solver.mk_int_s (space_solver space) (asprintf "%a" Apron.Coeff.print coeff)

  let term_of_linexpr space linexpr =
    let solver = space_solver space in
    let a_env = Env.apron (space_env space) in
    let (int_vars, real_vars) = Apron.Environment.vars a_env in
    let append_var_term terms a_var = 
      let a_dim = Apron.Environment.dim_of_var a_env a_var in
      let var_term = term_of_var space (Env.var_of_aprondim (space_env space) a_dim) in
      let coeff = Apron.Linexpr0.get_coeff linexpr a_dim in
      let coeff_1 = Apron.Coeff.s_of_int 1 in
      let coeff_m1 = Apron.Coeff.s_of_int (-1) in
      if Apron.Coeff.is_zero coeff then
        terms
      else if Apron.Coeff.equal coeff coeff_1 then
        var_term :: terms
      else if Apron.Coeff.equal coeff coeff_m1 then
        (Solver.mk_unary_minus solver var_term) :: terms
      else
        let coeff_term = term_of_coeff space coeff in
        (Solver.mk_mult solver [coeff_term; var_term]) :: terms
    in
    let terms = ref [] in      
    (let cst = Apron.Linexpr0.get_cst linexpr in
      if not (Apron.Coeff.is_zero cst) then
        terms := (term_of_coeff space cst) :: !terms
      else
        ());
    Array.iter (fun a_v -> terms := append_var_term !terms a_v) int_vars;
    Array.iter (fun a_v -> terms := append_var_term !terms a_v) real_vars;
    Solver.mk_plus solver !terms

  let term_of_lincons space lincons =
    let solver = space_solver space in
    let expr_term = term_of_linexpr space lincons.Apron.Lincons0.linexpr0 in
    let typ = lincons.Apron.Lincons0.typ in
    let zero_term = Solver.mk_int solver 0 in
    if typ = Apron.Lincons0.EQ then
      Solver.mk_eq solver [expr_term; zero_term]
    else if typ = Apron.Lincons0.SUPEQ then
      Solver.mk_geq solver [expr_term; zero_term]
    else if typ = Apron.Lincons0.SUP then
      Solver.mk_gt solver [expr_term; zero_term]
    else
      raise (Not_supported "Supported operators are =, >, and >=")

  let term_of_apron space a_elem =
    let domain = space_domain space in
    let solver = domain_solver domain in
    let linconses = Array.to_list
      (Apron.Abstract0.to_lincons_array (domain_apron_m domain) a_elem)
    in
    Solver.mk_and solver (List.map (term_of_lincons space) linconses)

  let term_of_elem space elem =
    let domain = space_domain space in
    let solver = domain_solver domain in    
    let disjuncts = Bdddomain0.to_bddapron (domain_m domain) elem in
    Solver.mk_or solver
      (List.map
        (fun (bdd, apron_elem) -> Solver.mk_and solver [
          (term_of_bdd space bdd);
          (term_of_apron space apron_elem) ])
        disjuncts)

  let is_bot space elem =
    let domain = space_domain space in  
    Bdddomain0.is_bottom (domain_m domain) elem

end

