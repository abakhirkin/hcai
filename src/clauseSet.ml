open Format

module DynArray = Batteries.DynArray
module Hashtbl = Batteries.Hashtbl

module type T = sig
  module Solver: Solvers.T

  module Pred: sig
    type t    
    val func_decl : t -> Solver.func_decl_t
    val head_args : t -> Solver.term_t list
    val is_init: t -> bool
    val format : Solver.t -> Format.formatter -> t -> unit
    val format_decl : Solver.t -> Format.formatter -> t -> unit    
    val format_head : Solver.t -> Format.formatter -> t -> unit
    val to_string : Solver.t -> t -> string      

    module Hashtbl: Hashtbl.S with type key = t
  end

  module PredApp: sig
    type t
    val pred : t -> Pred.t
    val args : t -> Solver.term_t list
    val make : Pred.t -> Solver.term_t list -> t
    val format : Solver.t -> Format.formatter -> t -> unit
  end

  module HornClause: sig
    type t
    val head : t -> Pred.t
    val constr : t -> Solver.term_t
    val body_apps_count : t -> int
    val iter_body_apps : (PredApp.t -> unit) -> t -> unit
    val fold_body_apps: ('a -> PredApp.t -> 'a) -> 'a -> t -> 'a
    val format : Solver.t -> Format.formatter -> t -> unit
  end

  module BackwClause: sig
    type t
    val make : HornClause.t -> HornClause.t option -> t
    val clause : t -> HornClause.t
    val forward_clause : t -> HornClause.t option
  end

  type t
  type clause_set_t = t

  val solver: t -> Solver.t
  val is_pred : t -> Solver.func_decl_t -> bool
  val func_decl_pred : t -> Solver.func_decl_t -> Pred.t
  val pred_count: t -> int
  val iter_preds: (Pred.t -> unit) -> t -> unit
  val fold_preds: ('a -> Pred.t -> 'a) -> 'a -> t -> 'a
  val fold_init_preds: ('a -> Pred.t -> 'a) -> 'a -> t -> 'a
  val fold_goal_preds: ('a -> Pred.t -> 'a) -> 'a -> t -> 'a
  val iter_head_clauses: (HornClause.t -> unit) -> t -> Pred.t -> unit
  val iter_backw_head_clauses: (BackwClause.t -> unit) -> t -> Pred.t -> unit
  val fold_goal_terms: ('a -> Solver.term_t -> 'a) -> 'a -> t -> 'a


  module Builder: sig
    type t
    val solver : t -> Solver.t
    val is_pred : t -> Solver.func_decl_t -> bool
    val func_decl_pred : t -> Solver.func_decl_t -> Pred.t
    val make : Solver.t -> t    
    val make_pred : t -> Solver.func_decl_t -> Pred.t    
    val make_clause : t -> PredApp.t array -> Solver.term_t -> PredApp.t -> unit
    val make_goal_term : t -> Solver.term_t -> unit    
    val build : t -> clause_set_t
  end
end

module Make(SolverA: Solvers.T) = struct
  module Solver = SolverA

  type pred_t = {
    t_func_decl: Solver.func_decl_t;
    t_head_args: Solver.term_t list;  (* TODO: Not sure if this should be a list or an array. *)
    mutable t_is_init: bool;
    mutable t_is_goal: bool;
    t_head_clauses: horn_clause_t DynArray.t;
    t_backw_head_clauses: backw_clause_t DynArray.t
  }
  and pred_app_t = {
    t_pred: pred_t;
    t_args: Solver.term_t list (* TODO: Not sure if this should be a list or an array. *)
  }
  and horn_clause_t = {
    t_body_apps: pred_app_t array;
    t_constraint: Solver.term_t;
    t_head: pred_t
  }
  and backw_clause_t = {
    t_clause: horn_clause_t;
    t_forward_clause: horn_clause_t option
  }  

  module Pred = struct
    
    type t = pred_t

    let func_decl pred =
      pred.t_func_decl

    let head_args pred =
      pred.t_head_args
    
    let is_init pred = 
      pred.t_is_init

    let format solver fmt pred =
      Solver.format_func_decl_name solver fmt pred.t_func_decl

    let format_decl solver fmt pred =
      Solver.format_func_decl solver fmt pred.t_func_decl

    let format_app solver fmt pred args =
      if (List.length args) = 0 then begin
        pp_open_box fmt 0;
        format solver fmt pred;
        pp_close_box fmt ()
      end else begin
        pp_open_box fmt 1;
        pp_print_string fmt "(";
        format solver fmt pred;
        pp_print_space fmt ();
        FormatU.format_list (Solver.format_term solver) fmt args;
        pp_print_string fmt ")";
        pp_close_box fmt ()
      end    

    let format_head solver fmt pred =
      format_app solver fmt pred pred.t_head_args

    let to_string solver pred =
      asprintf "%a" (format solver) pred

    module OrdHash = struct
      type out_t = t
      type t = out_t

      let compare p1 p2 =
        Solver.FuncDeclOrdHash.compare (func_decl p1) (func_decl p2)

      let equal p1 p2 =
        Solver.FuncDeclOrdHash.equal (func_decl p1) (func_decl p2)

      let hash pred =
        Solver.FuncDeclOrdHash.hash (func_decl pred)
    end

    module Hashtbl = Hashtbl.Make(OrdHash)
    module Hashset = Hashset.Make(OrdHash)  
  end

  module PredApp = struct

    type t = pred_app_t 

    let pred app =
      app.t_pred

    let args app =
      app.t_args

    let make pred args = 
      { t_pred = pred; t_args = args }

    let format solver fmt app =
      Pred.format_app solver fmt app.t_pred app.t_args
  end

  module HornClause = struct
    type t = horn_clause_t

    let head clause =
      clause.t_head

    let constr hc =
      hc.t_constraint

    let body_apps_count hc =
      Array.length hc.t_body_apps

    let iter_body_apps fn hc =
      Array.iter fn hc.t_body_apps

    let fold_body_apps fn acc hc =
      Array.fold_left fn acc hc.t_body_apps

    let iter_body_apps fn clause =
      Array.iter fn clause.t_body_apps

    let format_body solver fmt clause =  
      if (Array.length clause.t_body_apps) = 0 then begin
        pp_open_box fmt 0;
        Solver.format_term solver fmt clause.t_constraint;
        pp_close_box fmt ()
      end else begin
        pp_open_box fmt 1;
        pp_print_string fmt "(";
        pp_print_string fmt "and";
        pp_print_space fmt ();
        FormatU.format_array (PredApp.format solver) fmt clause.t_body_apps;
        pp_print_space fmt ();
        Solver.format_term solver fmt clause.t_constraint;
        pp_print_string fmt ")";
        pp_close_box fmt ()
      end    

    let format solver fmt hc =
      pp_open_box fmt 1;
      pp_print_string fmt "(";
      pp_print_string fmt "=>";
      pp_print_space fmt ();
      format_body solver fmt hc;
      pp_print_space fmt ();
      Pred.format_head solver fmt hc.t_head;
      pp_print_string fmt ")";
      pp_close_box fmt ()
  end

  module BackwClause = struct
    type t = backw_clause_t

    let make backw_clause forward_clause =
      { t_clause = backw_clause; t_forward_clause = forward_clause }

    let clause bc =
      bc.t_clause

    let forward_clause bc =
      bc.t_forward_clause
  end

  module FuncDeclHashtbl = Solver.FuncDeclHashtbl
  module FuncDeclHashset = Hashset.Make(Solver.FuncDeclOrdHash)
  module PredHashtbl = Pred.Hashtbl
  module PredHashset = Pred.Hashset
  module DepG = Graph.Imperative.Digraph.ConcreteBidirectional(Pred.OrdHash)

  type t = {
    t_solver: Solver.t;
    t_preds: Pred.t FuncDeclHashtbl.t;
    t_init_preds: Pred.t DynArray.t;
    t_goal_preds: Pred.t DynArray.t;    
    t_clauses: HornClause.t DynArray.t;    
    t_goal_terms: Solver.term_t DynArray.t;
    t_dep_graph: DepG.t
  }
  type clause_set_t = t

  let solver cs =
    cs.t_solver

  let is_pred cs func_decl =
      FuncDeclHashtbl.mem cs.t_preds func_decl

  let func_decl_pred cs func_decl =
    FuncDeclHashtbl.find cs.t_preds func_decl

  let pred_count cs =
    FuncDeclHashtbl.length cs.t_preds

  let iter_preds fn cs =
    FuncDeclHashtbl.iter (fun _ pred -> fn pred) cs.t_preds

  let fold_preds fn acc cs =
    FuncDeclHashtbl.fold (fun _ pred acc -> fn acc pred) cs.t_preds acc

  let fold_init_preds fn acc cs =
    DynArray.fold_left fn acc cs.t_init_preds

  let fold_init_preds fn acc cs =
    DynArray.fold_left fn acc cs.t_init_preds
  
  let fold_goal_preds fn acc cs =
    DynArray.fold_left fn acc cs.t_goal_preds

  let iter_head_clauses fn cs pred =
    DynArray.iter fn pred.t_head_clauses

  let iter_backw_head_clauses fn cs pred =
    DynArray.iter fn pred.t_backw_head_clauses

  let fold_goal_terms fn acc cs =
    DynArray.fold_left fn acc cs.t_goal_terms

  let dep_graph cs =
    cs.t_dep_graph

  let format fmt set =
    let solver = set.t_solver in
    (* TODO: Print in seahorn format, or even better, move printing to a separate module. *)
    pp_open_box fmt 0;
    pp_print_string fmt "; Predicates";
    pp_print_newline fmt ();
    FuncDeclHashtbl.iter
      (fun _ pred ->
        Pred.format_decl solver fmt pred;
        pp_print_newline fmt ())
      set.t_preds;
    pp_print_string fmt "; Clauses";
    pp_print_newline fmt ();
    FormatU.format_dynarray ~print_sep:FormatU.newline_sep (HornClause.format solver) fmt set.t_clauses;
    pp_print_newline fmt ();
    DynArray.iter
      (fun term ->
        pp_print_string fmt "; goal term";
        pp_print_newline fmt ();
        Solver.format_term solver fmt term;
        pp_print_newline fmt ())      
      set.t_goal_terms;
    pp_print_string fmt "; goals:";
    pp_print_newline fmt ();
    FormatU.format_iter (Pred.format solver) DynArray.iter fmt set.t_goal_preds;
    pp_close_box fmt ()    

  let to_string set =
    asprintf "%a" format set

  let format_backw_clauses fmt set =
    let solver = set.t_solver in
    FormatU.format_iter
      ~print_sep:FormatU.newline_sep
      (fun fmt pred ->
        FormatU.format_dynarray
          ~print_sep:FormatU.newline_sep
          (fun fmt bc -> HornClause.format solver fmt (BackwClause.clause bc))
          fmt
          pred.t_backw_head_clauses)
      (fun fn -> FuncDeclHashtbl.iter (fun _ pred -> fn pred))
      fmt
      set.t_preds

  let backw_clauses_to_string set =
    asprintf "%a" format_backw_clauses set

  module Builder = struct
    type t = {
      t_solver: Solver.t;
      mutable t_preds: Pred.t FuncDeclHashtbl.t;
      mutable t_clauses: HornClause.t DynArray.t;
      mutable t_init_preds: Pred.t DynArray.t;
      mutable t_goal_preds: Pred.t DynArray.t;
      mutable t_goal_terms: Solver.term_t DynArray.t
    }    

    let solver builder =
      builder.t_solver

    let is_pred builder func_decl =
      FuncDeclHashtbl.mem builder.t_preds func_decl

    let func_decl_pred builder func_decl =
      FuncDeclHashtbl.find builder.t_preds func_decl
    
    let make solver =    
      { t_solver = solver;
        t_preds = FuncDeclHashtbl.create 16;
        t_clauses = DynArray.create ();
        t_init_preds = DynArray.create ();
        t_goal_preds = DynArray.create ();
        t_goal_terms = DynArray.create () }

    let make_new_pred builder func_decl =
      let arg_sorts = Solver.func_decl_domain builder.t_solver func_decl in
      let head_args = List.map
        (* NOTE: This relies on that all uninterpreted contants and predicates
          have been seen. That is, they should be declared in advance. *)
        (Solver.mk_fresh_const builder.t_solver)
        arg_sorts
      in
      { t_func_decl = func_decl;
        t_head_args = head_args;
        t_is_init = false;
        t_is_goal = false;
        t_head_clauses = DynArray.create ();
        t_backw_head_clauses = DynArray.create () }

    let make_pred builder func_decl =
      try FuncDeclHashtbl.find builder.t_preds func_decl
      with
      | Not_found ->
        let pred = make_new_pred builder func_decl in
        FuncDeclHashtbl.replace builder.t_preds func_decl pred;
        pred

    let make_new_clause builder body_apps constr head_app =    
      let solver = solver builder in
      let pred = (PredApp.pred head_app) in
      let clause_constrs = DynArray.create () in
      let make_clause_body_app app =
        let pred = PredApp.pred app in
        let app_args = PredApp.args app in
        (* Set of non-fresh constrants in the arguments of app. *)
        let const_args = FuncDeclHashset.create (List.length app_args) in
        let new_args = List.map2
          (fun arg arg_sort ->
            let arg_func_decl = Solver.term_func_decl solver arg in
            (* NOTE: An uninterpreted constant in the argument cannot be a predicate. *)
            if (Solver.is_uninterpreted_const solver arg) &&
               (not (FuncDeclHashset.mem const_args arg_func_decl)) then begin
              FuncDeclHashset.add const_args arg_func_decl;
              arg              
            end else begin
              let new_arg = Solver.mk_fresh_const solver arg_sort in
              (* NOTE: new_arg is fresh, no need to add it to arg_set. *)
              DynArray.add clause_constrs (Solver.mk_eq builder.t_solver [new_arg; arg]);
              new_arg
            end)
          (PredApp.args app)
          (Solver.func_decl_domain solver (Pred.func_decl pred))          
        in
        PredApp.make pred new_args
      in
      DynArray.add clause_constrs constr;
      List.iter2
        (fun t1 t2 ->
          DynArray.add clause_constrs (Solver.mk_eq builder.t_solver [t1; t2]))
        (Pred.head_args pred)
        (PredApp.args head_app);
      let clause_body_apps = Array.map make_clause_body_app body_apps in
      { t_body_apps = clause_body_apps;
        t_constraint =
          Solver.to_nnf
            builder.t_solver
            (Solver.mk_and builder.t_solver (DynArray.to_list clause_constrs));
        t_head = pred }

    let make_init_pred builder pred =
      pred.t_is_init <- true;
      DynArray.add builder.t_init_preds pred

    (* TODO: Create backward clause from the add_clause arguments,
       and not from the forward clause. *)
    let add_clause_backw_clauses builder forward_clause =    
      if HornClause.body_apps_count forward_clause > 0 then begin
        let forward_head = HornClause.head forward_clause in
        let backw_body_args =
          List.map
            (fun arg ->
              Solver.mk_fresh_const builder.t_solver (Solver.term_sort builder.t_solver arg))
            (Pred.head_args forward_head) in
        let backw_body_app = PredApp.make forward_head backw_body_args in
        let new_constr =
          Solver.substitute
            builder.t_solver
            (HornClause.constr forward_clause)
            (Pred.head_args forward_head)
            backw_body_args
        in
        HornClause.iter_body_apps
          (fun forward_app ->          
            let backw_clause =
              make_new_clause builder [|backw_body_app|] new_constr forward_app in
            let backw_head = HornClause.head backw_clause in
            DynArray.add
              backw_head.t_backw_head_clauses
              (BackwClause.make backw_clause (Some forward_clause)))
          forward_clause
      end else ()

    let make_clause builder body_apps constr head_app =
      let clause = make_new_clause builder body_apps constr head_app in
      DynArray.add builder.t_clauses clause;
      let head = PredApp.pred head_app in
      DynArray.add head.t_head_clauses clause;
      add_clause_backw_clauses builder clause;
      if Array.length body_apps = 0 then
        make_init_pred builder (PredApp.pred head_app)    
      else ()

    let make_goal_pred builder pred =
      pred.t_is_goal <- true;
      DynArray.add builder.t_goal_preds pred;
      let backw_clause =
          make_new_clause builder
            [||]
            (Solver.mk_true builder.t_solver)
            (PredApp.make pred (Pred.head_args pred))
        in
        DynArray.add
          pred.t_backw_head_clauses
          (BackwClause.make backw_clause None)      

    let make_goal_term builder term =
      let solver = builder.t_solver in
      let rec make_goals_rec term =
        if Solver.is_apply solver term then begin
          let fd = Solver.term_func_decl solver term in
          begin try
            let pred = func_decl_pred builder fd in
            make_goal_pred builder pred
          with
          | Not_found -> ()
          end;
          List.iter make_goals_rec (Solver.term_args solver term)
        end else if Solver.is_uninterpreted_const solver term then begin
          let fd = Solver.term_func_decl solver term in
          begin try
            let pred = func_decl_pred builder fd in
            make_goal_pred builder pred
          with
          | Not_found -> ()
          end;
        end else ()
      in
      let nnf_term = Solver.to_nnf solver term in
      DynArray.add builder.t_goal_terms nnf_term;
      make_goals_rec nnf_term

    let make_new_dep_graph builder =
      let g = DepG.create () in
        DynArray.iter
          (fun hc ->
            let pred_to = HornClause.head hc in
            DepG.add_vertex g pred_to;
            HornClause.iter_body_apps              
              (fun app ->
                let pred_from = PredApp.pred app in 
                DepG.add_edge g pred_from pred_to)
              hc
          )
          builder.t_clauses;
      g

    let build builder =   
      let set =
        { t_solver = builder.t_solver;
          t_preds = builder.t_preds;
          t_init_preds = builder.t_init_preds;
          t_clauses = builder.t_clauses;
          t_goal_preds = builder.t_goal_preds;
          t_goal_terms = builder.t_goal_terms;
          t_dep_graph = make_new_dep_graph builder }
      in
      builder.t_preds <- FuncDeclHashtbl.create 16;
      builder.t_clauses <- DynArray.create ();
      builder.t_init_preds <- DynArray.create ();
      builder.t_goal_preds <- DynArray.create ();
      builder.t_goal_terms <- DynArray.create ();
      set
  end
end
