(** Solver interfaces and utilities. *)

module Hashtbl = Batteries.Hashtbl

(** Signature for the implementation of comparison and hashing for a type.
    
    Equivalent to the union of [Hashtbl.HashedType] and [Set.OrderedType]. *)
module type OrdHash = sig
  type t

  val compare: t -> t -> int

  val equal: t -> t -> bool

  val hash: t -> int
end

type sat_result_t =
  | Sat
  | Unsat
  | Unknown

type 'a model_result_t =
  | SatM of 'a
  | UnsatM
  | UnknownM

(** SMT solver interface.

  Provides functions that create, inspect, check for satisfiability and evaluate terms.
 *)
module type T = sig

  (** Type of a solver object.

      (Almost) every function of the solver interface accepts a solver object as
      an argument.
      An exception are comparison and hashing functions declared in {!FuncDeclOrdHash}.
      These do not require to specify the solver instance.

      Note that the interface does not include constructors of solver objects.
      These will be implementation-specific. *)
  type t

  (** {2 Symbols} *)

  (** Type of a symbol object. *)
  type symbol_t
 
  (** Creates a solver symbol from a syntatic symbol object. *)
  val mk_symbol_smt2: t -> Smt2.symbol_t -> symbol_t

  (** {2 Sorts} *)

  (** Type of a sort object. *)
  type sort_t

  (** Creates a sort object for sort [Bool]. *)
  val mk_bool_sort: t -> sort_t
  
  (** Creates a sort object for sort [Int]. *)
  val mk_int_sort: t -> sort_t
  
  (** Creates a sort object for sort [Real]. *)
  val mk_real_sort: t -> sort_t

  (** {2 Function Declarations} *)

  (** Type of a function declaration object. *)
  type func_decl_t
  
  (** Creates a function declaration.
      
      [(mk_func_decl s name domain range)] returns a function declaration where
      - [name] is the name of the function;
      - [domain] is the domain (sorts of parameters) of the function;
      - [range] is the range (sort of return value) of the function *)
  val mk_func_decl: t ->
    symbol_t ->
    sort_t list ->
    sort_t -> func_decl_t
 
  (* TODO: Need to create those with a prefix. *)
  (** Creates a fresh function declaration. *)
  val mk_fresh_func_decl: t ->
    sort_t list ->
    sort_t -> func_decl_t

  (** Creates the function declaration corresponding to the [true] Boolean constant. *)
  (* TODO: Move aux functions (that can be emulated by composing the other)
     to a separate module. *)
  val mk_true_func_decl: t-> func_decl_t

  (** Domain (sort of parameters) of a function. *)
  val func_decl_domain: t -> func_decl_t -> sort_t list

  (** Range (sort of return value) of a function. *)
  val func_decl_range: t -> func_decl_t -> sort_t

  (** Whether the range of the function declaration is 'Bool'. *)
  val is_bool_func_decl: t -> func_decl_t -> bool
  
  (** Whether the range of the function declaration is 'Int'. *)
  val is_int_func_decl: t -> func_decl_t -> bool
  
  (** Whether the range of the function declaration is 'Real'. *)
  val is_real_func_decl: t -> func_decl_t -> bool

  (** Provides comparison and hashing for function declarations.
   
      Notice that the solver implementation allows to compare and hash
      function declarations without explicitly specifying a solver instance.
      This allows to use function declarations as keys in functor-style
      maps, hash tables, etc. *)
  module FuncDeclOrdHash: OrdHash with type t = func_decl_t

  (** Default implementation of a hashtable with function declarations as keys. *)
  module FuncDeclHashtbl: Hashtbl.S with type key = func_decl_t

  (** Whether two function declarations are equal.
  
      Same as {!FuncDeclOrdHash.equal}. *)
  val func_decl_equal: func_decl_t -> func_decl_t -> bool

  (** {2 Terms}
   
      Note that some operations are labelled as {e chainable}, {e left-associative},
      or {e right-associative}.

      For a {e chainable} operation [f], the term [(f t1 t2 ... tn)] denotes
      [(and (f t1 t2) (f t2 t3) ... (f tn-1 tn)].

      For a {e left-associative} operation [f], the term [(f t1 t2 ... tn)] denotes
      [(f (f t1 t2 ... tn-1) tn)].
      
      For a {e right-associative} operation [f], the term [(f t1 t2 ... tn)] denotes
      [(f t1 (f t2 ... tn))]. *)

  (** Type of a term object. *)
  type term_t

  (** Creates a [true] term. *)
  val mk_true: t -> term_t

  (** Creates a [false] term. *)  
  val mk_false: t -> term_t

  (** Creates a constant term corresponding to a nullary function declaration.
   
      If the function declaration is not nullary, the result is not specified. *)
  (* TODO: perhaps check if it is not nullary and throw an exception. *)
  val mk_const: t -> func_decl_t -> term_t


  (** Creates a constant term with the given sort and name.
   
      If a there already exists a function declaration with the same name,
      but not nullary or with a different sort, the result is not specified. *)
  val mk_const_s: t -> sort_t -> string -> term_t

  (** Creates a fresh constant of a given sort. *)
  val mk_fresh_const: t -> sort_t -> term_t
  
  (** Creates an integer term from a value of type [int]. *)
  val mk_int: t -> int -> term_t

  (** Creates an integer term from a [string] representation. *)
  val mk_int_s: t -> string -> term_t

  (** Creates a conjunction of [terms].

      @return as follows
      - [(mk_and s [])] returns [true];
      - [(mk_and s [term])] returns (a logical equivalent of) [term];
      - [(mk_and s terms)] returns (a logical equivalent of) the conjunction of [terms],
        if [terms] has two or more elements. *)
  val mk_and: t -> term_t list -> term_t
  
  (** Creates a disjunction of [terms].

      @return as follows
      - [(mk_or s [])] returns [false];
      - [(mk_or s [term])] returns (a logical equivalent of) [term];
      - [(mk_or s terms)] returns (a logical equivalent of) the disjunction of [terms],
        if [terms] has two or more elements. *)
  val mk_or: t -> term_t list -> term_t
  
  (** Creates a negation of a Boolean term. *)
  val mk_not: t -> term_t -> term_t

  (** Creates an equality
  
      Equality is chainable.

      @raise Invalid_argument if the list of terms has less than two elements. *)
  val mk_eq: t -> term_t list -> term_t
  
  val mk_distinct: t -> term_t list -> term_t

  (** Creates an if-then-else term.
   
      If the condition (second argument) is not of Boolean sort,
      the result is not specified. *)   
  val mk_ite: t -> term_t -> term_t -> term_t -> term_t

  (** Creates a Boolean implication term.

      Implication is right-associative.
  
      @raise Invalid_argument if the list of terms has less than two elements. *)
  val mk_impl: t -> term_t list -> term_t
  
  (** Creates a Boolean xor term.

      Xor is left-associative.
  
      @raise Invalid_argument if the list of terms has less than two elements. *)
  val mk_xor: t -> term_t list -> term_t

  (** Creates an application of a function to a list of arguments.
   
      If the sorts of the arguments do not match the domain of the function declaration,
      the result is not specified. *)
  val mk_apply: t -> func_decl_t -> term_t list -> term_t

  (** Creates a {e less-or-equal} constraint.

      {e Less-or-equal} is chainable.
  
      @raise Invalid_argument if the list of terms has less than two elements. *)
  val mk_leq: t -> term_t list -> term_t
  
  (** Creates a {e strictly-less} constraint.

      {e Strictly-less} is chainable.
  
      @raise Invalid_argument if the list of terms has less than two elements. *)
  val mk_lt: t -> term_t list -> term_t

  (** Creates a {e greater-or-equal} constraint.

      {e greater-or-equal} is chainable.
  
      @raise Invalid_argument if the list of terms has less than two elements. *)
  val mk_geq: t -> term_t list -> term_t
  
  (** Creates a {e strictly-greater} constraint.

      {e Strictly-greater} is chainable.
  
      @raise Invalid_argument if the list of terms has less than two elements. *)
  val mk_gt: t -> term_t list -> term_t
  
  (** Creates a sum term.
      @return as follows
      - [(mk_or s [])] returns 0;
      - [(mk_or s [term])] returns (an equivalent of) [term];
      - [(mk_or s terms)] returns (an equivalent of) the sum of [terms],
        if [terms] has two or more elements. *)
  val mk_plus: t -> term_t list -> term_t
  
  (** Creates a difference term.
   
      If the list of terms has less than two arguments, the result is not specified. *)
  val mk_minus: t -> term_t list -> term_t

  (** Creates a unary minus term. *)
  val mk_unary_minus: t -> term_t -> term_t

  (** Creates a product term. *)
  val mk_mult: t -> term_t list -> term_t

  (** Whether the term is the [true] constant. *)
  val is_true: t -> term_t -> bool

  (** Whether the term is the [false] constant. *)
  val is_false: t -> term_t -> bool

  (** Whether the term is an integer numeral. *)
  val is_int_numeral: t -> term_t -> bool

  (** Whether the term is a conjunction. *)  
  val is_and: t -> term_t -> bool
  
  (** Whether the term is a disjunction. *)  
  val is_or: t -> term_t -> bool
  
  (** Whether the term is a Boolean negation. *)  
  val is_not: t -> term_t -> bool

  (** Whether the term is an equality constraint. *)
  val is_eq: t -> term_t -> bool

  (** Whether the term is an implication. *)
  val is_impl: t -> term_t -> bool

  (** Whether the term is an application of some function (interpreted or not). *)
  val is_apply: t -> term_t -> bool

  (** Whether the term is an uninterpreted constant,
   
      in the integer and real arithmetic.
      That is, whether the term is a constant, but not [true], [false], or a numeral. *)
  val is_interpreted_const: t -> term_t -> bool
  
  (** Whether the term is an uninterpreted constant

      i.e., a nullary uninterpreted predicate. *)  
  val is_uninterpreted_const: t -> term_t -> bool

  (** Whether the term is a less-or-equal constraint. *)
  val is_leq: t -> term_t -> bool
  
  (** Whether the term is a strictly-less constraint. *)
  val is_lt: t -> term_t -> bool

  (** Whether the term is a greater-or-equal constraint. *)
  val is_geq: t -> term_t -> bool

  (** Whether the term is a strictly-greater constraint. *)
  val is_gt: t -> term_t -> bool

  (** Whether the term is a sum. *)
  val is_plus: t -> term_t -> bool

  (** Whether the term is a difference. *)
  val is_minus: t -> term_t -> bool

  (** Whether the term is an application of the unary minus. *)
  val is_unary_minus: t -> term_t -> bool

  (** Whether the term is a product. *)
  val is_mult: t -> term_t -> bool

  (** Returns the string representation of a numeral term.

      If the term is not an integer, the result is not specified. *)
  val term_numeral_s: t -> term_t -> string

  (** Returns the list of arguments of a function application.

      If the term is not a function application, the result is not specified. *)
  val term_args: t -> term_t -> term_t list

  (** Returns the funtion declaration of function application or a constant term.

      If the term is not a function application or a constant,
      the result is not specified. *)
  val term_func_decl: t -> term_t -> func_decl_t

  (** Whether the term is a real-to-int cast. *)
  val is_real_to_int: t -> term_t -> bool
  
  (** Whether the term is an int-to-real cast. *)
  val is_int_to_real: t -> term_t -> bool

  (** Converts the term to NNF. *)
  (* TODO: Better specify what is NNF. *)
  val to_nnf: t -> term_t -> term_t

  (** Substitutes occurrences of subterms. *)
  val substitute: t -> term_t -> term_t list -> term_t list -> term_t

  (** Sort of a well-sorted term.
   
      If the term is not well-sorted, the result is not specified. *)
  val term_sort: t -> term_t -> sort_t

  (** Whether the term is of sort 'Bool'. *)
  val is_bool_term: t -> term_t -> bool
  
  (** Whether the term is of sort 'Int' or 'Real'. *)
  val is_numeric_term: t -> term_t -> bool
  
  (** {2 Models} *)

  (* Type of a model object. *)
  type model_t

  (** Checks the term for SAT and returns a model (if SAT). *)      
  val get_model: t -> term_t -> model_t model_result_t

  (** Checks the term for SAT. *)      
  val is_sat: t -> term_t -> sat_result_t

  (** Evaluates a Boolean term in a model.

      @return [Some b] is the term evaluates to [b],
      and [None] if the model does not give a value to the term.

      @raise Invalid_argument if the term evaluates to a non-Boolean value. *)
  val eval_bool: ?completion:bool -> t -> model_t -> term_t -> bool option

  (** {2 Formatting} *)

  (** Name of a function declaration. *)
  val func_decl_name: t -> func_decl_t -> string

  (** Formats the name of a function declaration. *)
  (* TODO: Mention that this is usable. *)
  val format_func_decl_name: t -> Format.formatter -> func_decl_t -> unit
  
  (** Formats a function declaration for diagnostic output.
   
      Result is implementation specific. *)
  val format_func_decl: t -> Format.formatter -> func_decl_t -> unit

  (** Formats a term for diagnostc output.
   
      Result is implementation specific. *)
  val format_term: t -> Format.formatter -> term_t -> unit

  val term_to_string: t -> term_t -> string

  (** Formats a model for diagnostc output.
   
      Result is implementation specific. *)
  val format_model: t -> Format.formatter -> model_t -> unit

end