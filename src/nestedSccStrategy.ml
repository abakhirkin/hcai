(* TODO: Try to figure out how to clean up this mess. *)

module DynArray = Batteries.DynArray

module Make (DepG: Graph.Sig.I) = struct

  module NScc = NestedScc.Make(DepG)  

  module LocHashtbl = NScc.VHashtbl

  module type HierNumOrdT = Set.OrderedType with type t = NScc.data_t

  module type WlT = sig

    type wl_t
    
    module LocEntrySet: MutableSet.T

    val wl_dep_graph: wl_t -> DepG.t
    
    val wl_entries: wl_t -> LocEntrySet.t
  end
  
  module type WlPickT = functor (Wl: WlT) -> sig

    val next: Wl.wl_t -> Wl.LocEntrySet.elt

    val iter_dependent: (DepG.V.t -> unit) -> Wl.wl_t -> DepG.V.t -> unit
  end

  module MakeBase (HierNumOrd: HierNumOrdT) (MakeWlPick: WlPickT) = struct    
    
    type loc_t = DepG.V.t 
    
    type priority_t = NScc.data_t

    type loc_entry_t = {
      ld_loc: loc_t;
      ld_priority: priority_t;
      ld_widening_point: bool;
      mutable ld_widening_delay: int;
      mutable ld_narrowing_updates: int
    }

    module LocEntryOrd = struct
      type t = loc_entry_t

      let rec compare ld1 ld2 =      
        HierNumOrd.compare ld1.ld_priority ld2.ld_priority

      let equal ld1 ld2 =
        (compare ld1 ld2) = 0
    end

    module LocEntrySet = MutableSet.Make(LocEntryOrd)

    type strat_t = {
      s_dep_graph: DepG.t;
      s_init_locs: DepG.V.t list;
      s_hier_nums: priority_t LocHashtbl.t;
      s_widening_delay: int;
      s_narrowing_updates: int
    }

    let loc_priority s loc =
      LocHashtbl.find s.s_hier_nums loc

    let mk ?(widening_delay=2) ?(narrowing_updates=2) dep_graph dep_roots init_locs =
      let hier_nums = NScc.make_hier_nums dep_graph dep_roots in
      { s_dep_graph = dep_graph;
        s_init_locs = init_locs;
        s_hier_nums = hier_nums;
        s_widening_delay = widening_delay;
        s_narrowing_updates = narrowing_updates }

    type wl_t = {
      wl_dep_graph: DepG.t;
      wl_wl: LocEntrySet.t;
      wl_loc_data: loc_entry_t LocHashtbl.t;
      wl_narrowing: bool
    }

    let loc_entry wl loc =
      LocHashtbl.find wl.wl_loc_data loc         

    let mk_wl_internal s iter_init_locs init_locs narrowing =
      let loc_data = LocHashtbl.create (DepG.nb_vertex s.s_dep_graph) in
      LocHashtbl.iter
        (fun loc hier_num ->
          let ld =
            { ld_loc = loc;
              ld_priority = hier_num;
              ld_widening_point = NScc.is_nscc_head hier_num;
              ld_widening_delay = s.s_widening_delay;
              ld_narrowing_updates = s.s_narrowing_updates }
          in
          LocHashtbl.replace loc_data loc ld
        )
        s.s_hier_nums;
      let wl = LocEntrySet.make () in
      iter_init_locs
        (fun loc ->
          let loc_data = LocHashtbl.find loc_data loc in
          if NScc.is_reachable loc_data.ld_priority then
            LocEntrySet.add wl loc_data
          else ())        
        init_locs;      
      { wl_dep_graph = s.s_dep_graph;
        wl_wl = wl;
        wl_loc_data = loc_data;
        wl_narrowing = narrowing }

    let mk_wl s =
      mk_wl_internal s List.iter s.s_init_locs false

    let mk_narrow_wl s =
      mk_wl_internal s DepG.iter_vertex s.s_dep_graph true

    let should_widen wl loc =
      let loc_data = LocHashtbl.find wl.wl_loc_data loc in
      loc_data.ld_widening_point && (loc_data.ld_widening_delay = 0)

    let did_not_widen wl loc =
      let loc_data = LocHashtbl.find wl.wl_loc_data loc in
      if loc_data.ld_widening_point && loc_data.ld_widening_delay > 0 then
        loc_data.ld_widening_delay <- loc_data.ld_widening_delay - 1
      else
        ()

    module WlPick = MakeWlPick (struct
      type outer_wl_t = wl_t
      type wl_t = outer_wl_t
      module LocEntrySet = LocEntrySet
      let wl_dep_graph wl = wl.wl_dep_graph
      let wl_entries wl = wl.wl_wl
    end)
    
    let remove_next wl =
      try
        let max_elt = WlPick.next wl in
        LocEntrySet.remove wl.wl_wl max_elt;        
        Some max_elt.ld_loc
      with
      | Not_found -> None

    let add_dependent ?(add_self=true) wl loc =
      WlPick.iter_dependent
        (fun succ_loc ->
          let succ_data = LocHashtbl.find wl.wl_loc_data succ_loc in
          if  (NScc.is_reachable succ_data.ld_priority) &&
              ((not wl.wl_narrowing) || succ_data.ld_narrowing_updates > 0) && 
              (add_self || not (DepG.V.equal succ_loc loc)) then
            LocEntrySet.add wl.wl_wl succ_data 
          else ())
        wl
        loc             

    let narrowed wl loc =
      let entry = loc_entry wl loc in
      entry.ld_narrowing_updates <- entry.ld_narrowing_updates - 1
  end
  
  module ForwardHierNumOrd = struct
    type t = NScc.data_t

    let compare p1 p2 =
      let hier_num1 = NScc.hier_num p1 in
      let hier_num2 = NScc.hier_num p2 in
      let length1 = DynArray.length hier_num1 in
      let length2 = DynArray.length hier_num2 in
      let rec compare_rec i =
        if i >= length1 && i >= length2 then
          0
        else if i < length1 && i < length2 then
          (* ... > 2 > 1 > 0 > -1 *)
          let cmp = (DynArray.get hier_num1 i) - (DynArray.get hier_num2 i) in
          if cmp <> 0 then cmp else compare_rec (i + 1)
        else
          invalid_arg "One hierarchical number should not be a strict prefix of another."
      in
      compare_rec 0      
          
  end

  module BackwardHierNumOrd = struct
    type t = NScc.data_t

    let compare p1 p2 =
      let hier_num1 = NScc.hier_num p1 in
      let hier_num2 = NScc.hier_num p2 in
      let length1 = DynArray.length hier_num1 in
      let length2 = DynArray.length hier_num2 in
      let rec compare_rec i =
        if i >= length1 && i >= length2 then
          0
        else if i < length1 && i < length2 then          
          (* 0 > 1 > 2 > ... > -1 *)
          let num1 = (DynArray.get hier_num1 i) in
          let num2 = (DynArray.get hier_num2 i) in
          let cmp = if num2 >= 0 then num2 - num1 else num1 - num2 in
          if cmp <> 0 then cmp else compare_rec (i + 1)
        else
          invalid_arg "One hierarchical number should not be a strict prefix of another."
      in
      compare_rec 0

  end

  module MakePickForward (Wl: WlT) = struct

    (* TODO: Figure out if min_elt or max_elt is better. *)
    let next wl = Wl.LocEntrySet.max_elt (Wl.wl_entries wl)

    let iter_dependent iter_f wl loc =
      DepG.iter_succ iter_f (Wl.wl_dep_graph wl) loc

  end
  
  module MakePickBackward (Wl: WlT) = struct

    let next wl = Wl.LocEntrySet.max_elt (Wl.wl_entries wl)

    let iter_dependent iter_f wl loc =
      DepG.iter_pred iter_f (Wl.wl_dep_graph wl) loc

  end

  module Forward = MakeBase (ForwardHierNumOrd) (MakePickForward)
  module Backward = MakeBase (BackwardHierNumOrd) (MakePickBackward)

end
