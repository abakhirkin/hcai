module File = Batteries.File

let with_file_in ?stdin_name file_name fn  =
  match stdin_name with
  | None -> File.with_file_in file_name fn
  | Some stdin_name ->
      if file_name = stdin_name then
        fn BatIO.stdin
      else
        File.with_file_in file_name fn

let with_opt_file_in file fn =
  match file with
  | Opt.FileName file_name -> File.with_file_in file_name fn
  | Opt.Stdin -> fn BatIO.stdin
  | Opt.NoFile -> invalid_arg "Input file not specified."
