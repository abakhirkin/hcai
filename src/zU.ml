(** Additional functions on Zarith numbers. *)

(** A version of GCD operation that allows zeroes as arguments.
    
    @return [safe_gcd z1 z2] returns:
      - [z2], if [z1] is 0;
      - [z1], if [z2] is 0;
      - [Z.gcd z1 z2] otherwise. *)
let safe_gcd z1 z2 =
  if Z.equal z1 Z.zero then
    (Z.abs z2)
  else if Z.equal z2 Z.zero then
    (Z.abs z1)
  else
    Z.gcd z1 z2

(** Creates a Z3 term from a number. *)
let make_term z3 z =
  Z3.Arithmetic.Integer.mk_numeral_s z3 (Z.to_string z)

let is_zero z =
  (* TODO: Figure out what is the faster way to check for 0. *)
  Z.equal z Z.zero
