open ExceptionU

let left_assoc_of_binop binop args =
  match args with
  | [] | [_] ->
      raise (Invalid_argument "Operation should be applied to at least two arguments.")
  | head_arg :: tail_args ->
      List.fold_left binop head_arg tail_args

let right_assoc_of_binop binop args =
  match args with
  | [] | [_] ->
      raise (Invalid_argument "Operation should be applied to at least two arguments.")
  | _ ->
      match List.rev args with
      | [] | [_] -> unreachable ()
      | rev_head_arg :: rev_tail_args ->
          List.fold_left (fun acc arg -> binop arg acc) rev_head_arg rev_tail_args

let chainable_of_binop and_op binop args =
  match args with
  | [] | [_] ->
      raise (Invalid_argument "Operation should be applied to at least two arguments.")
  | [arg1; arg2] -> binop arg1 arg2
  | head_arg :: tail_args ->
      let (atoms, _) =
        List.fold_left
          (fun (atoms, prev_arg) arg -> ((binop prev_arg arg)::atoms, arg))
          ([], head_arg)
          tail_args
      in
      and_op (List.rev atoms)

let pairwise_of_binop and_op binop args =
  let rec rev_collect_atoms acc args =
    match args with
    | [] -> unreachable ()
    | [_] -> acc
    | head_arg::tail_args ->
        let rev_new_acc = List.fold_left
          (fun acc arg -> (binop head_arg arg) :: acc)
          acc
          tail_args
        in
        rev_collect_atoms rev_new_acc tail_args
  in
  match args with
  | [] | [_] ->
      raise (Invalid_argument "Operation should be applied to at least two arguments.")
  | [arg1; arg2] -> binop arg1 arg2
  | _ -> and_op (List.rev (rev_collect_atoms [] args))




        


