type t

val make: Ctx.t -> t

val format: Format.formatter -> t -> unit

val set_false: t -> unit

val of_poly: PolyBase.t -> t

val copy: t -> t

val assume_not_b: t -> BoolExpr.t -> unit

val assume_b: t -> BoolExpr.t -> unit

val assume_eq: ?remove_redundant:bool -> t -> ImplEq.t -> unit

val assume_ineq: ?remove_redundant:bool -> t -> ImplIneq.t -> unit

val assume_eq_array: t -> ImplEq.t array -> unit

val assume_eq_dynarray: t -> ImplEq.t Batteries.DynArray.t -> unit

val assume_ineq_array: t -> ImplIneq.t array -> unit

val assume_ineq_dynarray: t -> ImplIneq.t Batteries.DynArray.t -> unit

val assume_poly: t -> PolyBase.t -> unit

val remove_redundant: t -> unit

val to_poly: ?remove_redundant:bool -> t -> PolyBase.t

val eliminate_r: t -> Var.R.t -> unit

val eliminate_array_r: t -> Var.R.t array -> unit

val eliminate_dynarray_r: t -> Var.R.t Batteries.DynArray.t -> unit

val eliminate_iter_r: t -> ((Var.R.t -> unit) -> 'a -> unit) -> 'a -> unit

val convex_hull: t -> t -> unit

val convex_hull_poly: t -> PolyBase.t -> unit

val constrain_b: t -> Var.B.t -> bool -> unit

val eliminate_b: t -> Var.B.t -> unit

val eliminate_iter_b: t -> ((Var.B.t -> unit) -> 'a -> unit) -> 'a -> unit

val rename_r: t -> (Var.R.t -> Var.R.t option) -> unit

val rename_b: t -> (Var.B.t -> Var.B.t option) -> unit

val assign_b_b: t -> Var.B.t -> BoolExpr.t -> unit

(* TODO: Test assign_b_lin_ineq, assign_b_ineq *)

val assign_b_lin_ineq: t -> Var.B.t -> Ineq.t -> unit

val assign_b_ineq: t -> Var.B.t -> ImplIneq.t -> unit

val assign_r: t -> Var.R.t -> AffExpr.t -> unit

val invertible_assign_r: t -> Var.R.t -> AffExpr.t -> unit

val project_assign_r: t -> Var.R.t -> AffExpr.t -> unit

val widen_poly: t -> PolyBase.t -> unit
