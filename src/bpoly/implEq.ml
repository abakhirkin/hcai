(** Implications with Boolean body and equality in the head. *)

open U

type t = {
  t_cond: BoolExpr.t;
  t_eq: Eq.t;
  t_max_abs_coeff: Z.t;
  mutable t_terms: ImplTerms.t option
}

let ctx ieq =
  BoolExpr.ctx ieq.t_cond

let cond ieq =
  ieq.t_cond

let eq ieq =
  ieq.t_eq

let max_abs_coeff ieq =
  ieq.t_max_abs_coeff

let make cond eq =
  { t_cond = cond;
    t_eq = eq;
    t_max_abs_coeff = Eq.max_abs_coeff eq;
    t_terms = None }

let of_eq ctx eq =
  make (BoolExpr.make_true ctx) eq

let is_true ieq =
  (BoolExpr.is_false (cond ieq)) || (Eq.is_true (eq ieq))

let is_false ieq =
  (BoolExpr.is_true (cond ieq)) && (Eq.is_false (eq ieq))

let equal ieq1 ieq2 =
  (BoolExpr.equal (cond ieq1) (cond ieq2)) &&
  (Eq.equal (eq ieq1) (eq ieq2))

(** Creates a linear combination of two inequalities that eliminates a given
    variable. *)
let eliminate_r i ieq1 ieq2 =
  (* TODO: Maybe also fill eq_terms if they are present in the operands. *)
  check_arg (ctx ieq1 == ctx ieq2) "Equalities should be from the same context.";
  make
    (BoolExpr.make_and (cond ieq1) (cond ieq2))
    (Eq.eliminate i (eq ieq1) (eq ieq2))

let make_terms ieq =
  let ctx = ctx ieq in
  let cond_term = BoolExpr.ensure_term (cond ieq) in
  let lincons_term = Eq.make_term ctx (eq ieq) in
  ImplTerms.make ctx cond_term lincons_term

let ensure_terms ieq =
  match ieq.t_terms with
  | Some terms -> terms
  | None ->
      let terms = make_terms ieq in
      ieq.t_terms <- Some terms;
      terms

let rename_r ?free_var f ieq =
  make (cond ieq) (Eq.rename ?free_var:free_var f (eq ieq))

let inv_replace_r var aff_expr ieq =
  make (cond ieq) (Eq.inv_replace var aff_expr (eq ieq))

let rename_b f ieq =
  make (BoolExpr.rename f (cond ieq)) (eq ieq)

let constrain_b var value ieq =
  make (BoolExpr.constrain var value (cond ieq)) (eq ieq)

let iter_vars_r f ieq =
  Eq.iter_vars f (eq ieq)
