open Format
open FormatU

module DynArray = Batteries.DynArray

type terms_t = {
  terms_poly: Z3.Expr.expr; (** Term for the whole polyhedron. *)
  terms_npoly: Z3.Expr.expr; (** Negation of the term for the whole polyhedron. *)
  terms_false_cond: Z3.Expr.expr; (** Term for the falsity condition. *)
  terms_nfalse_cond: Z3.Expr.expr (** Term for negation of the falsity condition. *)
}

type t = {
  t_ctx: Ctx.t;
  t_eqs: ImplEq.t DynArray.t;
  t_ineqs: ImplIneq.t DynArray.t;  
  t_false_cond: BoolExpr.t;
  mutable t_terms: terms_t option
}

let format_eq ?format_rvar ?format_bvar fmt eq =
  pp_open_box fmt 0;
  BoolExpr.format ?format_var:format_bvar fmt (ImplEq.cond eq);
  pp_print_space fmt ();
  pp_print_string fmt "->";
  pp_print_space fmt ();
  Eq.format ?format_var:format_rvar fmt (ImplEq.eq eq);
  pp_close_box fmt ()

let format_ineq ?format_rvar ?format_bvar fmt ineq =
  pp_open_box fmt 0;
  BoolExpr.format ?format_var:format_bvar fmt (ImplIneq.cond ineq);
  pp_print_space fmt ();
  pp_print_string fmt "->";
  pp_print_space fmt ();
  Ineq.format ?format_var:format_rvar fmt (ImplIneq.ineq ineq);
  pp_close_box fmt ()

let format fmt ctx eqs ineqs false_cond =
  pp_open_box fmt 0;
  pp_print_string fmt "[";
  let empty = ref true in
  if DynArray.length eqs > 0 then begin
    empty := false;
    format_dynarray
      ~print_sep:(fun fmt -> pp_print_string fmt ";"; pp_print_space fmt ())
      (format_eq
        ~format_rvar:(Ctx.format_rvar ctx)
        ~format_bvar:(Ctx.format_bvar ctx))
      fmt
      eqs;        
  end else ();
  if DynArray.length ineqs > 0 then begin
    if not !empty then begin
      pp_print_string fmt ";";
      pp_print_space fmt ()
    end else ();
    empty := false;
    format_dynarray
      ~print_sep:(fun fmt -> pp_print_string fmt ";"; pp_print_space fmt ())
      (format_ineq
        ~format_rvar:(Ctx.format_rvar ctx)
        ~format_bvar:(Ctx.format_bvar ctx))
      fmt
      ineqs;    
  end else ();
  if not (BoolExpr.is_false false_cond) then begin
    if not !empty then begin
      pp_print_string fmt ";";
      pp_print_space fmt ()
    end else ();
    empty := false;
    BoolExpr.format fmt false_cond;
    pp_print_space fmt ();
    pp_print_string fmt "->";
    pp_print_space fmt ();
    pp_print_string fmt "false"
  end else ();
  if !empty then
    pp_print_string fmt "true -> true"
  else ();
  pp_print_string fmt "]";
  pp_close_box fmt ()
