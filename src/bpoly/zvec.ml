(** Sparse vectors of rational (Zarith) coefficients.
    
    Vectors are represented as hashtables.
    Vectors are mutable, but for most operation, the module provides both
    the inplace version and the version that returns a new vector. *)

open Format
open FormatU

(** Defines type of the index and functions required to be able to use it as
    hashtable index. *)
module type IndexT = sig
  (** Type of the index. *)
  type t

  (** Whether two indices are equal. *)
  val equal: t -> t -> bool

  (** Hash of the index. *)
  val hash: t -> int

  (** Compares two indices. *)
  val compare: t -> t -> int

  (** Minimum index. *)
  val min: t -> t -> t

  (** Maximum index. *)
  val max: t -> t -> t

  (** Formats an index. *)
  val format: formatter -> t -> unit
end

(** Creates the module for vectors indexed by a given type. *)
module Make(Idx: IndexT) = struct

  module IdxHashtbl = Hashtbl.Make(Idx)

  (** Sparse vector.
      
      Represented a a hashtable mapping indices to Zarith numbers. *)
  type t = {
    t_entries: Z.t IdxHashtbl.t
  } [@@unboxed]

  (** Whether the value at a given index is not 0. *)
  let coeff_is_nonzero vec i =
    IdxHashtbl.mem vec.t_entries i

  (** Whether the value at a given index is 0. *)
  let coeff_is_zero vec i =
    not (coeff_is_nonzero vec i)

  (** Number of non-zero entries. *)
  let nonzero_count vec =
    IdxHashtbl.length vec.t_entries

  (** Whether all the entries are zero. *)
  let is_zero vec =
    (nonzero_count vec) = 0

  (** Gets the value at a given index. *)
  let get vec i =
    try
      IdxHashtbl.find vec.t_entries i
    with
    | Not_found -> Z.zero

  (** Sets the value at a given index. *)
  let set vec i v =
    if Z.equal v Z.zero then
      IdxHashtbl.remove vec.t_entries i
    else
      IdxHashtbl.replace vec.t_entries i v

  (** If a value at a index is 0, replaces it with a new value,
      otherwise throws an exception. *)
  let replace_zero vec i v =
    if coeff_is_nonzero vec i then
      invalid_arg "Current value should be 0."
    else
      set vec i v

  (** Creates a zero vector. *)
  let make_zero () =
    { t_entries = IdxHashtbl.create 0 }

  (** Creates a vector from the aray of pairs: index, {!Z.t} value. *)
  let of_coeffs entry_array =
    let vec = { t_entries = IdxHashtbl.create (Array.length entry_array) } in
    Array.iter
      (fun (i, v) -> set vec i v)
      entry_array;
    vec

  (** Creates a vector from the aray of pairs: index, {!int} value. *)
  let of_int_coeffs entries =
    let vec = { t_entries = IdxHashtbl.create (Array.length entries) } in
    Array.iter
      (fun (i, v) -> set vec i (Z.of_int v))
      entries;
    vec

  (** Creates a copy of the vector. *)
  let copy vec =
    { t_entries = IdxHashtbl.copy vec.t_entries }

  (** Iterates the funtions over the pairs (index, value). *)
  let iter fn vec =
    IdxHashtbl.iter fn vec.t_entries

  (** Folds the pairs (index, value) using the function. *)
  let fold fn vec acc =
    IdxHashtbl.fold fn vec.t_entries acc

  (** Maps every pair (index, value) to a new value in place. *)
  let map_inplace fn vec =
    IdxHashtbl.filter_map_inplace
      (fun i v ->
        let new_v = fn i v in
        if Z.equal new_v Z.zero then None else Some new_v)
      vec.t_entries

  let neg vec =
    let res = { t_entries = IdxHashtbl.create (nonzero_count vec) } in
    iter (fun i v -> set res i (Z.neg v)) vec;
    res

  (** Multiplies the vector by a constant in place. *)
  let mul_inplace vec z =
    map_inplace (fun _ v -> Z.mul v z) vec

  (** Multiplies the vector by a constant creating a new vector. *)
  let mul vec z =
    let res = copy vec in
    mul_inplace res z;
    res

  (** Divides (with integer division) the vector by a constant in place. *)
  let div_inplace vec z =
    map_inplace (fun _ v -> Z.div v z) vec

  (** Divides (with integer division) the vector by a constant creating a new
      vector. *)
  let div vec z =
    let res = copy vec in
    div_inplace res z;
    res

  (** Divides the vector by a constant in place, assuming all the values
      are divisible exactly. *)
  let divexact_inplace vec z =
    map_inplace (fun _ v -> Z.divexact v z) vec

  (** Divides the vector by a constant creating a new vector, assuming all the
      values are divisible exactly. *)
  let divexact vec z =
    let res = copy vec in
    divexact_inplace res z;
    res

  (** Adds the second vector to the first vector in place.

      [add_inplace vec_to vec_from] modifies [vec_to] to be the sum of the
      vectors. *)
  let add_inplace vec_to vec_from =    
    iter
      (fun i v -> set vec_to i (Z.add (get vec_to i) v))
      vec_from

  let add_coeff_inplace vec_to vec_from coeff_from =
    iter
      (fun i v -> set vec_to i (Z.add (get vec_to i) (Z.mul v coeff_from)))
      vec_from

  (** Adds two vectors producing a new vector. *)
  let add vec_to vec_from =
    let res = copy vec_to in
    add_inplace res vec_from;
    res

  (** Creates the linear combination of two vectors in place.
      
      [lincomb_inplace vec_to coeff_to vec_from coeff_from] replaces [vec_to]
      with the linear combination. *)
  let lincomb_inplace vec_to coeff_to vec_from coeff_from =
    mul_inplace vec_to coeff_to;
    add_coeff_inplace vec_to vec_from coeff_from

  (** Creates the linear combination of two vectors creating a new vector. *)
  let lincomb vec_to coeff_to vec_from coeff_from =
    let res = copy vec_to in
    lincomb_inplace res coeff_to vec_from coeff_from;
    res

  (** Creates the linear combination of a collection of pairs (vector, coefficient). *)
  let lincomb_iter iter_f vecs_coeffs =
    let res = make_zero () in
    iter_f
      (fun (v, c) -> add_coeff_inplace res v c)
      vecs_coeffs;
    res

  (** Creates the linear combination of an array of pairs (vector, coefficient). *)
  let lincomb_array vecs_coeffs =
    lincomb_iter Array.iter vecs_coeffs

  (** Reorders the entries of the vector, creating a new vector.
   
      The funtion [f] should not map two different indices to the same index. *)
  let rename f vec =
    let res = make_zero () in
    IdxHashtbl.iter
      (fun i v ->
        match f i with
        | Some j -> replace_zero res j v
        | None -> invalid_arg (asprintf "No mapping for index: %a" Idx.format i))
      vec.t_entries;
    res

  (** Creates an array of vector entries (index, value). *)
  let to_array vec =
    (* TODO: Do this without an extra array copy. *)
    let entries = DynArray.make (nonzero_count vec) in
    IdxHashtbl.iter (fun i v -> DynArray.add entries (i, v)) vec.t_entries;
    DynArray.to_array entries

  exception Not_equal
  (** Whether two vectors are equal. *)
  let equal vec1 vec2 =
    try
      iter
        (fun i v -> if Z.equal (get vec2 i) v then () else raise Not_equal)
        vec1;
      iter
        (fun i v -> if Z.equal (get vec1 i) v then () else raise Not_equal)
        vec2;
      true
    with
    | Not_equal -> false

  (** Minimal index with non-zero value. *)
  let min_index vec =
    let opt_min idx1_opt idx2 =
      match idx1_opt with
      | None -> Some idx2
      | Some idx1 -> Some (Idx.min idx1 idx2)
    in
    fold
      (fun i v acc -> opt_min acc i)
      vec
      None

  (** Maximal index with non-zero value. *)
  let max_index vec =
    let opt_max idx1_opt idx2 =
      match idx1_opt with
      | None -> Some idx2
      | Some idx1 -> Some (Idx.max idx1 idx2)
    in
    fold
      (fun i v acc -> opt_max acc i)
      vec
      None

  let max_abs_coeff vec =
    if is_zero vec then
      Z.zero
    else
      let max = ref Z.zero in
      let first = ref true in
      iter
        (fun _ v ->
          let v = Z.abs v in
          if !first then begin
            max := v;
            first := false
          end else
            max := Z.max !max v)
        vec;
      !max

  (** Formats a vector. *)
  let format fmt vec =
    pp_open_box fmt 0;
    pp_print_string fmt "(";
    (* There's no sorting for DynArrays in Batteries. *)
    let entries = to_array vec in
    Array.sort (fun (i1, v1) (i2, v2) -> Idx.compare i1 i2) entries;
    format_array
      ~print_sep:(fun fmt -> pp_print_string fmt ";"; pp_print_space fmt ())
      (fun fmt (i, v) ->
        Idx.format fmt i;
        pp_print_string fmt ":";
        Z.pp_print fmt v)
      fmt
      entries;
    pp_print_string fmt ")";
    pp_close_box fmt ()

  (** Formats a vector to string. *)
  let to_string vec =
    asprintf "%a" format vec
end

(** Implements functions to use [int] as vector index. *)
module IntIdx = struct
  type t = int

  let equal i j = i = j

  let hash i = i

  let compare i j = i - j

  let min = Pervasives.min

  let max = Pervasives.max

  let format = pp_print_int
end

(** Implements vector with [int] index. *)
module Int = Make(IntIdx)
module Rvar = Make(Var.R)
