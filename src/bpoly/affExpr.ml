(** Affine expressions over variables of type {!Var.R.t}. *)

open Format

module DynArray = Batteries.DynArray

module Zvec = Zvec.Rvar

(** Linear expression [cx].
 
    Stored as a sparse vector of coefficients [c].
    The vector of variables [x] is implicit. *)
type lin_expr_t = Zvec.t

(** Affine expression [cx - b].
 
    Where [aff_expr_lin_expr] is [c] and [aff_expr_free_coeff] is [b].
    *)
type t = {
  t_lin_expr: lin_expr_t;
  t_free_coeff: Z.t
}

(** Linear part of the affine expression.
    
    @return Linear expression [cx] for the affine expression [cx - b]. *)
let lin_expr aff_expr =
  aff_expr.t_lin_expr

(** Free coefficient of the affine expression.
    
    @return [b] for the affine expression [cx - b]. *)
let free_coeff aff_expr =
  aff_expr.t_free_coeff

(** Formats a linear expression.
    
    Formats a vector [c] as a linear expression [cx]. *)
let format_lin_expr ?(format_var=Var.R.format) fmt lin_expr =
  let format_coeff c =
    if Z.equal c Z.one then
      ()
    else if Z.equal c Z.minus_one then
      pp_print_string fmt "-"
    else begin
      Z.pp_print fmt c;
      pp_print_string fmt "*";
    end
  in
  pp_open_box fmt 0;
  let entries = Zvec.to_array lin_expr in
  let len = Array.length entries in
  Array.sort (fun (i1, v1) (i2, v2) -> Var.R.compare i1 i2) entries;
  if len = 0 then
    pp_print_string fmt "0"
  else begin
    let (var0, val0) = entries.(0) in
    format_coeff val0;
    format_var fmt var0;
    for i = 1 to len - 1 do
      pp_print_space fmt ();
      let (var_i, val_i) = entries.(i) in
      if Z.sign val_i >= 0 then begin
        pp_print_string fmt "+";
        pp_print_space fmt ();
        format_coeff val_i
      end else begin
        pp_print_string fmt "-";
        pp_print_space fmt ();
        format_coeff (Z.neg val_i)
      end;
      format_var fmt var_i
    done
  end;
  pp_close_box fmt ()

(** Formats an affine expression. *)
let format fmt aff_expr =
  pp_open_box fmt 0;
  format_lin_expr fmt (lin_expr aff_expr);
  pp_print_space fmt ();
  pp_print_string fmt "-";
  pp_print_space fmt ();
  Z.pp_print fmt (free_coeff aff_expr);
  pp_close_box fmt ()

let to_string aff_expr =
  asprintf "%a" format aff_expr

(** Coefficient of a variable in the linear part of the expression. *)
let get aff_expr i =
  Zvec.get (lin_expr aff_expr) i

(** Whether the coefficient of a variable is non-zero. *)
let coeff_is_nonzero aff_expr i =
  Zvec.coeff_is_nonzero (lin_expr aff_expr) i

(** Creates a new affine expression.
 
    @return [cx - b] when the arguments are [cx] and [b] respectively. *)
let make lin_expr free_coeff =
  { t_lin_expr = lin_expr; t_free_coeff = free_coeff }

(** Creates a new affine expression from a variable. *)
let of_var var =
  { t_lin_expr = Zvec.of_int_coeffs [| (var, 1) |];
    t_free_coeff = Z.zero }

(** Creates a new affine expression from a {!Z.t} number.

    [of_n n] returns the expression [0x - (-n)]. *)
let of_const n =
  { t_lin_expr = Zvec.make_zero ();
    t_free_coeff = (Z.neg n)}

(** Creates a new affine expression from an {!int} number. *)
let of_int_const n =
  of_const (Z.of_int n)

(** Creates a new affine expressiion from an array of entries
   (variable, {!Z.t} coefficient), and a {!Z.t} free coefficient. *)
let of_coeffs coeffs free_coeff =
  { t_lin_expr = Zvec.of_coeffs coeffs;
    t_free_coeff = free_coeff }

(** Creates a new affine expressiion from an array of entries
   (variable, {!int} coefficient), and an {!int} free coefficient. *)
let of_int_coeffs coeffs free_coeff =
  { t_lin_expr = Zvec.of_int_coeffs coeffs;
    t_free_coeff = Z.of_int free_coeff }

let neg aff_expr =
  { t_lin_expr = Zvec.neg (lin_expr aff_expr);
    t_free_coeff = Z.neg (free_coeff aff_expr) }

(** Whether two affine expressions are equal. *)
let equal expr1 expr2 =
  (Zvec.equal (lin_expr expr1) (lin_expr expr2)) &&
  (Z.equal (free_coeff expr1) (free_coeff expr2))

(** GCD of the variable coefficients and the free coefficient.
    
    The result is always strictly positive. *)
let gcd expr =
  if Zvec.is_zero (lin_expr expr) then begin
    let free_coeff = free_coeff expr in
    if ZU.is_zero free_coeff then
      Z.one
    else
      Z.abs free_coeff
  end else
    Zvec.fold
      (fun _ v acc -> ZU.safe_gcd acc v)
      (lin_expr expr)
      (free_coeff expr)
  
(** Divides the affine expression by the GCD of its coefficients. *)
let div_gcd expr =
  let gcd = gcd expr in
  if Z.equal gcd Z.one then
    expr
  else
    make (Zvec.divexact (lin_expr expr) gcd) (Z.divexact (free_coeff expr) gcd)

(** GCD of the variable coefficients and the free coefficient, taken with
    the same sign as the coefficient of the variable with smallest id.
    
    The result is always non-zero. *)
let eq_gcd expr =
  let (_, min_coeff_neg) =
    Zvec.fold
      (fun i v (min_i, min_i_neg) ->
        if (Var.R.compare i min_i) < 0 then (i, (Z.sign v) < 0) else (min_i, min_i_neg))
      (lin_expr expr)
      (Var.R.max_var, false)
  in
  let gcd = gcd expr in
  if min_coeff_neg then Z.neg gcd else gcd

(** Divides the affine expression by its [eq_gcd]. *)
let div_eq_gcd expr =
  let gcd = eq_gcd expr in
  if Z.equal gcd Z.one then
    expr
  else
    make (Zvec.divexact (lin_expr expr) gcd) (Z.divexact (free_coeff expr) gcd)

(** Renames variables and possibly multiplies free coefficient by a variable.
   
    [rename ?free_var f aff_expr] renames every variable in the linear part
    using the renaming function [f].
    If [free_var] is not [None], free coefficient is multiplied by [free_var]
    (i.e., [free_var] is taken with the coefficient [-b]) and the new free
    coefficient becomes 0.
    The renaming function [f] should not map two different variables to the same
    variable.
    After applying [f] to the linear part, the coefficient of [free_var] should
    be 0.
    *)
let rename ?free_var f aff_expr =
  let renamed = Zvec.rename f (lin_expr aff_expr) in
  match free_var with
  | Some free_var ->
      Zvec.replace_zero renamed free_var (Z.neg (free_coeff aff_expr));
      make renamed Z.zero
  | None ->
      make renamed (free_coeff aff_expr)

(** Minimum (in id order) variable with non-zero coefficient. *)
let min_var aff_expr =
  Zvec.min_index (lin_expr aff_expr)

(** Maximum (in id order) variable with non-zero coefficient. *)
let max_var aff_expr =
  Zvec.max_index (lin_expr aff_expr)

(** Creates a linear combination of two affine expressions. *)
let lincomb aff_expr1 coeff1 aff_expr2 coeff2 =
  make
    (Zvec.lincomb (lin_expr aff_expr1) coeff1 (lin_expr aff_expr2) coeff2)
    (Z.add (Z.mul (free_coeff aff_expr1) coeff1) (Z.mul (free_coeff aff_expr2) coeff2))

(** TODO: Unit tests for plus/minus. *)

(** Creates a sum of two affine expressions. *)
let plus aff_expr1 aff_expr2 =
  lincomb aff_expr1 Z.one aff_expr2 Z.one

(** Creates a difference of two affine expressions. *)
let minus aff_expr1 aff_expr2 =
  lincomb aff_expr1 Z.one aff_expr2 Z.minus_one

(** TODO: Tests mul. *)

(** Creates a product, assuming one of the expressions is a constant. *)
let mul aff_expr1 aff_expr2 =
  if Zvec.is_zero (lin_expr aff_expr1) then
    let const1 = Z.neg (free_coeff aff_expr1) in
    make
      (Zvec.mul (lin_expr aff_expr2) const1)
      (Z.mul (free_coeff aff_expr2) const1)
  else if Zvec.is_zero (lin_expr aff_expr2) then
    let const2 = Z.neg (free_coeff aff_expr2) in
    make
      (Zvec.mul (lin_expr aff_expr1) const2)
      (Z.mul (free_coeff aff_expr1) const2)
  else
    invalid_arg "At least one expression should be a constant."

(** Creates a linear combination of an array of entries
   (affine expression, coefficient). *)
let lincomb_array aff_exprs_coeffs =
  make
    (Zvec.lincomb_iter
      (fun f -> Array.iter (fun (aff_expr, coeff) -> f (lin_expr aff_expr, coeff)))
      aff_exprs_coeffs)
    (Array.fold_left
      (fun acc (aff_expr, coeff) -> (Z.add acc (Z.mul (free_coeff aff_expr) coeff)))
      Z.zero
      aff_exprs_coeffs)

(** Inverse replace.
  
    [inv_replace var with_expr in_expr] returns a pair [(expr, coeff)], where [coeff] 
    is the coefficient of [var] in [expr].
    
    Replacing [var] with [with_expr] in [expr] and then dividing by [coeff] results in
    [in_expr]. *)
let inv_replace var with_expr in_expr =
  let with_coeff = get with_expr var in
  let in_coeff = get in_expr var in
  (* NOTE: We multiply in_expr by a positive number. *)
  match Z.sign with_coeff with
  | 1 ->
      (lincomb_array
        [| (in_expr, with_coeff);
           (with_expr, Z.neg in_coeff);
           (of_var var, in_coeff) |],
       with_coeff)
  | (-1) ->
      let neg_with_coeff = Z.neg with_coeff in
      (lincomb_array
        [| (in_expr, neg_with_coeff);
           (with_expr, in_coeff);
           (of_var var, Z.neg in_coeff) |],
       neg_with_coeff)
  | _  -> (* 0 *)
      invalid_arg "Variable coefficient in the replacing expression should not be 0."

(** Creates a Z3 term from a linear expression. *)
let make_lin_expr_term ctx lin_expr =
  let z3 = Ctx.z3 ctx in
  let make_entry_term i v =
    let var_term = Ctx.make_rvar_term ctx i in
    if Z.equal v Z.one then
      var_term
    else if Z.equal v Z.minus_one then
      Z3.Arithmetic.mk_unary_minus z3 var_term
    else
      Z3.Arithmetic.mk_mul z3 [(ZU.make_term z3 v); var_term]
  in
  let sum_terms =
    Zvec.fold (fun i v acc -> (make_entry_term i v)::acc) lin_expr [] in
  match sum_terms with
  | [] -> Z3.Arithmetic.Integer.mk_numeral_i z3 0
  | [sum_term] -> sum_term
  | _ -> Z3.Arithmetic.mk_add z3 (List.rev sum_terms)

let iter_vars f aff_expr =
  Zvec.iter
    (fun var _ -> f var)
    (lin_expr aff_expr)

let max_abs_coeff aff_expr =
  Z.max
    (Zvec.max_abs_coeff (lin_expr aff_expr))
    (Z.abs (free_coeff aff_expr))
