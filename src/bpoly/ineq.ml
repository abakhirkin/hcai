(** Inequalities. *)

open U
open Format

module Zvec = Zvec.Rvar

(** Comparison sign. *)
type sign_t =
  | Geq (** Greater or equal.*)
  | Gt (** Strictly greater. *)

let sign_flip sign = 
  match sign with
  | Geq -> Gt
  | Gt -> Geq

(** Inequality [cx >= b] or [cx > b].
    
    Where [ineq_aff_expr] is an expression [cx - b] and [ineq_sign] is the sign,
    [>=] or [>].
    GCD of the coefficients of [c] and [b] is 1. *)
type t = {
  t_aff_expr: AffExpr.t;
  t_sign: sign_t
}

(** Affine expression corresponding to the inequality.
    
    @return [cx - b] for the inequality [cx >= b] or [cx > b]. *)
let aff_expr ineq =
  ineq.t_aff_expr

(** Linear part of the inequality.
 
    @return [cx] for the inequality [cx >= b] or [cx > b]. *)
let lin_expr ineq =
  AffExpr.lin_expr ineq.t_aff_expr

(** Free coefficient of the inequality.
 
    @return [b] for the inequality [cx >= b] or [cx > b]. *)
let free_coeff ineq =
  AffExpr.free_coeff ineq.t_aff_expr

(** Sign of the inequality. *)
let sign ineq =
  ineq.t_sign

(** Coefficient of a variable. *)
let get ineq i =
  AffExpr.get (aff_expr ineq) i

(** Whether the coefficient of a variable is non-zero. *)
let coeff_is_nonzero ineq i =
  AffExpr.coeff_is_nonzero (aff_expr ineq) i

(** Creates a new inequality from an affine expression and sign. *)
let make aff_expr sign =
  { t_aff_expr = AffExpr.div_gcd aff_expr;
    t_sign = sign }

let close ineq =
  { t_aff_expr = ineq.t_aff_expr;
    t_sign = Geq }

(** Creates a new inequality from a linear expression, free coefficient, and sign. *)
let of_expr_coeff lin_expr free_coeff sign =
  make (AffExpr.make lin_expr free_coeff) sign

(** Creates a new equality from an array of entries (variable, {!Z.t} coefficient),
    a {!Z.t} free coefficient, and a sign. *)
let of_coeffs lin_expr_coeffs free_coeff sign =
  make (AffExpr.make (Zvec.of_coeffs lin_expr_coeffs) free_coeff) sign

(** Creates a new equality from an array of entries (variable, {!int} coefficient),
    an {!int} free coefficient, and a sign. *)
let of_int_coeffs lin_expr_coeffs free_coeff sign =
  make (AffExpr.make (Zvec.of_int_coeffs lin_expr_coeffs) (Z.of_int free_coeff)) sign

let neg ineq =
  make (AffExpr.neg (aff_expr ineq)) (sign_flip (sign ineq))

(** Whether two inequalities are the same.
 
    Since the inequalities have canonical form, the function just compares the
    coefficients. *)
let equal ineq1 ineq2 =
  (AffExpr.equal (aff_expr ineq1) (aff_expr ineq2)) &&
  ((sign ineq1) = (sign ineq2))

(** Whether the inequality is always true. *)
let is_true ineq =
  match sign ineq with
  | Geq ->
      (Zvec.is_zero (lin_expr ineq)) && (Z.leq (free_coeff ineq) Z.zero)
  | Gt ->
      (Zvec.is_zero (lin_expr ineq)) && (Z.lt (free_coeff ineq) Z.zero)

(** Whether the inequality is always false. *)
let is_false ineq =
  match sign ineq with
  | Geq ->
      (Zvec.is_zero (lin_expr ineq)) && (Z.gt (free_coeff ineq) Z.zero)
  | Gt ->
      (Zvec.is_zero (lin_expr ineq)) && (Z.geq (free_coeff ineq) Z.zero)

(** Whether one inequality implies the other.
 
    [implies ineq1 ineq2] returns [true] if [ineq1] implies [ineq2]. *)
let implies ineq1 ineq2 =
  (is_false ineq1) ||
  (is_true ineq2) ||
  ((Zvec.equal (lin_expr ineq1) (lin_expr ineq2)) &&
   begin match sign ineq1, sign ineq2 with
   | Geq, Gt -> Z.gt (free_coeff ineq1) (free_coeff ineq2)
   | _ -> Z.geq (free_coeff ineq1) (free_coeff ineq2)
   end)

(** Whether an equality implies an inequality.
 
    [implies eq1 ineq2] returns [true] if [eq1] implies [ineq2]. *)
let eq_implies eq1 ineq2 =
  (Eq.is_false eq1) ||
  (is_true ineq2) ||
  ((Zvec.equal (Eq.lin_expr eq1) (lin_expr ineq2)) &&
   begin match sign ineq2 with
   | Gt -> Z.gt (Eq.free_coeff eq1) (free_coeff ineq2)
   | Geq -> Z.geq (Eq.free_coeff eq1) (free_coeff ineq2)
   end)

(** Renames variables and possibly multiplies free coefficient by a variable.
    
    See {!AffExpr.rename}. *)
let rename ?free_var f ineq =
  make (AffExpr.rename ?free_var:free_var f (aff_expr ineq)) (sign ineq)

(** Inverse replace.
    
    [inv_replace var expr ineq] returns the inequality, s.t. replacing [var] with
    [expr] in it results in [ineq]. *)
let inv_replace var with_expr ineq =
  let aff_expr, _ = AffExpr.inv_replace var with_expr (aff_expr ineq) in
  make aff_expr (sign ineq)

(** Minimum (in id order) variable with non-zero coefficient. *)
let min_var ineq =
  AffExpr.min_var (aff_expr ineq)

(** Maximum (in id order) variable with non-zero coefficient. *)
let max_var ineq =
  AffExpr.max_var (aff_expr ineq)

(** Creates a positive combination of two inequalities that eliminates a given
    variable.
    
    In the two inequalities, coefficients of the eliminated variable should be
    of different sign and should not be zero. *)
let eliminate i ineq1 ineq2 =
  let aff_expr1 = aff_expr ineq1 in
  let aff_expr2 = aff_expr ineq2 in
  let icoeff1 = AffExpr.get aff_expr1 i in
  let icoeff2 = AffExpr.get aff_expr2 i in
  let sign1 = Z.sign icoeff1 in
  let sign2 = Z.sign icoeff2 in
  check_arg (sign1 <> 0)
    "Coefficients of the eliminated variable should not be 0.";
  check_arg (sign2 <> 0)
    "Coefficients of the eliminated variable should not be 0.";
  check_arg (sign1 <> sign2)
    "Coefficients of the eliminated variable should have different sign.";
  let lincomb_sign =
    match (sign ineq1), (sign ineq2) with
    | Geq, Geq -> Geq
    | _ -> Gt
  in
  make
    (AffExpr.lincomb aff_expr1 (Z.abs icoeff2) aff_expr2 (Z.abs icoeff1))
    lincomb_sign

(** Formats an inequality. *)
let format ?format_var fmt ineq =
  pp_open_box fmt 0;
  AffExpr.format_lin_expr ?format_var:format_var fmt (lin_expr ineq);
  pp_print_space fmt ();
  begin match sign ineq with
  | Geq -> pp_print_string fmt ">=" 
  | Gt -> pp_print_string fmt ">" 
  end;
  pp_print_space fmt ();
  Z.pp_print fmt (free_coeff ineq);
  pp_close_box fmt ()

(** Formats an inequality to string. *)
let to_string ?format_var ineq =
  asprintf "%a" (format ?format_var:format_var) ineq

(** Creates a combination of and equality and an inequality that eliminates a given
    variable. *)
let eq_eliminate i eq1 ineq2 =
  let aff_expr1 = Eq.aff_expr eq1 in
  let aff_expr2 = aff_expr ineq2 in
  let icoeff1 = AffExpr.get aff_expr1 i in
  let icoeff2 = AffExpr.get aff_expr2 i in
  let sign1 = Z.sign icoeff1 in
  let sign2 = Z.sign icoeff2 in
  check_arg (sign1 <> 0)
    "Coefficients of the eliminated variable should not be 0.";
  check_arg (sign2 <> 0)
    "Coefficients of the eliminated variable should not be 0.";
  if sign1 <> sign2 then
    make
      (AffExpr.lincomb aff_expr1 (Z.abs icoeff2) aff_expr2 (Z.abs icoeff1))
      (sign ineq2)
  else
    (* NOTE: We make sure that we multiply the inequality (aff_expr2) by a
       positive number. *)
    make
      (AffExpr.lincomb aff_expr1 (Z.neg (Z.abs icoeff2)) aff_expr2 (Z.abs icoeff1))
      (sign ineq2)

(** Creates a Z3 term from an inequality. *)
let make_term ctx ineq =
  let z3 = Ctx.z3 ctx in
  let expr_term = AffExpr.make_lin_expr_term ctx (lin_expr ineq) in
  let free_coeff_term = ZU.make_term z3 (free_coeff ineq) in
  match sign ineq with
  | Geq ->
      Z3.Arithmetic.mk_ge z3 expr_term free_coeff_term  
  | Gt ->
      Z3.Arithmetic.mk_gt z3 expr_term free_coeff_term  

let iter_vars f ineq =
  AffExpr.iter_vars f (aff_expr ineq)

let geq_of_eq eq =
  make (Eq.aff_expr eq) Geq

let leq_of_eq eq =
  make (AffExpr.neg (Eq.aff_expr eq)) Geq

let of_operands aff_expr1 aff_expr2 sign =
  make (AffExpr.lincomb aff_expr1 Z.one aff_expr2 Z.minus_one) sign

let max_abs_coeff ineq =
  AffExpr.max_abs_coeff (aff_expr ineq)
