(** Z3 terms corresponding to an implication. *)
type t = {
  t_cond: Z3.Expr.expr; (** Term for the Boolean condition. *)
  t_lincons: Z3.Expr.expr; (** Term for the linear constraint. *)
  t_impl: Z3.Expr.expr; (** Term for the whole implication. *)
  t_impl_assume: Z3.Expr.expr; (** Literal for assuming that the implication
                                        holds. *)
  t_nimpl_assume: Z3.Expr.expr; (** Literal for assuming that the implication
                                         does not hold. *)
  t_assumed_impl: Z3.Expr.expr; (** Assumed implication. *)
  t_assumed_nimpl: Z3.Expr.expr (** Assumed negation of the implication. *)
}

let cond iterms =
  iterms.t_cond

let lincons iterms =
  iterms.t_lincons

let impl iterms =
  iterms.t_impl

let impl_assume iterms =
  iterms.t_impl_assume

let nimpl_assume iterms =
  iterms.t_nimpl_assume

let assumed_impl iterms =
  iterms.t_assumed_impl

let assumed_nimpl iterms =
  iterms.t_assumed_nimpl

let make ctx cond_term lincons_term =
  let z3 = Ctx.z3 ctx in
  let bool_sort = Z3.Boolean.mk_sort z3 in
  let impl =  Z3.Boolean.mk_implies z3 cond_term lincons_term in
  let nimpl =  Z3.Boolean.mk_and z3 [cond_term; Z3.Boolean.mk_not z3 lincons_term] in
  let impl_assume = Z3.Expr.mk_fresh_const z3 "as" bool_sort in
  let nimpl_assume = Z3.Expr.mk_fresh_const z3 "as" bool_sort in
  { t_cond = cond_term;
    t_lincons = lincons_term;
    t_impl = impl;
    t_impl_assume = impl_assume; 
    t_nimpl_assume = nimpl_assume; 
    t_assumed_impl = Z3.Boolean.mk_implies z3 impl_assume impl;
    t_assumed_nimpl = Z3.Boolean.mk_implies z3 nimpl_assume nimpl }
