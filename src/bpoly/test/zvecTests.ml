open OUnit2
open Format

module Zvec = Zvec.Rvar

let test_add_vec _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let v1 = Zvec.of_coeffs [| (x0, Z.one); (x2, Z.one) |] in
  let v2 = Zvec.of_coeffs [| (x0, Z.minus_one); (x1, Z.one); (x2, Z.one) |] in
  let expected1 = Zvec.of_coeffs [| (x1, Z.one); (x2, Z.of_int 2) |] in
  assert_equal ~cmp:Zvec.equal ~printer:Zvec.to_string
    expected1
    (Zvec.add v1 v2);
  let expected2 = Zvec.of_coeffs
    [| (x0, Z.minus_one); (x1, Z.of_int 3); (x2, Z.of_int 5) |] in
  assert_equal ~cmp:Zvec.equal ~printer:Zvec.to_string
    expected2
    (Zvec.lincomb v1 (Z.of_int 2) v2 (Z.of_int 3))
  
let test_format_vec _ =
  let x0 = Var.R.make 0 in
  let x2 = Var.R.make 2 in
  let x4 = Var.R.make 4 in
  let v0 = Zvec.of_coeffs [||] in
  let s0 = asprintf "%a" Zvec.format v0 in
  assert_equal ~printer:(fun s->s) "()" s0;
  let v1 = Zvec.of_coeffs
    [| (x4, Z.of_int 4); (x0, Z.of_int 0); (x2, Z.of_int 2) |] in
  let s1 = asprintf "%a" Zvec.format v1 in
  assert_equal  ~printer:(fun s->s) "(x2:2; x4:4)" s1

let test_rename _ =
  let x0 = Var.R.make 0 in
  let x2 = Var.R.make 2 in
  let x3 = Var.R.make 3 in
  let x4 = Var.R.make 4 in
  let x5 = Var.R.make 5 in
  let rename_f var =
    Some (Var.R.make ((Var.R.id var) + 1))
  in
  let vec1 = Zvec.of_int_coeffs [| (x0, 0); (x2, 1); (x4, 2) |]  in
  let expected1 = Zvec.of_int_coeffs [| (x3, 1); (x5, 2) |] in
  assert_equal ~cmp:Zvec.equal ~printer:Zvec.to_string
    expected1
    (Zvec.rename rename_f vec1)

let test_lincomb _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let v1 = Zvec.of_int_coeffs [| (x2, -1); (x0, 1); (x1, 2) |] in
  let v2 = Zvec.of_int_coeffs [| (x0, 1) |] in
  let expected12 = Zvec.of_int_coeffs [| (x2, 1); (x1, -2) |] in
  assert_equal ~cmp:Zvec.equal ~printer:Zvec.to_string
    expected12
    (Zvec.lincomb v1 (Z.of_int (-1)) v2 (Z.one))

let tests = "Zvec" >:::
  [ "test_add_vec" >:: test_add_vec;
    "test_format_vec" >:: test_format_vec;
    "test_rename" >:: test_rename;
    "test_lincomb" >:: test_lincomb ]
