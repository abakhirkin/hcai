open Format
open OUnit2

module Zvec = Zvec.Make(Var.R)

let test_format_eq _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let x4 = Var.R.make 4 in
  let v0 = Zvec.of_coeffs [||] in
  let eq0 = Eq.of_expr_coeff v0 Z.zero in
  let s0 = asprintf "%a" (Eq.format ?format_var:None) eq0 in
  assert_equal ~printer:(fun s->s) "0 = 0" s0;
  let v1 = Zvec.of_coeffs [| (x4, Z.of_int 4); (x0, Z.of_int 0); (x2, Z.of_int 2) |] in
  let eq1 = Eq.of_expr_coeff v1 Z.one in
  let s1 = asprintf "%a"  (Eq.format ?format_var:None) eq1 in
  assert_equal ~printer:(fun s->s) "2*x2 + 4*x4 = 1" s1;
  let v2 = Zvec.of_coeffs [| (x0, Z.of_int (-1)); (x1, Z.of_int (-1)) |] in
  let eq2 = Eq.of_expr_coeff v2 (Z.of_int (-1)) in
  let s2 = asprintf "%a"  (Eq.format ?format_var:None) eq2 in
  (* Note that in an equality, the coefficient with the smallest index is positive. *)
  assert_equal ~printer:(fun s->s) "x0 + x1 = 1" s2

let test_format_ineq _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let x4 = Var.R.make 4 in
  let v0 = Zvec.of_coeffs [||] in
  let eq0 = Ineq.of_expr_coeff v0 Z.zero Ineq.Geq in
  let s0 = asprintf "%a" (Ineq.format ?format_var:None) eq0 in
  assert_equal ~printer:(fun s->s) "0 >= 0" s0;
  let v1 = Zvec.of_coeffs [| (x4, Z.of_int 4); (x0, Z.of_int 0); (x2, Z.of_int 2) |] in
  let eq1 = Ineq.of_expr_coeff v1 Z.one Ineq.Gt in
  let s1 = asprintf "%a" (Ineq.format ?format_var:None) eq1 in
  assert_equal ~printer:(fun s->s) "2*x2 + 4*x4 > 1" s1;
  let v2 = Zvec.of_coeffs [| (x0, Z.of_int (-1)); (x1, Z.of_int (-1)) |] in
  let eq2 = Ineq.of_expr_coeff v2 (Z.of_int (-1)) Ineq.Geq in
  let s2 = asprintf "%a" (Ineq.format ?format_var:None) eq2 in
  assert_equal ~printer:(fun s->s) "-x0 - x1 >= -1" s2;
  let ineq3 = Ineq.of_int_coeffs [| |] (-1) Ineq.Geq in
  assert_equal ~printer:(fun s->s) "0 >= -1" (Ineq.to_string ineq3)

let test_aff_expr_gcd _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let e0 = AffExpr.make (Zvec.make_zero ()) Z.zero in
  let e0_div = AffExpr.div_gcd e0 in
  (* Doing div_gcd before other asserts to make sure it does not change the
     input expression. *)
  assert_equal ~printer:Z.to_string Z.one (AffExpr.gcd e0);
  assert_equal ~cmp:AffExpr.equal e0 e0_div;
  let e1 = AffExpr.make
    (Zvec.of_coeffs [| (x0, Z.of_int (2)); (x1, Z.of_int (-4)) |])
    (Z.of_int (-6))
  in
  let e1_div = AffExpr.div_gcd e1 in
  let e1_div_expected = AffExpr.make
    (Zvec.of_coeffs [| (x0, Z.of_int (1)); (x1, Z.of_int (-2)) |])
    (Z.of_int (-3))
  in
  assert_equal ~cmp:Z.equal ~printer:Z.to_string (Z.of_int 2) (AffExpr.gcd e1);
  assert_equal ~cmp:AffExpr.equal e1_div_expected e1_div;
  (* e1_div_expected should be unaffected by div_gcd, since gcd is 1. *)
  assert_equal ~cmp:(==) e1_div_expected (AffExpr.div_gcd e1_div_expected);
  let e2 = AffExpr.of_int_coeffs [| |] (-2) in
  assert_equal (Z.of_int 2) (AffExpr.gcd e2)

let test_aff_expr_mul _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let e1 = AffExpr.of_int_coeffs [| (x0, 1); (x1, 2) |] 3 in
  let e2 = AffExpr.of_int_const 2 in
  let expected12 = AffExpr.of_int_coeffs [| (x0, 2); (x1, 4) |] 6 in
  assert_equal ~cmp:AffExpr.equal ~printer:AffExpr.to_string
    expected12
    (AffExpr.mul e1 e2);
  assert_equal ~cmp:AffExpr.equal ~printer:AffExpr.to_string
    expected12
    (AffExpr.mul e2 e1)

let test_eq_ineq_gcd _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let x4 = Var.R.make 4 in
  let eq1 = Eq.of_expr_coeff
    (Zvec.of_coeffs [| (x4, Z.of_int 4); (x0, Z.of_int 0); (x2, Z.of_int (-2)) |])
    (Z.of_int (-2))
  in
  let eq1_expected = Eq.of_expr_coeff
    (Zvec.of_coeffs [| (x4, Z.of_int 2); (x2, Z.minus_one) |])
    Z.minus_one
  in
  assert_equal
    ~cmp:Eq.equal
    ~printer:Eq.to_string
    eq1_expected
    eq1;
  let ineq1 = Ineq.of_expr_coeff
    (Zvec.of_coeffs [| (x0, Z.of_int (-3)); (x1, Z.of_int 3) |])
    (Z.of_int 3)
    Ineq.Geq
  in
  let ineq1_expected = Ineq.of_expr_coeff
    (Zvec.of_coeffs [| (x0, Z.minus_one); (x1, Z.one) |])
    Z.one
    Ineq.Geq
  in
  assert_equal
    ~cmp:Ineq.equal
    ~printer:Ineq.to_string
    ineq1_expected
    ineq1;
  let eq2 = Eq.of_int_coeffs [| (x0, 2); (x1, 2) |] 0 in
  let eq2_expected = Eq.of_int_coeffs [| (x0, 1); (x1, 1) |] 0 in
  assert_equal
    ~cmp:Eq.equal
    ~printer:Eq.to_string
    eq2_expected
    eq2

let test_neg_ineq _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let ineq1 = Ineq.of_int_coeffs [| |] (-1) Ineq.Geq in
  assert_equal ~cmp:Ineq.equal ~printer:Ineq.to_string
    (Ineq.of_int_coeffs [| |] 1 Ineq.Gt)
    (Ineq.neg ineq1);
  let ineq2 = Ineq.of_int_coeffs [| (x0, 1); (x1, 1) |] 2 Ineq.Gt in
  assert_equal ~cmp:Ineq.equal ~printer:Ineq.to_string
    (Ineq.of_int_coeffs [| (x0, -1); (x1, -1) |] (-2) Ineq.Geq)
    (Ineq.neg ineq2)

let test_eq_ineq_eliminate _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let x3 = Var.R.make 3 in
  let ineq1 = Ineq.of_expr_coeff
    (Zvec.of_int_coeffs [| (x1, 1); (x2, 2) |])
    Z.zero
    Ineq.Geq
  in
  let ineq2 = Ineq.of_expr_coeff
    (Zvec.of_int_coeffs [| (x1, 1); (x2, -3); (x3, -2) |])
    Z.zero
    Ineq.Gt
  in
  let expected1 = Ineq.of_expr_coeff
    (Zvec.of_int_coeffs [| (x1, 5); (x3, -4) |])
    Z.zero
    Ineq.Gt
  in
  assert_equal ~cmp:Ineq.equal ~printer:Ineq.to_string
    expected1
    (Ineq.eliminate x2 ineq1 ineq2);
  let eq1 = Eq.of_expr_coeff
    (Zvec.of_int_coeffs [| (x1, 2); (x2, 4) |])
    (Z.of_int 3)
  in
  let eq2 = Eq.of_expr_coeff
    (Zvec.of_int_coeffs [| (x1, -4); (x2, -3) |])
    Z.one
  in
  let expected2 = Eq.of_expr_coeff
    (Zvec.of_int_coeffs [| (x2, 5) |])
    (Z.of_int 7)
  in
  assert_equal ~cmp:Eq.equal ~printer:Eq.to_string
    expected2
    (Eq.eliminate x1 eq1 eq2);
  let expected3 = Ineq.of_expr_coeff
    (Zvec.of_int_coeffs [| (x1, 5); (x3, -2) |])
    Z.minus_one
    Ineq.Gt
  in
  assert_equal ~cmp:Ineq.equal ~printer:Ineq.to_string
    expected3
    (Ineq.eq_eliminate x2 eq2 ineq2);
  let eq3 = Eq.of_int_coeffs [| (x2, -1); (x0, 1); (x1, 2) |] 0 in
  let ineq3 = Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq in
  let expected4 = Ineq.of_int_coeffs [| (x1, -2); (x2, 1) |] 0 Ineq.Geq in
  assert_equal ~cmp:Ineq.equal ~printer:Ineq.to_string
    expected4
    (Ineq.eq_eliminate x0 eq3 ineq3);
  let ineq4 = Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq in
  let ineq5 = Ineq.of_int_coeffs [| (x0, -1) |] (-1) Ineq.Geq in
  let expected45 = Ineq.of_int_coeffs [| |] (-1) Ineq.Geq in
  let result45 = Ineq.eliminate x0 ineq4 ineq5 in
  assert_equal ~cmp:Ineq.equal ~printer:Ineq.to_string
    expected45
    result45;
  assert_equal true (Ineq.is_true result45)

let test_rename _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let x3 = Var.R.make 3 in
  let x5 = Var.R.make 5 in
  let rename_f var =
    Some (Var.R.make ((Var.R.id var) + 1))
  in
  let eq1 = Eq.of_expr_coeff
    (Zvec.of_int_coeffs [| (x0, 2); (x2, 4) |])
    (Z.of_int 3)
  in
  let expected1_1 = Eq.of_expr_coeff
    (Zvec.of_int_coeffs [| (x1, 2); (x3, 4) |])
    (Z.of_int 3)
  in
  let expected1_2 = Eq.of_expr_coeff
    (Zvec.of_int_coeffs [| (x1, 2); (x3, 4); (x5, -3) |])
    Z.zero
  in
  assert_equal ~cmp:Eq.equal ~printer:Eq.to_string
    expected1_1
    (Eq.rename rename_f eq1);
  assert_equal ~cmp:Eq.equal ~printer:Eq.to_string
    expected1_2
    (Eq.rename ~free_var:x5 rename_f eq1)  

let test_min_max_var _ =
  let x1 = Var.R.make 1 in
  let x3 = Var.R.make 3 in
  let x5 = Var.R.make 5 in
  let eq1 = Eq.of_expr_coeff
    (Zvec.of_int_coeffs [| (x1, 2); (x3, 4); (x5, -3) |])
    Z.zero
  in
  let ineq2 = Ineq.of_int_coeffs [| |] 0 Ineq.Geq in
  assert_equal (Some x1) (Eq.min_var eq1);
  assert_equal (Some x5) (Eq.max_var eq1);
  assert_equal None (Ineq.min_var ineq2);
  assert_equal None (Ineq.max_var ineq2)

let test_inv_replace _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let expr1 = AffExpr.of_int_coeffs [| (x0, 2); (x1, 1) |] (-1) in
  let ineq1 = Ineq.of_int_coeffs [| (x0, 3); (x2, 1) |] 0 Ineq.Gt in
  let expected11 = Ineq.of_int_coeffs [| (x0, 3); (x1, -3); (x2, 2) |] 3 Ineq.Gt in
  assert_equal ~cmp:Ineq.equal ~printer:Ineq.to_string
    expected11 (Ineq.inv_replace x0 expr1 ineq1);
  let expr2 = AffExpr.of_int_coeffs [| (x0, -2); (x1, 1) |] (-1) in
  let expected21 = Ineq.of_int_coeffs [| (x0, -3); (x1, 3); (x2, 2) |] (-3) Ineq.Gt in
  assert_equal ~cmp:Ineq.equal ~printer:Ineq.to_string
    expected21 (Ineq.inv_replace x0 expr2 ineq1);
  let eq2 = Eq.of_int_coeffs [| (x0, 2) |] 3 in
  let expr3 = AffExpr.of_int_coeffs [| (x0, 1); (x1, -1) |] 0 in
  let expected32 = Eq.of_int_coeffs [| (x0, 2); (x1, 2) |] 3 in
  assert_equal ~cmp:Eq.equal ~printer:Eq.to_string
    expected32 (Eq.inv_replace x0 expr3 eq2);
  let eq3 = Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 1 in
  let expr4 = AffExpr.of_int_coeffs [| (x0, 1); (x1, 1) |] 0 in
  let expected43 = Eq.of_int_coeffs [| (x0, 2); (x1, -1) |] 1 in
  assert_equal ~cmp:Eq.equal ~printer:Eq.to_string
    expected43 (Eq.inv_replace x1 expr4 eq3)

let tests = "expr_eq_ineq" >:::
  [ "format_eq" >:: test_format_eq;
    "format_ineq" >:: test_format_ineq;
    "aff_expr_gcd" >:: test_aff_expr_gcd;
    "aff_expr_mul" >:: test_aff_expr_mul;
    "eq_ineq_gcd" >:: test_eq_ineq_gcd;
    "neg_ineq" >:: test_neg_ineq;
    "eq_ineq_eliminate" >:: test_eq_ineq_eliminate;
    "rename" >:: test_rename;
    "min_max_var" >:: test_min_max_var;
    "inv_replace" >:: test_inv_replace ]
