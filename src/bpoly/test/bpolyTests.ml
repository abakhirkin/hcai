open OUnit2

let tests = "bpoly" >:::
  [ ZvecTests.tests;
    ExprEqIneqTests.tests;
    PolyTests.tests;
    BoolExprTests.tests ]