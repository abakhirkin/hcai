open OUnit2

let test_rename _ =
  let ctx = Ctx.make () in
  let b0 = Var.B.make 0 in
  let b1 = Var.B.make 1 in
  let b2 = Var.B.make 2 in
  let t1 = Var.B.make_temp 0 in
  let t2 = Var.B.make_temp 1 in
  let t3 = Var.B.make_temp 2 in
  let eb0 = BoolExpr.of_var ctx b0 in
  let eb1 = BoolExpr.of_var ctx b1 in
  let eb2 = BoolExpr.of_var ctx b2 in
  let et1 = BoolExpr.of_var ctx t1 in
  let et2 = BoolExpr.of_var ctx t2 in
  let et3 = BoolExpr.of_var ctx t3 in
  let expr1 = BoolExpr.make_and (BoolExpr.make_or eb0 eb1) eb2 in
  let expected11 = BoolExpr.make_and (BoolExpr.make_or et1 et2) et3 in
  let to_temp_opt var =
    if Var.B.is_temp var then
      None
    else
      Some (Var.B.make_temp (Var.B.id var))
  in
  assert_equal ~cmp:BoolExpr.equal ~printer:BoolExpr.to_string
    expected11
    (BoolExpr.rename to_temp_opt expr1);
  let f2 var =
    if Var.B.equal var b0 then
      Some t1
    else if not (Var.B.is_temp var) then
      Some var
    else
      None
  in
  let expected12 = BoolExpr.make_and (BoolExpr.make_or et1 eb1) eb2 in
  assert_equal ~cmp:BoolExpr.equal ~printer:BoolExpr.to_string
    expected12
    (BoolExpr.rename f2 expr1);
  let f3 var =
    if Var.B.equal var b0 then Some b1
    else if Var.B.equal var b1 then Some b2
    else if Var.B.equal var b2 then Some b0
    else None
  in
  let expected13 = BoolExpr.make_and (BoolExpr.make_or eb1 eb2) eb0 in
  assert_equal ~cmp:BoolExpr.equal ~printer:BoolExpr.to_string
    expected13
    (BoolExpr.rename f3 expr1);
  let f4 var =
    if Var.B.equal var b0 then Some b2
    else if Var.B.equal var b1 then Some b0
    else if Var.B.equal var b2 then Some b1
    else None
  in
  let expected14 = BoolExpr.make_and (BoolExpr.make_or eb2 eb0) eb1 in
  assert_equal ~cmp:BoolExpr.equal ~printer:BoolExpr.to_string
    expected14
    (BoolExpr.rename f4 expr1)


let tests = "bool_expr" >:::
  [ "test_rename" >:: test_rename ]
