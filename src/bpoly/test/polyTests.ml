open U
open OUnit2
open Format

module Zvec = Zvec.Rvar

let term_equiv z3 term1 term2 =
  let solver = Z3.Solver.mk_solver z3 None in
  Z3.Solver.add solver [Z3.Boolean.mk_distinct z3 [term1; term2]];
  match Z3.Solver.check solver [] with
  | Z3.Solver.UNSATISFIABLE -> true
  | _ -> false

let test_format_top_bot _ =
  let ctx = Ctx.make () in
  let top = Poly.make_top ctx in
  let top_s = asprintf "%a" Poly.format top in
  assert_equal ~printer:(fun s->s) "[true -> true]" top_s;
  let bot = Poly.make_bot ctx in
  let bot_s = asprintf "%a" Poly.format bot in
  assert_equal ~printer:(fun s->s) "[true -> false]" bot_s

let test_format_poly _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let b0 = Var.B.make 0 in
  let b1 = Var.B.make 1 in
  let ctx = Ctx.make () in
  let poly1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_expr_coeff (Zvec.of_coeffs [|(x0, Z.one)|]) Z.zero) |]
    [| ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_expr_coeff
          (Zvec.of_coeffs [|(x0, Z.one); (x1, Z.one)|])
          Z.zero
          Ineq.Geq) |]
    (BoolExpr.of_var ctx b1)
  in
  let s1 = asprintf "%a" Poly.format poly1 in
  assert_equal
    ~printer:(fun s->s)
    "[true -> x0 = 0; b0 -> x0 + x1 >= 0; b1 -> false]"
    s1

let test_poly_term _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let b0 = Var.B.make 0 in
  let b1 = Var.B.make 1 in
  let ctx = Ctx.make () in
  let z3 = Ctx.z3 ctx in
  let top = Poly.make_top ctx in
  let top_term = (Poly.terms_poly (Poly.ensure_terms top)) in
  assert_equal
    ~cmp:(term_equiv z3)
    ~printer:Z3.Expr.to_string
    (Z3.Boolean.mk_true z3)
    top_term;
  let bot = Poly.make_bot ctx in
  let bot_term = (Poly.terms_poly (Poly.ensure_terms bot)) in
  assert_equal
    ~cmp:(term_equiv z3)
    ~printer:Z3.Expr.to_string
    (Z3.Boolean.mk_false z3)
    bot_term;
  let poly1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_expr_coeff (Zvec.of_coeffs [|(x0, Z.one)|]) Z.zero) |]
    [| ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_expr_coeff
          (Zvec.of_coeffs [|(x0, Z.one); (x1, Z.one)|])
          Z.zero
          Ineq.Geq) |]
    (BoolExpr.of_var ctx b1)
  in
  let term1 = (Poly.terms_poly (Poly.ensure_terms poly1)) in
  let expected1 =
    Z3.Boolean.mk_and z3 [
      Z3.Boolean.mk_eq
        z3
        (Z3.Arithmetic.Real.mk_const_s z3 "x0")
        (Z3.Arithmetic.Real.mk_numeral_i z3 0);
      Z3.Boolean.mk_implies z3
        (Z3.Boolean.mk_const_s z3 "b0")
        (Z3.Arithmetic.mk_ge z3
          (Z3.Arithmetic.mk_add z3 [
            (Z3.Arithmetic.Real.mk_const_s z3 "x0");
            (Z3.Arithmetic.Real.mk_const_s z3 "x1")
          ])
          (Z3.Arithmetic.Real.mk_numeral_i z3 0));
      Z3.Boolean.mk_not z3 (Z3.Boolean.mk_const_s z3 "b1")
    ]
  in
  assert_equal
    ~cmp:(term_equiv z3)
    ~printer:Z3.Expr.to_string
    expected1
    term1

let test_pairwise_remove_redundant _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let ctx = Ctx.make () in
  let poly1 = Poly.of_impl_array ~remove_redundant:false
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1) |] 2) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, -1) |] (-4) Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x2, 1) |] 0 Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, 1) |] 2 Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x2, 1) |] 0 Ineq.Gt) |]
    (BoolExpr.make_false ctx)
  in
  let expected1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1) |] 2) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, -1) |] (-4) Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, 1) |] 2 Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x2, 1) |] 0 Ineq.Gt) |]
    (BoolExpr.make_false ctx)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected1
    poly1

let test_merge_same_lincons _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let b0 = Var.B.make 0 in
  let b1 = Var.B.make 1 in
  let b2 = Var.B.make 2 in
  let b3 = Var.B.make 3 in
  let ctx = Ctx.make () in
  let poly1 = Poly.of_impl_array ~remove_redundant:false
    ctx
    [| ImplEq.make
        (BoolExpr.of_var ctx b0)
        (Eq.of_int_coeffs [| (x0, 1); (x1, 1) |] 1);
       ImplEq.make
        (BoolExpr.of_var ctx b3)
        (Eq.of_int_coeffs [| (x2, 1) |] 0);
       ImplEq.make
        (BoolExpr.of_var ctx b1)
        (Eq.of_int_coeffs [| (x0, 2); (x1, 2) |] 2) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq) |]
    (BoolExpr.of_var ctx b2)
  in
  let expected1 = Poly.of_impl_array ctx
    [| ImplEq.make
        (BoolExpr.make_or (BoolExpr.of_var ctx b0) (BoolExpr.of_var ctx b1))
        (Eq.of_int_coeffs [| (x0, 1); (x1, 1) |] 1);
       ImplEq.make
        (BoolExpr.of_var ctx b3)
        (Eq.of_int_coeffs [| (x2, 1) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq) |]
    (BoolExpr.of_var ctx b2)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected1
    poly1

let test_remove_included_in_false _ = 
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let b0 = Var.B.make 0 in
  let b2 = Var.B.make 2 in
  let ctx = Ctx.make () in
  let builder1 = Builder.make ctx in
  Builder.assume_not_b builder1 (BoolExpr.of_var ctx b2);
  Builder.assume_eq_array builder1 [|
    ImplEq.make
      (BoolExpr.of_var ctx b0)
      (Eq.of_expr_coeff (Zvec.of_int_coeffs [|(x0, 1); (x1, 1)|]) Z.zero);
    ImplEq.make
      (BoolExpr.of_var ctx b2)
      (Eq.of_expr_coeff (Zvec.of_int_coeffs [|(x0, 1); (x1, -1)|]) (Z.of_int 2)) |];  
  let poly1 = Builder.to_poly ~remove_redundant:false builder1 in
  let expected1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.of_var ctx b0)
        (Eq.of_expr_coeff (Zvec.of_int_coeffs [|(x0, 1); (x1, 1)|]) Z.zero) |]
    [|  |]
    (BoolExpr.of_var ctx b2)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected1
    poly1

let test_remove_true_false _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let b0 = Var.B.make 0 in
  let b1 = Var.B.make 1 in
  let b2 = Var.B.make 2 in
  let ctx = Ctx.make () in
  let eb0 = BoolExpr.of_var ctx b0 in
  let eb1 = BoolExpr.of_var ctx b1 in
  let eb2 = BoolExpr.of_var ctx b2 in
  let poly1 = Poly.of_impl_array ~remove_redundant:false
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1) |] 0);
       ImplEq.make
        eb2
        (Eq.of_int_coeffs [| |] 1) |]
    [| ImplIneq.make
        (BoolExpr.make_false ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        eb1
        (Ineq.of_int_coeffs [| |] 1 Ineq.Geq);
       ImplIneq.make
        eb0
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  let expected1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1) |] 0) |]
    [| ImplIneq.make
        eb0
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq) |]
    (BoolExpr.make_or eb1 eb2)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected1
    poly1;
  let poly2 = Poly.of_impl_array ~remove_redundant:false
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1) |] 0) ;
       ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| |] 1) |]
    [| ImplIneq.make
        (BoolExpr.make_false ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        eb1
        (Ineq.of_int_coeffs [| |] 1 Ineq.Geq);
       ImplIneq.make
        eb0
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  let expected2 = Poly.of_impl_array
    ctx
    [| |]
    [| |]
    (BoolExpr.make_true ctx)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected2
    poly2

let test_remove_redundant _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let b0 = Var.B.make 0 in
  let b1 = Var.B.make 1 in
  let ctx = Ctx.make () in
  let poly1 = Poly.of_impl_array
    ctx
    [| |]
    [| ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_expr_coeff (Zvec.of_int_coeffs [|(x0, 1); (x1, -1)|]) Z.zero Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_expr_coeff
          (Zvec.of_int_coeffs [|(x0, -1); (x1, -1)|])
          (Z.of_int (-2))
          Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_expr_coeff (Zvec.of_int_coeffs [|(x1, -1)|]) Z.minus_one Ineq.Geq) |]
    (BoolExpr.of_var ctx b1)
  in
  let expected1 = Poly.of_impl_array
    ctx
    [|  |]
    [| ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_expr_coeff (Zvec.of_int_coeffs [|(x0, 1); (x1, -1)|]) Z.zero Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_expr_coeff
          (Zvec.of_int_coeffs [|(x0, -1); (x1, -1)|])
          (Z.of_int (-2))
          Ineq.Geq) |]
    (BoolExpr.of_var ctx b1)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected1
    poly1

let test_eliminate_r _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let b0 = Var.B.make 0 in
  let b1 = Var.B.make 1 in
  let b2 = Var.B.make 2 in
  let ctx = Ctx.make () in
  let poly1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.of_var ctx b0)
        (Eq.of_expr_coeff (Zvec.of_int_coeffs [|(x0, 1); (x1, 1)|]) Z.zero);
       ImplEq.make
        (BoolExpr.of_var ctx b1)
        (Eq.of_expr_coeff (Zvec.of_int_coeffs [|(x0, 1); (x1, -1)|]) (Z.of_int 2)) |]
    [|  |]
    (BoolExpr.of_var ctx b2)
  in
  let expected1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_and (BoolExpr.of_var ctx b0) (BoolExpr.of_var ctx b1))
        (Eq.of_expr_coeff (Zvec.of_int_coeffs [|(x0, 1)|]) Z.one) |]
    [|  |]
    (BoolExpr.of_var ctx b2)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected1
    (Poly.eliminate_r x1 poly1);
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected1
    (Poly.eliminate_array_r [| x1 |] poly1);
  let poly2 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1); (x1, 1) |] 1);
       ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x2, -1); (x0, 1); (x1, 2) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  let expected2 = Poly.of_impl_array
    ctx
    [| |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x2, 1) |] 1 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x2, -1) |] (-2) Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected2
    (Poly.eliminate_array_r [| x0; x1 |] poly2)

let test_bot_hull _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let ctx = Ctx.make () in
  let poly0 = Poly.make_bot ctx in
  let poly3 = Poly.of_impl_array
    ctx
    [| ImplEq.make (BoolExpr.make_true ctx) (Eq.of_int_coeffs [| (x0, 1) |] 0);
       ImplEq.make (BoolExpr.make_true ctx) (Eq.of_int_coeffs [| (x1, 1) |] 0) |]
    [| |]
    (BoolExpr.make_false ctx)
  in
  assert_equal ~cmp:Poly.equiv ~printer:Poly.to_string
    poly3
    (Poly.convex_hull poly0 poly3);
  assert_equal ~cmp:Poly.equiv ~printer:Poly.to_string
    poly3
    (Poly.convex_hull poly3 poly0)

let test_hull _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let ctx = Ctx.make () in  
  let poly1 = Poly.of_impl_array
    ctx
    [| ImplEq.make (BoolExpr.make_true ctx) (Eq.of_int_coeffs [| (x0, 1) |] 1);
       ImplEq.make (BoolExpr.make_true ctx) (Eq.of_int_coeffs [| (x1, 1) |] 1) |]
    [| |]
    (BoolExpr.make_false ctx)
  in
  let poly2 = Poly.of_impl_array
    ctx
    [| ImplEq.make (BoolExpr.make_true ctx) (Eq.of_int_coeffs [| (x0, 1) |] 2);
       ImplEq.make (BoolExpr.make_true ctx) (Eq.of_int_coeffs [| (x1, 1) |] 2) |]
    [| |]
    (BoolExpr.make_false ctx)
  in
  let expected12 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 1 Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, -1) |] (-2) Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in  
  assert_equal ~cmp:Poly.equiv ~printer:Poly.to_string
    expected12
    (Poly.convex_hull poly1 poly2); 
  let poly3 = Poly.of_impl_array
    ctx
    [| ImplEq.make (BoolExpr.make_true ctx) (Eq.of_int_coeffs [| (x0, 1) |] 0) |]
    [| |]
    (BoolExpr.make_false ctx)
  in
  let poly4 = Poly.of_impl_array
    ctx
    [| |]
    [| ImplIneq.make (BoolExpr.make_true ctx) (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Gt) |]
    (BoolExpr.make_false ctx)
  in
  let expected34 = Poly.of_impl_array
    ctx
    [| |]
    [| ImplIneq.make (BoolExpr.make_true ctx) (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  assert_equal ~cmp:Poly.equiv ~printer:Poly.to_string
    expected34
    (Poly.convex_hull poly3 poly4)

let test_rename_r _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let ctx = Ctx.make () in
  let poly1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 1 Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x2, -1) |] (-2) Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  let swap01 var =
    if Var.R.equal var x0 then
      Some x1
    else if Var.R.equal var x1 then
      Some x0
    else
      Some var
  in
  let expected1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x1, 1); (x0, -1) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, 1) |] 1 Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x2, -1) |] (-2) Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected1
    (Poly.rename_r swap01 poly1)

let test_rename_b _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let b0 = Var.B.make 0 in
  let b1 = Var.B.make 1 in
  let b2 = Var.B.make 2 in
  let t1 = Var.B.make_temp 0 in
  let t2 = Var.B.make_temp 1 in
  let t3 = Var.B.make_temp 2 in
  let ctx = Ctx.make () in
  let eb0 = BoolExpr.of_var ctx b0 in
  let eb1 = BoolExpr.of_var ctx b1 in
  let eb2 = BoolExpr.of_var ctx b2 in
  let et1 = BoolExpr.of_var ctx t1 in
  let et2 = BoolExpr.of_var ctx t2 in
  let et3 = BoolExpr.of_var ctx t3 in
  let to_temp_opt var =
    if Var.B.is_temp var then
      None
    else
      Some (Var.B.make_temp (Var.B.id var))
  in
  let poly1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        eb0
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| ImplIneq.make
        eb1
        (Ineq.of_int_coeffs [| (x0, 1) |] 1 Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x2, -1) |] (-2) Ineq.Geq) |]
    eb2
  in
  let expected1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        et1
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| ImplIneq.make
        et2
        (Ineq.of_int_coeffs [| (x0, 1) |] 1 Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x2, -1) |] (-2) Ineq.Geq) |]
    et3
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected1
    (Poly.rename_b to_temp_opt poly1)

let test_assign_r _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let x2 = Var.R.make 2 in
  let ctx = Ctx.make () in
  let poly1 = Poly.of_impl_array
    ctx
    [| |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, -1) |] (-1) Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, -1); (x1, 1) |] 0 Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  let builder11 = Builder.of_poly poly1 in
  let builder12 = Builder.of_poly poly1 in
  let expr1 = AffExpr.of_int_coeffs [| (x0, 1); (x1, 1); (x2, 1) |] (-1) in
  Builder.invertible_assign_r builder11 x0 expr1;
  Builder.project_assign_r builder12 x0 expr1;
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    (Builder.to_poly builder11)
    (Builder.to_poly builder12);
  let expr2 = AffExpr.of_int_coeffs [| (x1, 1) |] 1 in
  let expected12 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] (-1)) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, -1) |] (-1) Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected12
    (Poly.assign_r x0 expr2 poly1);
  let expr3 = AffExpr.of_int_coeffs [| (x0, -1); (x1, 1) |] 0 in
  let expected13 = Poly.of_impl_array
    ctx
    [| |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, -1); (x1, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, -1) |] (-1) Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected13
    (Poly.assign_r x0 expr3 poly1);
  let builder11 = Builder.of_poly poly1 in
  let builder12 = Builder.of_poly poly1 in
  Builder.invertible_assign_r builder11 x0 expr3;
  Builder.project_assign_r builder12 x0 expr3;
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    (Builder.to_poly builder11)
    (Builder.to_poly builder12)

let test_eliminate_b _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let b0 = Var.B.make 0 in
  let b1 = Var.B.make 1 in
  let ctx = Ctx.make () in
  let eb0 = BoolExpr.of_var ctx b0 in
  let eb1 = BoolExpr.of_var ctx b1 in
  let poly1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_and eb0 eb1)
        (Eq.of_int_coeffs [| (x0, 1) |] 0);
       ImplEq.make
        eb0
        (Eq.of_int_coeffs [| (x1, 1) |] 0);
       ImplEq.make
        (BoolExpr.make_and (BoolExpr.make_not eb0) eb1)
        (Eq.of_int_coeffs [| (x0, 1) |] 1);
       ImplEq.make
        (BoolExpr.make_not eb0)
        (Eq.of_int_coeffs [| (x1, 1) |] 1) |]
    [| |]
    (BoolExpr.make_false ctx)
  in
  let expected1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        eb1
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, -1) |] (-1) Ineq.Geq);
      ImplIneq.make
        eb1
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        eb1
        (Ineq.of_int_coeffs [| (x0, -1) |] (-1) Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  assert_equal ~cmp:Poly.equal ~printer:Poly.to_string
    expected1
    (Poly.eliminate_b b0 poly1)

let test_leq _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let b0 = Var.B.make 0 in
  let b1 = Var.B.make 1 in
  let ctx = Ctx.make () in
  let poly1 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x0, -1) |] (-1) Ineq.Geq) |]
    (BoolExpr.of_var ctx b1)
  in
  let poly2 = Poly.of_impl_array
    ctx
    [| |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x0, -1) |] (-1) Ineq.Geq);
      ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x1, -1) |] (-1) Ineq.Geq) |]
    (BoolExpr.of_var ctx b1)
  in
  assert_bool
    "poly1 <= poly2"
    (Poly.leq poly1 poly2)

let test_widen _ =
  let x0 = Var.R.make 0 in
  let x1 = Var.R.make 1 in
  let b0 = Var.B.make 0 in
  let b1 = Var.B.make 1 in
  let b2 = Var.B.make 2 in
  let ctx = Ctx.make () in
  let poly11 = Poly.of_impl_array
    ctx
    [| |]
    [| ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x0, -1) |] (-1) Ineq.Geq);
      ImplIneq.make
        (BoolExpr.of_var ctx b1)
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b1)
        (Ineq.of_int_coeffs [| (x1, -1) |] (-1) Ineq.Geq) |]
    (BoolExpr.of_var ctx b2)
  in
  let poly12 = Poly.of_impl_array
    ctx
    [| |]
    [| ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x0, -1) |] (-2) Ineq.Geq);
      ImplIneq.make
        (BoolExpr.of_var ctx b1)
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b1)
        (Ineq.of_int_coeffs [| (x1, -1) |] (-1) Ineq.Geq) |]
    (BoolExpr.of_var ctx b2)
  in
  let expected1 = Poly.of_impl_array
    ctx
    [| |]
    [| ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b1)
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b1)
        (Ineq.of_int_coeffs [| (x1, -1) |] (-1) Ineq.Geq) |]
    (BoolExpr.of_var ctx b2)
  in
  let widened1 = Poly.widen poly11 poly12 in
  assert_bool
    "widened1 >= poly12"
    (Poly.leq poly12 widened1);
  assert_equal ~cmp:Poly.equiv ~printer:Poly.to_string
    expected1
    widened1;
  let poly21 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, -1) |] (-1) Ineq.Geq) |]
    (BoolExpr.of_var ctx b2)
  in
  let poly22 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.of_var ctx b0)
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x1, -1) |] (-1) Ineq.Geq) |]
    (BoolExpr.of_var ctx b2)
  in
  let expected2 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.of_var ctx b0)
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x0, 1) |] 0 Ineq.Geq);
       ImplIneq.make
        (BoolExpr.of_var ctx b0)
        (Ineq.of_int_coeffs [| (x0, -1) |] (-1) Ineq.Geq) |]
    (BoolExpr.of_var ctx b2)
  in
  let widened2 = Poly.widen poly21 poly22 in
  assert_bool
    "widened2 >= poly22"
    (Poly.leq poly22 widened2);
  assert_equal ~cmp:Poly.equiv ~printer:Poly.to_string
    expected2
    widened2;
  let poly3 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, -1) |] (-1) Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, 1) |] 0 Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  let poly4 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x0, -1) |] (-2) Ineq.Geq);
       ImplIneq.make
        (BoolExpr.make_true ctx)
        (Ineq.of_int_coeffs [| (x1, 1) |] 1 Ineq.Geq) |]
    (BoolExpr.make_false ctx)
  in
  let poly34 = Poly.convex_hull poly3 poly4 in
  let widened34 = Poly.widen poly3 poly34 in
  let expected_constraint34 = Poly.of_impl_array
    ctx
    [| ImplEq.make
        (BoolExpr.make_true ctx)
        (Eq.of_int_coeffs [| (x0, 1); (x1, -1) |] 0) |]
    [| |]
    (BoolExpr.make_false ctx)  
  in
  assert_bool "widened34 <= expected_constraint34" (Poly.leq widened34 expected_constraint34)

let tests = "poly" >:::
  [ "test_format_top_bot" >:: test_format_top_bot;
    "test_format_poly" >:: test_format_poly;
    "test_poly_term" >:: test_poly_term;
    "test_remove_included_in_false" >:: test_remove_included_in_false;
    "test_remove_true_false" >:: test_remove_true_false;
    "test_pairwise_remove_redundant" >:: test_pairwise_remove_redundant;
    "test_remove_redundant" >:: test_remove_redundant;
    "test_merge_same_lincons" >:: test_merge_same_lincons;
    "test_eliminate_r" >:: test_eliminate_r;
    "test_bot_hull" >:: test_bot_hull;
    "test_hull" >:: test_hull;
    "test_rename_r" >:: test_rename_r;
    "test_rename_b" >:: test_rename_b;
    "test_assign_r" >:: test_assign_r;
    "test_eliminate_b" >:: test_eliminate_b;
    "test_leq" >:: test_leq;
    "test_widen" >:: test_widen ]
