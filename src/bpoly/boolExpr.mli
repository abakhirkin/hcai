(** Boolean expressions. *)

(** Type of a Boolean expression. *)
type t

(** Context of the expression. *)
val ctx : t -> Ctx.t

(** BDD of the expression. *)
val bdd : t -> Cudd.Bdd.dt

(** Whether the expression is constant true. *)
val is_true : t -> bool

(** Whether the expresion is constant false. *)
val is_false : t -> bool

(** Whether the first expression implies the second expression.
 
    [is_included expr1 expr2] returns true if [expr1] implies [expr2]. *)
val is_included : t -> t -> bool

(** Whether the expressions are equivalent. *)
val equal : t -> t -> bool

(** Creates a constant true expression. *)
val make_true : Ctx.t -> t

(** Creates a constant false expression. *)
val make_false : Ctx.t -> t

(** Creates an expression from a Boolean variable. *)
val of_var : Ctx.t -> Var.B.t -> t

(** Creates a conjunction. *)
val make_and : t -> t -> t

(** Creates a disjunction. *)
val make_or : t -> t -> t

(** Creates an equivalence (negaton of xor). *)
val make_eq : t -> t -> t

(** Creates an disequivalence (xor). *)
val make_neq : t -> t -> t

(** Creates a negation. *)
val make_not : t -> t

(** Restricts the expression to the given value of the variable. *)
val constrain: Var.B.t -> bool -> t -> t

(** Gets a cached Z3 term for the expression, or creates and caches a new one. *)
val ensure_term : t -> Z3.Expr.expr

(** Gets a cached Z3 term for the negation of the expression,
    or creates and caches a new one. *)
val ensure_nterm : t -> Z3.Expr.expr

(** Renames variables.
 
    [rename f expr] renames variables in [expr] using the renaming function [f].
    The renaming function [f] should not map two different variables to the same
    variable. *)
val rename: (Var.B.t -> Var.B.t option) -> t -> t

(** Formats an expression. *)
val format : ?format_var:(Format.formatter -> Var.B.t -> unit) -> Format.formatter -> t -> unit

(** Formats an expression to string. *)
val to_string : ?format_var:(Format.formatter -> Var.B.t -> unit) -> t -> string
