(** Equalities *)

open U
open Format

module Zvec = Zvec.Rvar

(** Equality [cx = b].
 
    Represented as an affine expression [cx - b], s.t.
    the GCD of coefficients of [c] and [b] is 1 and the coefficient with the
    smallest index is positive.
    *)
type t = {
  t_aff_expr: AffExpr.t
} [@@unboxed]

(** Affine expression representing the equality. *)
let aff_expr eq =
  eq.t_aff_expr

(** Linear part of the affine expression representing the equality.
    
    @return [cx] for the equality [cx = b]. *)
let lin_expr eq =
  AffExpr.lin_expr eq.t_aff_expr

(** Free coefficient affine expression representing the equality.
    
    @return [b] for the equality [cx = b]. *)
let free_coeff eq =
  AffExpr.free_coeff eq.t_aff_expr

(** Whether the coefficient of a variable is non-zero. *)
let coeff_is_nonzero eq i =
  AffExpr.coeff_is_nonzero (aff_expr eq) i

(** Creates a new equality from an affine expression. *)
let make aff_expr =
  { t_aff_expr = AffExpr.div_eq_gcd aff_expr }

(** Cretes a new equality from a linear expression and free coefficient. *)
let of_expr_coeff lin_expr free_coeff =
  make (AffExpr.make lin_expr free_coeff)

(** Creates a new equality from an array of entries (variable, {!Z.t} coefficient),
    and a {!Z.t} free coefficient. *)
let of_coeffs lin_expr_coeffs free_coeff =
  make (AffExpr.make (Zvec.of_coeffs lin_expr_coeffs) free_coeff)

(** Creates a new equality from an array of entries (variable, {!int} coefficient),
    and an {!int} free coefficient. *)
let of_int_coeffs lin_expr_coeffs free_coeff =
  make (AffExpr.make (Zvec.of_int_coeffs lin_expr_coeffs) (Z.of_int free_coeff))

(** Creates a new equality, asserting that two affine expressions are equal .*)
let of_operands aff_expr1 aff_expr2 =
  make (AffExpr.lincomb aff_expr1 Z.one aff_expr2 Z.minus_one)

(** Whether two equalities are the same.
 
    Since the equalities have canonical form, the function just compares the
    coefficients. *)
let equal eq1 eq2 =
  AffExpr.equal eq1.t_aff_expr eq2.t_aff_expr

(** Whether the equality is a tautology [0 = 0]. *)
let is_true eq =
  (Zvec.is_zero (lin_expr eq)) && (Z.equal (free_coeff eq) Z.zero)

(** Whether the equality is a false: [0 = a] for non-zero a. *)
let is_false eq =
  (Zvec.is_zero (lin_expr eq)) && (not (Z.equal (free_coeff eq) Z.zero))

(** Renames variables and possibly multiplies free coefficient by a variable.
    
    See {!AffExpr.rename}. *)
let rename ?free_var f eq =
  make (AffExpr.rename ?free_var:free_var f (aff_expr eq))

(** Inverse replace.
    
    [inv_replace var expr eq] returns the equality, s.t. replacing [var] with
    [expr] in it results in [eq]. *)
let inv_replace var with_expr eq =
  let aff_expr, _ = AffExpr.inv_replace var with_expr (aff_expr eq) in
  make aff_expr

(** Minimum (in id order) variable with non-zero coefficient. *)
let min_var eq =
  AffExpr.min_var (aff_expr eq)

(** Maximum (in id order) variable with non-zero coefficient. *)
let max_var eq =
  AffExpr.max_var (aff_expr eq)

(** Creates a linear combination of two equalities that eliminates a given
    variable.
    
    In the two equalities, coefficients of the eliminated variable should not be
    zero. *)
let eliminate i eq1 eq2 =
  let aff_expr1 = aff_expr eq1 in
  let aff_expr2 = aff_expr eq2 in
  let icoeff1 = AffExpr.get aff_expr1 i in
  let icoeff2 = AffExpr.get aff_expr2 i in
  let sign1 = Z.sign icoeff1 in
  let sign2 = Z.sign icoeff2 in
  check_arg (sign1 <> 0)
    "Coefficients of the eliminated variable should not be 0.";
  check_arg (sign2 <> 0)
    "Coefficients of the eliminated variable should not be 0.";
  if sign1 <> sign2 then
    make (AffExpr.lincomb aff_expr1 (Z.abs icoeff2) aff_expr2 (Z.abs icoeff1))
  else
    make (AffExpr.lincomb aff_expr1 (Z.neg icoeff2) aff_expr2 icoeff1)

(** Formats an equality. *)
let format ?format_var fmt eq =
  pp_open_box fmt 0;
  AffExpr.format_lin_expr ?format_var:format_var fmt (lin_expr eq);
  pp_print_space fmt ();
  pp_print_string fmt "="; 
  pp_print_space fmt ();
  Z.pp_print fmt (free_coeff eq);
  pp_close_box fmt ()

(** Formats an equality to string. *)
let to_string ?format_var eq =
  asprintf "%a" (format ?format_var:format_var) eq

(** Creates a Z3 term from an equality. *)
let make_term ctx eq =
  let z3 = Ctx.z3 ctx in
  let expr_term = AffExpr.make_lin_expr_term ctx (lin_expr eq) in
  let free_coeff_term = ZU.make_term z3 (free_coeff eq) in
  Z3.Boolean.mk_eq z3 expr_term free_coeff_term  

let iter_vars f eq =
  AffExpr.iter_vars f (aff_expr eq)

let max_abs_coeff eq =
  AffExpr.max_abs_coeff (aff_expr eq)
