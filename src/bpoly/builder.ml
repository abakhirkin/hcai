open U
open Format

module DynArray = Batteries.DynArray
module Option = Batteries.Option
module Zvec = Zvec.Make(Var.R)
module RvarHashset = Hashset.Make(Var.R)
module BvarHashset = Hashset.Make(Var.B)

let remove_redundant_smt_count = Counter.make_int "bpoly.remove_redundant_smt_count"
let remove_redundant_smt_useless_count = Counter.make_int "bpoly.remove_redundant_smt_useless_count"
let remove_redundant_smt_time = Counter.make_time_span "bpoly.remove_redundant_smt_time"


type t = {
  mutable t_ctx: Ctx.t;
  mutable t_eqs: ImplEq.t DynArray.t;
  mutable t_ineqs: ImplIneq.t DynArray.t;
  mutable t_false_cond: BoolExpr.t;
  (* TODO: Consider counting new constraints instead. *)
  (* TODO: Consider caching free variables. *)
  mutable t_need_remove_redundant: bool
}

let format fmt builder =
  PolyBase.format fmt builder.t_ctx builder.t_eqs builder.t_ineqs builder.t_false_cond

let make ctx =
  { t_ctx = ctx;
    t_eqs = DynArray.create ();
    t_ineqs = DynArray.create ();
    t_false_cond = BoolExpr.make_false ctx;
    t_need_remove_redundant = false }

let set_false builder =
  builder.t_eqs <- DynArray.create ();
  builder.t_ineqs <- DynArray.create ();
  builder.t_false_cond <- BoolExpr.make_true builder.t_ctx;
  builder.t_need_remove_redundant <- false

let of_poly poly =
  { t_ctx = poly.PolyBase.t_ctx;
    t_eqs = DynArray.copy poly.PolyBase.t_eqs;
    t_ineqs = DynArray.copy poly.PolyBase.t_ineqs;
    t_false_cond = poly.PolyBase.t_false_cond;
    t_need_remove_redundant = false }

let copy builder = 
  { t_ctx = builder.t_ctx;
    t_eqs = DynArray.copy builder.t_eqs;
    t_ineqs = DynArray.copy builder.t_ineqs;
    t_false_cond = builder.t_false_cond;
    t_need_remove_redundant = builder.t_need_remove_redundant }

let move from_builder to_builder =
  to_builder.t_ctx <- from_builder.t_ctx;
  to_builder.t_eqs <- from_builder.t_eqs;
  to_builder.t_ineqs <- from_builder.t_ineqs;
  to_builder.t_false_cond <- from_builder.t_false_cond;
  to_builder.t_need_remove_redundant <- from_builder.t_need_remove_redundant;
  from_builder.t_eqs <- DynArray.create ();
  from_builder.t_ineqs <- DynArray.create ();
  from_builder.t_false_cond <- BoolExpr.make_true from_builder.t_ctx;
  from_builder.t_need_remove_redundant <- false

let remove_included_in_false builder =
  let rec remove_eqs i =
    if i >= DynArray.length builder.t_eqs then
      ()
    else begin
      let eq_i = DynArray.get builder.t_eqs i in
      if BoolExpr.is_included
          (ImplEq.cond eq_i)
          (builder.t_false_cond) then begin
        DynArrayU.swap_remove builder.t_eqs i;
        remove_eqs i
      end else
        remove_eqs (i + 1)
    end
  in
  let rec remove_ineqs i =
    if i >= DynArray.length builder.t_ineqs then
      ()
    else begin
      let ineq_i = DynArray.get builder.t_ineqs i in
      if BoolExpr.is_included
          (ImplIneq.cond ineq_i)
          (builder.t_false_cond) then begin
        DynArrayU.swap_remove builder.t_ineqs i;
        remove_ineqs i
      end else
        remove_ineqs (i + 1)
    end
  in
  remove_eqs 0;
  remove_ineqs 0

let assume_not_b builder bool_expr =
  check_arg
    (BoolExpr.ctx bool_expr == builder.t_ctx)
    "Expression should be from the builder's context.";
  if BoolExpr.is_false bool_expr then
    ()
  else if BoolExpr.is_true bool_expr then
    set_false builder
  else begin
    builder.t_false_cond <- BoolExpr.make_or builder.t_false_cond bool_expr;
    remove_included_in_false builder;
    if DynArray.length builder.t_eqs > 0 || DynArray.length builder.t_ineqs > 0 then
      builder.t_need_remove_redundant <- true
    else ();
  end

let assume_b builder bool_expr =
  assume_not_b builder (BoolExpr.make_not bool_expr)

let add_eq builder eq =
  if BoolExpr.is_included (ImplEq.cond eq) builder.t_false_cond then
    ()
  else begin
    let rec remove_same_lin_eq i =
      if i >= DynArray.length builder.t_eqs then      
        eq
      else begin
        let eq_i = DynArray.get builder.t_eqs i in
        if Eq.equal (ImplEq.eq eq) (ImplEq.eq eq_i) then begin
          let new_eq = ImplEq.make
              (BoolExpr.make_or (ImplEq.cond eq) (ImplEq.cond eq_i))
              (ImplEq.eq eq)
          in
          DynArrayU.swap_remove builder.t_eqs i;
          new_eq
        end else
          remove_same_lin_eq (i + 1)
      end
    in
    let new_eq = remove_same_lin_eq 0 in
    let rec remove_implied_ineqs i =
      if i >= DynArray.length builder.t_ineqs then
        ()
      else begin
        let ineq_i = DynArray.get builder.t_ineqs i in
        if ImplIneq.eq_implies new_eq ineq_i then begin
          DynArrayU.swap_remove builder.t_ineqs i;
          remove_implied_ineqs i
        end else
          remove_implied_ineqs (i + 1)
      end
    in
    remove_implied_ineqs 0;
    DynArray.add builder.t_eqs new_eq;
    builder.t_need_remove_redundant <- true
  end

let add_ineq builder ineq =
  if  (BoolExpr.is_included (ImplIneq.cond ineq) builder.t_false_cond) ||
      (DynArrayU.exists
        (fun eq -> ImplIneq.eq_implies eq ineq)
        builder.t_eqs) ||
      (DynArrayU.exists
        (fun ineq_i -> ImplIneq.implies ineq_i ineq)
        builder.t_ineqs) then
    ()
  else begin
    let rec remove_same_lin_ineq i =
      if i >= DynArray.length builder.t_ineqs then     
        ineq
      else begin
        let ineq_i = DynArray.get builder.t_ineqs i in
        if Ineq.equal (ImplIneq.ineq ineq) (ImplIneq.ineq ineq_i) then begin
          let new_ineq = ImplIneq.make
              (BoolExpr.make_or (ImplIneq.cond ineq) (ImplIneq.cond ineq_i))
              (ImplIneq.ineq ineq)
          in
          DynArrayU.swap_remove builder.t_ineqs i;
          new_ineq
        end else
          remove_same_lin_ineq (i + 1)
      end
    in
    let new_ineq = remove_same_lin_ineq 0 in
    let rec remove_implied_ineqs i =
      if i >= DynArray.length builder.t_ineqs then
        ()
      else begin
        let ineq_i = DynArray.get builder.t_ineqs i in
        if ImplIneq.implies new_ineq ineq_i then begin
          DynArrayU.swap_remove builder.t_ineqs i;
          remove_implied_ineqs i
        end else
          remove_implied_ineqs (i + 1)
      end
    in
    remove_implied_ineqs 0;
    DynArray.add builder.t_ineqs new_ineq;
    builder.t_need_remove_redundant <- true
  end

(* TODO: remove_redundant should be an enum: not at all, no smt, yes. *)
let assume_eq ?(remove_redundant=true) builder eq =
  check_arg
    (ImplEq.ctx eq == builder.t_ctx)
    "Equality should be from the builder's context.";
  if not remove_redundant then
    DynArray.add builder.t_eqs eq
  else if ImplEq.is_true eq then
    ()
  else if ImplEq.is_false eq then
    set_false builder
  else if Eq.is_false (ImplEq.eq eq) then
    assume_b builder (BoolExpr.make_not (ImplEq.cond eq))
  else begin
    add_eq builder eq
  end

let assume_ineq ?(remove_redundant=true) builder ineq =
  check_arg
    (ImplIneq.ctx ineq == builder.t_ctx)
    "Inequality should be from the builder's context.";
  if not remove_redundant then
    DynArray.add builder.t_ineqs ineq
  else if ImplIneq.is_true ineq then
    ()
  else if ImplIneq.is_false ineq then
    set_false builder
  else if Ineq.is_false (ImplIneq.ineq ineq) then
    assume_b builder (BoolExpr.make_not (ImplIneq.cond ineq))
  else begin
    add_ineq builder ineq
  end

let assume_eq_array builder eqs =
  Array.iter
    (fun eq -> assume_eq builder eq)
    eqs

let assume_eq_dynarray builder eqs =
  DynArray.iter
    (fun eq -> assume_eq builder eq)
    eqs

let assume_ineq_array builder ineqs =
  Array.iter
    (fun ineq -> assume_ineq builder ineq)
    ineqs

let assume_ineq_dynarray builder ineqs =
  DynArray.iter
    (fun ineq -> assume_ineq builder ineq)
    ineqs

(* TODO: Tests for assume_poly *)

let assume_poly builder poly =
  assume_eq_dynarray builder poly.PolyBase.t_eqs;
  assume_ineq_dynarray builder poly.PolyBase.t_ineqs;
  assume_not_b builder poly.PolyBase.t_false_cond

let make_remove_redundant_solver builder =
  let ctx = builder.t_ctx in
  (* NOTE: If we don't set logic here, initialization will take MUCH longer.*)
  let solver = Ctx.make_solver ctx in
  Z3.Solver.add solver [BoolExpr.ensure_nterm builder.t_false_cond];
  DynArray.iter 
    (fun eq ->
      let terms = ImplEq.ensure_terms eq in
      Z3.Solver.add solver
        [ (ImplTerms.assumed_impl terms); (ImplTerms.assumed_nimpl terms) ])
    builder.t_eqs;
  DynArray.iter
    (fun ineq ->
      let terms = ImplIneq.ensure_terms ineq in
      Z3.Solver.add solver
        [ (ImplTerms.assumed_impl terms); (ImplTerms.assumed_nimpl terms) ])
    builder.t_ineqs;
  if Opt.print_smt_queries () then
    eprintf "; Solver:@.%s@." (Z3.Solver.to_string solver)
  else ();
  solver

let remove_redundant_smt builder =  
  DynArrayU.sort
    (fun eq1 eq2 -> Z.compare (ImplEq.max_abs_coeff eq2) (ImplEq.max_abs_coeff eq1))
    builder.t_eqs;
  DynArrayU.sort
    (fun eq1 eq2 -> Z.compare (ImplIneq.max_abs_coeff eq2) (ImplIneq.max_abs_coeff eq1))
    builder.t_ineqs;
  let solver = make_remove_redundant_solver builder in
  let is_unsat assumptions =
    if Opt.print_smt_queries () then
      eprintf "; Assume: [%a]@."
        (FormatU.format_list
          ~print_sep:(fun fmt -> pp_print_string fmt ", ")
          (fun fmt lit -> pp_print_string fmt (Z3.Expr.to_string lit)))
        assumptions
    else ();
    let status = Z3.Solver.check solver assumptions in
    if Opt.print_smt_queries () then
      eprintf "; %s@." (Z3U.status_to_string status)
    else ();
    match status with
    | Z3.Solver.UNSATISFIABLE -> true
    | _ -> false
  in
  let eq_is_redundant i =
    let ineq_assumptions = DynArray.fold_left
      (fun acc ineq -> (ImplTerms.impl_assume (ImplIneq.ensure_terms ineq))::acc)
      []
      builder.t_ineqs
    in
    let assumptions = DynArrayU.fold_lefti
      (fun acc j eq_j ->
        let j_terms = ImplEq.ensure_terms eq_j in
        if j <> i then
          (ImplTerms.impl_assume j_terms)::acc
        else
          (ImplTerms.nimpl_assume j_terms)::acc)
      ineq_assumptions
      builder.t_eqs
    in
    is_unsat assumptions
  in
  let ineq_is_redundant i =
    let eq_assumptions = DynArray.fold_left
      (fun acc eq -> (ImplTerms.impl_assume (ImplEq.ensure_terms eq))::acc)
      []
      builder.t_eqs
    in
    let assumptions = DynArrayU.fold_lefti
      (fun acc j ineq_j ->
        let j_terms = ImplIneq.ensure_terms ineq_j in
        if j <> i then
          (ImplTerms.impl_assume j_terms)::acc
        else
          (ImplTerms.nimpl_assume j_terms)::acc)
      eq_assumptions
      builder.t_ineqs
    in
    is_unsat assumptions
  in
  let rec check_eq_loop i =
    if i >= DynArray.length builder.t_eqs then
      ()
    else
      if eq_is_redundant i then begin
        DynArrayU.swap_remove builder.t_eqs i;
        check_eq_loop i
      end else
        check_eq_loop (i + 1)
  in
  let rec check_ineq_loop i =
    if i >= DynArray.length builder.t_ineqs then
      ()
    else
      if ineq_is_redundant i then begin
        DynArrayU.swap_remove builder.t_ineqs i;
        check_ineq_loop i
      end else
        check_ineq_loop (i + 1)
  in
  let is_empty =
    let assumptions = DynArray.fold_left
      (fun acc eq -> (ImplTerms.impl_assume (ImplEq.ensure_terms eq))::acc)
      []
      builder.t_eqs
    in
    let assumptions = DynArray.fold_left
      (fun acc ineq -> (ImplTerms.impl_assume (ImplIneq.ensure_terms ineq))::acc)
      assumptions
      builder.t_ineqs
    in
    is_unsat assumptions
  in
  if is_empty then
    set_false builder
  else begin
    check_eq_loop 0;
    check_ineq_loop 0
  end

let remove_redundant builder =
  if builder.t_need_remove_redundant then begin
    let eqs_len = DynArray.length builder.t_eqs in
    let ineqs_len = DynArray.length builder.t_ineqs in
    Counter.inc_int remove_redundant_smt_count;
    Counter.add_duration remove_redundant_smt_time (fun () ->    
      remove_redundant_smt builder);
    if DynArray.length builder.t_eqs = eqs_len && DynArray.length builder.t_ineqs = ineqs_len then
      Counter.inc_int remove_redundant_smt_useless_count
    else ();
    builder.t_need_remove_redundant <- false
  end else ()

let remove_redundant_function = remove_redundant

let to_poly ?(remove_redundant=true) builder =
  if remove_redundant then
    remove_redundant_function builder
  else ();
  let poly =
    { PolyBase.t_ctx = builder.t_ctx;
      PolyBase.t_eqs = builder.t_eqs;
      PolyBase.t_ineqs = builder.t_ineqs;
      PolyBase.t_false_cond = builder.t_false_cond;
      PolyBase.t_terms = None }
  in
  builder.t_eqs <- DynArray.create ();
  builder.t_ineqs <- DynArray.create ();
  builder.t_false_cond <- BoolExpr.make_true builder.t_ctx;
  poly

let eliminate_eq_ineq_count var_i builder =
  let eqs = builder.t_eqs in
  let ineqs = builder.t_ineqs in
  let eqs_len = DynArray.length eqs in
  let ineqs_len = DynArray.length ineqs in
  let rec count_ineqs_loop pos_count neg_count j =
    if j >= ineqs_len then
      (pos_count, neg_count)
    else begin
      let coeff = Ineq.get (ImplIneq.ineq (DynArray.get ineqs j)) var_i in
      let cmp = Z.compare coeff Z.zero in
      if cmp = 0 then
        count_ineqs_loop pos_count neg_count (j + 1)
      else if cmp > 0 then
        count_ineqs_loop (pos_count + 1) neg_count (j + 1)
      else (* if cmp < 0 *)
        count_ineqs_loop pos_count (neg_count + 1) (j + 1)
    end
  in
  let has_true_eq =
    DynArrayU.exists
      (fun eq -> BoolExpr.is_true (ImplEq.cond eq) && Eq.coeff_is_nonzero (ImplEq.eq eq) var_i)
      eqs
  in
  if has_true_eq then
    (eqs_len - 1, ineqs_len)
  else
    let eqs_count =
      DynArrayU.count (fun eq -> Eq.coeff_is_nonzero (ImplEq.eq eq) var_i) eqs
    in    
    let (pos_count, neg_count) = count_ineqs_loop 0 0 0 in  
    let new_eqs_len = eqs_len - eqs_count + eqs_count * (eqs_count - 1) in
    let new_ineqs_len =
      ineqs_len - pos_count - neg_count + (pos_count * neg_count) + eqs_count * (pos_count + neg_count)
    in
    (new_eqs_len, new_ineqs_len)

let fourier_eliminate_r builder i =  
  let result = make builder.t_ctx in
  assume_not_b result builder.t_false_cond;
  let i_eqs = DynArray.make (DynArray.length builder.t_eqs) in
  let pos_ineqs = DynArray.make (DynArray.length builder.t_eqs) in
  let neg_ineqs = DynArray.make (DynArray.length builder.t_ineqs) in
  DynArray.iter
    (fun eq ->
      if Eq.coeff_is_nonzero (ImplEq.eq eq) i then
        DynArray.add i_eqs eq
      else
        (* Disabling on-add checks for pre-existing eqs. *)
        assume_eq ~remove_redundant:false result eq)
    builder.t_eqs;
  DynArray.iter
    (fun ineq ->
      let icoeff = Ineq.get (ImplIneq.ineq ineq) i in
      let cmp = Z.compare icoeff Z.zero in
      if cmp = 0 then
        (* Disabling on-add checks for pre-existing ineqs. *)
        assume_ineq ~remove_redundant:false result ineq
      else if cmp > 0 then
        DynArray.add pos_ineqs ineq
      else (* if cmp < 0 *)
        DynArray.add neg_ineqs ineq)
    builder.t_ineqs;
  DynArrayU.iter_triangle
    (fun eq1 eq2 -> assume_eq result (ImplEq.eliminate_r i eq1 eq2))
    (i_eqs);
  DynArrayU.iter_cartesian
    (fun eq1 ineq2 -> assume_ineq result (ImplIneq.eq_eliminate_r i eq1 ineq2))
    (i_eqs)
    (pos_ineqs);
  DynArrayU.iter_cartesian
    (fun eq1 ineq2 -> assume_ineq result (ImplIneq.eq_eliminate_r i eq1 ineq2))
    (i_eqs)
    (neg_ineqs);
  DynArrayU.iter_cartesian
    (fun ineq1 ineq2 -> assume_ineq result (ImplIneq.eliminate_r i ineq1 ineq2))
    (pos_ineqs)
    (neg_ineqs);
  remove_redundant result;
  move result builder

let gauss_eliminate_r builder with_eq_i var =  
  let eqs = builder.t_eqs in
  let ineqs = builder.t_ineqs in
  let eqs_len = DynArray.length eqs in
  let ineqs_len = DynArray.length ineqs in
  (* Assumed that the condition is true. *)
  let with_eq = DynArray.get eqs with_eq_i in
  check_arg
    (BoolExpr.is_true (ImplEq.cond with_eq))
    "Condition of the definition equality should be true";
  let with_lin_eq = ImplEq.eq with_eq in
  let result = make builder.t_ctx in
  (* TODO: need a test that will fail if this line is forgotten. *)
  assume_not_b result builder.t_false_cond;
  let rec copy_zero_eq i =
    if i >= eqs_len then
      ()
    else begin
      let eq = DynArray.get eqs i in
      let lin_eq = ImplEq.eq eq in
        if not (Eq.coeff_is_nonzero lin_eq var) then
          (* Just copying pre-existing equalities (where var has coefficient 0)
             does not need redundancy checks. *)
          assume_eq ~remove_redundant:false result eq
        else ();
        copy_zero_eq (i + 1)
    end    
  in
  let rec copy_zero_ineq i =
    if i >= ineqs_len then
      ()
    else begin
      let ineq = DynArray.get ineqs i in
      let lin_ineq = ImplIneq.ineq ineq in
        if not (Ineq.coeff_is_nonzero lin_ineq var) then
          (* Just copying pre-existing inequalities (where var has coefficient 0)
             does not need redundancy checks. *)
          assume_ineq ~remove_redundant:false result ineq
        else ();
        copy_zero_ineq (i + 1)
    end    
  in
  let rec copy_eliminate_eq i =
    if i >= eqs_len then
      ()
    else begin
      if i <> with_eq_i then
        let eq = DynArray.get eqs i in
        let lin_eq = ImplEq.eq eq in
        if Eq.coeff_is_nonzero lin_eq var then
          assume_eq
            result
            (ImplEq.make (ImplEq.cond eq) (Eq.eliminate var with_lin_eq lin_eq))
        else ()          
      else ();
      copy_eliminate_eq (i + 1)
    end
  in
  let rec copy_eliminate_ineq i =
    if i >= ineqs_len then
      ()
    else begin
      let ineq = DynArray.get ineqs i in
      let lin_ineq = ImplIneq.ineq ineq in
      if Ineq.coeff_is_nonzero lin_ineq var then
        assume_ineq
          result
          (ImplIneq.make (ImplIneq.cond ineq) (Ineq.eq_eliminate var with_lin_eq lin_ineq))
      else ();      
      copy_eliminate_ineq (i + 1)
    end
  in
  copy_zero_eq 0;
  copy_zero_ineq 0;
  copy_eliminate_eq 0;
  copy_eliminate_ineq 0;
  move result builder

let eliminate_r builder var =
  remove_redundant builder;
  let eqs = builder.t_eqs in
  let eqs_len = DynArray.length eqs in
  let rec find_true_eq i =
    if i >= eqs_len then
      fourier_eliminate_r builder var
    else
      (* TODO: Perhaps, instead find a set of eqs where conditions sum up to true. *)
      let eq = DynArray.get eqs i in
      if BoolExpr.is_true (ImplEq.cond eq) && Eq.coeff_is_nonzero (ImplEq.eq eq) var then
        gauss_eliminate_r builder i var
      else
        find_true_eq (i + 1)
  in
  find_true_eq 0

let eliminate_mutable_dynarray_r builder vars =
  let rec eliminate_loop () =
    if DynArray.empty vars then
      ()
    else begin
      let len = DynArray.length vars in
      let rec find_min_var_loop min_i min_var min_count i =
        if i >= len then
          min_i, min_var
        else begin
          let var = DynArray.get vars i in
          let eq_count, ineq_count = eliminate_eq_ineq_count var builder in
          let count = eq_count + ineq_count in
          if count < min_count then
            find_min_var_loop i var count (i + 1)
          else
            find_min_var_loop min_i min_var min_count (i + 1)
        end
      in
      let min_i, min_var = find_min_var_loop (-1) Var.R.max_var max_int 0 in
      DynArrayU.swap_remove vars min_i;
      eliminate_r builder min_var;
      eliminate_loop ()
    end
  in
  (* Calling remove_redundant here, so that the estimete of the number of
     resulting constraints is a bit more accurate. *)
  remove_redundant builder;
  eliminate_loop ()

let eliminate_iter_r builder iter vars =
  eliminate_mutable_dynarray_r builder (DynArrayU.of_iter iter vars)

let eliminate_dynarray_r builder vars =
  eliminate_mutable_dynarray_r builder (DynArray.copy vars)

let eliminate_array_r builder vars =
  eliminate_mutable_dynarray_r builder (DynArray.of_array vars)

let iter_vars_r_eqs_ineqs f eqs ineqs =
  DynArray.iter (fun eq -> ImplEq.iter_vars_r f eq) eqs;
  DynArray.iter (fun ineq -> ImplIneq.iter_vars_r f ineq) ineqs

let iter_vars_r f builder =
  iter_vars_r_eqs_ineqs f builder.t_eqs builder.t_ineqs

let used_vars_hashset_r builder =
  let result = RvarHashset.create 16 in
  iter_vars_r (fun var -> RvarHashset.add result var) builder;
  result

let king_convex_hull builder1 eqs2 ineqs2 false_cond2 =
  (* NOTE: The calling functions should have removed redundancies from both input polyhedra. *)
  let used_vars12 = RvarHashset.create 16 in
  iter_vars_r (fun var -> RvarHashset.add used_vars12 var) builder1;
  iter_vars_r_eqs_ineqs (fun var -> RvarHashset.add used_vars12 var) eqs2 ineqs2;
  let ctx = builder1.t_ctx in
  let eqs1 = builder1.t_eqs in
  let ineqs1 = builder1.t_ineqs in
  let false_cond1 = builder1.t_false_cond in
  let result = make ctx in
  assume_not_b result (BoolExpr.make_and false_cond1 false_cond2);
  let rec add_eqs_loop free_var to_temp eqs i =
    if i >= DynArray.length eqs then
      ()
    else begin
      let eq = DynArray.get eqs i in
      assume_eq
        result
        (ImplEq.rename_r ~free_var:free_var to_temp eq);
      add_eqs_loop free_var to_temp eqs (i + 1)
    end
  in
  let rec add_ineqs_loop free_var to_temp ineqs i =
    if i >= DynArray.length ineqs then
      ()
    else begin
      let ineq = DynArray.get ineqs i in
      assume_ineq
        result
        (** TODO: Perhaps there is a way to not take closeure here. *)
        (ImplIneq.close
          (ImplIneq.rename_r ~free_var:free_var to_temp ineq));
      add_ineqs_loop free_var to_temp ineqs (i + 1)
    end
  in
  let scale1 = Var.R.make_temp 0 in
  let scale2 = Var.R.make_temp 1 in
  let to_temp1_opt var =
    if Var.R.is_temp var then
      None
    else
      Some (Var.R.make_temp (2 * (Var.R.id var) + 2))
  in
  let to_temp2_opt var =
    if Var.R.is_temp var then
      None
    else
      Some (Var.R.make_temp (2 * (Var.R.id var) + 3))
  in
  let to_temp1 var = Option.get (to_temp1_opt var) in
  let to_temp2 var = Option.get (to_temp2_opt var) in
  add_eqs_loop scale1 to_temp1_opt eqs1 0;
  add_ineqs_loop scale1 to_temp1_opt ineqs1 0;
  add_eqs_loop scale2 to_temp2_opt eqs2 0;
  add_ineqs_loop scale2 to_temp2_opt ineqs2 0;
  assume_ineq
    result
    (ImplIneq.make
      (BoolExpr.make_true ctx)
      (Ineq.of_int_coeffs [| (scale1, 1) |] 0 Ineq.Geq));
  assume_ineq
    result
    (ImplIneq.make
      (BoolExpr.make_true ctx)
      (Ineq.of_int_coeffs [| (scale2, 1) |] 0 Ineq.Geq));
  assume_eq
    result
    (ImplEq.make
      (BoolExpr.make_true ctx)
      (Eq.of_int_coeffs [| (scale1, 1); (scale2, 1) |] 1));
  assume_eq
    result
    (ImplEq.make
      false_cond1
      (Eq.of_int_coeffs [| (scale1, 1) |] 0));
  assume_eq
    result
    (ImplEq.make
      false_cond2
      (Eq.of_int_coeffs [| (scale2, 1) |] 0));
  RvarHashset.iter
    (fun var ->
      assume_eq
        result
        (ImplEq.make
          false_cond1
          (Eq.of_int_coeffs [| ((to_temp1 var), 1) |] 0));
      assume_eq
        result
        (ImplEq.make
          false_cond2
          (Eq.of_int_coeffs [| ((to_temp2 var), 1) |] 0)))
    used_vars12;
  let eliminate = DynArray.make (2 * (RvarHashset.length used_vars12) + 2) in
  RvarHashset.iter
    (fun var ->
      let temp1 = to_temp1 var in
      let temp2 = to_temp2 var in
      assume_eq
        result
        (ImplEq.make
          (BoolExpr.make_true ctx)
          (Eq.of_int_coeffs [| (var, 1); (temp1, -1); (temp2, -1) |] 0));
      DynArray.add eliminate temp1;
      DynArray.add eliminate temp2)
    used_vars12;
  DynArray.add eliminate scale1;
  DynArray.add eliminate scale2;  
  eliminate_dynarray_r result eliminate;
  move result builder1

let bool_disjoint_convex_hull builder1 eqs2 ineqs2 false_cond2 =
  let ctx = builder1.t_ctx in
  let eqs1 = builder1.t_eqs in
  let ineqs1 = builder1.t_ineqs in
  let false_cond1 = builder1.t_false_cond in
  let nfalse_cond1 = BoolExpr.make_not false_cond1 in
  let nfalse_cond2 = BoolExpr.make_not false_cond2 in
  let result = make ctx in
  assume_not_b result (BoolExpr.make_and false_cond1 false_cond2);
  let copy_eqs eqs nfalse_cond =
    DynArray.iter
      (fun eq ->
        (* TODO: No need to check redundancy via smt, but need to do other checks. *)
        assume_eq
          result
          (ImplEq.make (BoolExpr.make_and (ImplEq.cond eq) nfalse_cond) (ImplEq.eq eq)))
      eqs
  in
  let copy_ineqs ineqs nfalse_cond =
    DynArray.iter
      (fun ineq ->
        (* TODO: No need to check redundancy via smt, but need to do other checks. *)
        assume_ineq
          result
          (ImplIneq.make
            (BoolExpr.make_and (ImplIneq.cond ineq) nfalse_cond)
            (ImplIneq.ineq ineq)))
      ineqs
  in  
  copy_eqs eqs1 nfalse_cond1;
  copy_ineqs ineqs1 nfalse_cond1;
  copy_eqs eqs2 nfalse_cond2;
  copy_ineqs ineqs2 nfalse_cond2;
  move result builder1

let convex_hull_eqs_ineqs_cond builder1 eqs2 ineqs2 false_cond2 =
  (* TODO: Perhaps check for bottom after remove-redundant. *)
  remove_redundant builder1;
  if BoolExpr.is_true (BoolExpr.make_or builder1.t_false_cond false_cond2) then
    bool_disjoint_convex_hull builder1 eqs2 ineqs2 false_cond2
  else
    king_convex_hull builder1 eqs2 ineqs2 false_cond2

let convex_hull builder_to builder2 =
  check_arg
    (builder_to.t_ctx == builder2.t_ctx)
    "Polyhedron builders should be from the same context.";
  remove_redundant builder2;
  convex_hull_eqs_ineqs_cond builder_to builder2.t_eqs builder2.t_ineqs builder2.t_false_cond

let convex_hull_poly builder1 poly2 =
  check_arg
    (builder1.t_ctx == poly2.PolyBase.t_ctx)
    "Polyhedrons should be from the same context.";
  convex_hull_eqs_ineqs_cond
    builder1
    poly2.PolyBase.t_eqs poly2.PolyBase.t_ineqs poly2.PolyBase.t_false_cond

let constrain_b builder var value =
  let result = make builder.t_ctx in
  assume_not_b result (BoolExpr.constrain var value builder.t_false_cond);
  for i = 0 to DynArray.length builder.t_eqs - 1 do
    assume_eq
      result
      (ImplEq.constrain_b var value (DynArray.get builder.t_eqs i))
  done;
  for i = 0 to DynArray.length builder.t_ineqs - 1 do
    assume_ineq
      result
      (ImplIneq.constrain_b var value (DynArray.get builder.t_ineqs i))
  done;
  move result builder

let eliminate_b builder var =
  let builder2 = copy builder in
  constrain_b builder var true;
  constrain_b builder2 var false;
  convex_hull builder builder2

let eliminate_iter_b builder iter vars =
  iter (eliminate_b builder) vars

(** Renames rational variables in the polyhedron.
    
    [rename_r builder f] renames rational variables in [poly] using renaming
    function [f].
    The renaming function [f] should not map different variables to the same
    variable. *)
let rename_r builder f =
  (* NOTE: We assume that renaming does not create redundant inequalities,
     thus we can rename inplace and nov via a new builder. *)
  for i = 0 to DynArray.length builder.t_eqs - 1 do
    DynArray.set builder.t_eqs i
      (ImplEq.rename_r f (DynArray.get builder.t_eqs i))
  done;
  for i = 0 to DynArray.length builder.t_ineqs - 1 do
    DynArray.set builder.t_ineqs i
      (ImplIneq.rename_r f (DynArray.get builder.t_ineqs i))
  done

let rename_b builder f =
  (* NOTE: We assume that renaming does not create redundant inequalities,
     thus we can rename inplace. *)
  for i = 0 to DynArray.length builder.t_eqs - 1 do
    DynArray.set builder.t_eqs i
      (ImplEq.rename_b f (DynArray.get builder.t_eqs i))
  done;
  for i = 0 to DynArray.length builder.t_ineqs - 1 do
    DynArray.set builder.t_ineqs i
      (ImplIneq.rename_b f (DynArray.get builder.t_ineqs i))
  done;
  builder.t_false_cond <- BoolExpr.rename f builder.t_false_cond

let to_temp0_r renamed_var =
  check_arg
    (not (Var.R.is_temp renamed_var))
    "Variable to rename to should not be temporary.";
  fun var ->
    if Var.R.equal var renamed_var then
      Some Var.R.temp0
    else if not (Var.R.is_temp var) then
      Some var
    else
      None

let to_temp0_b renamed_var =
  check_arg
    (not (Var.B.is_temp renamed_var))
    "Variable to rename to should not be temporary.";
  fun var ->
    if Var.B.equal var renamed_var then
      Some Var.B.temp0
    else if not (Var.B.is_temp var) then
      Some var
    else
      None

(* NOTE: Assumes that there are no temporary variables in poly. *)
let project_assign_r builder var aff_expr =
  let rename_f = to_temp0_r var in
  rename_r builder rename_f;
  assume_eq
    builder
    (ImplEq.make
      (BoolExpr.make_true builder.t_ctx)
      (Eq.of_operands
        (AffExpr.of_var var)
        (AffExpr.rename rename_f aff_expr)));
  (* TODO: Perhaps allow to have some existentially quantified variables,
     don't eliminate immediately. *)
  eliminate_r builder Var.R.temp0

let assign_b_b builder var bool_expr =
  check_arg
    (builder.t_ctx == BoolExpr.ctx bool_expr)
    "Polyhedron builder and expression should be from the same context.";
  let rename_f = to_temp0_b var in
  rename_b builder rename_f;
  assume_b
    builder
    (BoolExpr.make_eq
      (BoolExpr.of_var builder.t_ctx var)
      (BoolExpr.rename rename_f bool_expr));
  eliminate_b builder Var.B.temp0 

let assign_b_lin_ineq builder var ineq =
  let ctx = builder.t_ctx in
  let rename_f = to_temp0_b var in
  rename_b builder rename_f;
  assume_ineq
    builder
    (ImplIneq.make (BoolExpr.of_var ctx var) ineq);
  assume_ineq
    builder
    (ImplIneq.make
      (BoolExpr.make_not (BoolExpr.of_var ctx var))
      (Ineq.neg ineq));
  eliminate_b builder Var.B.temp0

let assign_b_ineq builder var ineq =
  check_arg
    (builder.t_ctx == ImplIneq.ctx ineq)
    "Polyhedron and inequality should be from the same context.";
  let rename_f = to_temp0_b var in
  rename_b builder rename_f;
  let renamed_cond = BoolExpr.rename rename_f (ImplIneq.cond ineq) in
  let ctx = builder.t_ctx in
  assume_b
    builder
    (BoolExpr.make_or
      (BoolExpr.of_var ctx var)
      renamed_cond);
  assume_ineq
    builder
    (ImplIneq.make
      (BoolExpr.make_and (BoolExpr.of_var ctx var) renamed_cond)
      (ImplIneq.ineq ineq));
  assume_ineq
    builder
    (ImplIneq.make
      (BoolExpr.make_not (BoolExpr.of_var ctx var))
      (Ineq.neg (ImplIneq.ineq ineq)));
  eliminate_b builder Var.B.temp0

let invertible_assign_r builder var aff_expr =
  (* NOTE: I think, invertible assign does not create redundant inequalities,
     thus we can do it inplace and nov via a new builder. *)
  for i = 0 to DynArray.length builder.t_eqs - 1 do
    DynArray.set builder.t_eqs i
      (ImplEq.inv_replace_r var aff_expr (DynArray.get builder.t_eqs i))
  done;
  for i = 0 to DynArray.length builder.t_ineqs - 1 do
    DynArray.set builder.t_ineqs i
      (ImplIneq.inv_replace_r var aff_expr (DynArray.get builder.t_ineqs i))
  done

let assign_r builder var aff_expr =
  if ZU.is_zero (AffExpr.get aff_expr var) then
    project_assign_r builder var aff_expr
  else
    invertible_assign_r builder var aff_expr

exception Has_extra_var
let widen_eqs_ineqs_cond builder eqs ineqs false_cond =  
  remove_redundant builder;
  let used_vars = used_vars_hashset_r builder in
  let used_vars_array = RvarHashset.to_dynarray used_vars in
  let ctx = builder.t_ctx in
  let result = make ctx in
  let eq_has_extra_var eq =
    try
      ImplEq.iter_vars_r
        (fun var ->
          if not (RvarHashset.mem used_vars var) then
            raise Has_extra_var
          else ())
        eq;
      false
    with
    | Has_extra_var -> true
  in
  let ineq_has_extra_var ineq =
    try
      ImplIneq.iter_vars_r
        (fun var ->
          if not (RvarHashset.mem used_vars var) then
            raise Has_extra_var
          else ())
        ineq;
      false
    with
    | Has_extra_var -> true
  in
  let result_add_false_cond () =
    let copy1 = copy builder in
    eliminate_dynarray_r copy1 used_vars_array;
    assume_not_b result copy1.t_false_cond
  in
  let result_add_eq eq =    
    if not (eq_has_extra_var eq) then begin
      let copy1 = copy builder in
      assume_ineq
        copy1
        (ImplIneq.make
          (BoolExpr.make_true ctx)
          (Ineq.neg (Ineq.geq_of_eq (ImplEq.eq eq))));      
      eliminate_dynarray_r copy1 used_vars_array;      
      let cond1 = copy1.t_false_cond in
      let copy2 = copy builder in
      assume_ineq
        copy2
        (ImplIneq.make
          (BoolExpr.make_true ctx)
          (Ineq.neg (Ineq.leq_of_eq (ImplEq.eq eq))));      
      eliminate_dynarray_r copy2 used_vars_array;      
      let cond2 = copy2.t_false_cond in      
      (* TODO: Maybe keep an inequality if cond1 or cond2 is false. *)
      assume_eq
        result
        (ImplEq.make (BoolExpr.make_and cond1 cond2) (ImplEq.eq eq))      
    end else ()
  in
  let result_add_ineq ineq =
    if not (ineq_has_extra_var ineq) then begin
      let copy1 = copy builder in
      assume_ineq
        copy1
        (ImplIneq.make (BoolExpr.make_true ctx) (Ineq.neg (ImplIneq.ineq ineq)));
      eliminate_dynarray_r copy1 used_vars_array;
      assume_ineq
        result
        (ImplIneq.make copy1.t_false_cond (ImplIneq.ineq ineq))
    end else ()
  in
  result_add_false_cond ();
  DynArray.iter result_add_eq eqs;
  DynArray.iter result_add_ineq ineqs;
  move result builder

let widen_poly builder old_poly =
  widen_eqs_ineqs_cond
    builder
    old_poly.PolyBase.t_eqs old_poly.PolyBase.t_ineqs old_poly.PolyBase.t_false_cond   
