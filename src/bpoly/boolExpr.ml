(* NOTE: There exists boolExpr.mli. *)

open U
open Format

type t = {
  t_ctx: Ctx.t;
  t_bdd: Cudd.Bdd.dt;
  mutable t_term: Z3.Expr.expr option;
  mutable t_nterm: Z3.Expr.expr option
}

let ctx expr =
  expr.t_ctx

let bdd expr =
  expr.t_bdd

let is_true expr =
  Cudd.Bdd.is_true (bdd expr)

let is_false expr =
  Cudd.Bdd.is_false (bdd expr)

let is_included expr1 expr2 =
  check_arg (ctx expr1 == ctx expr2) "Expressions should be from the same context.";
  Cudd.Bdd.is_included_in (bdd expr1) (bdd expr2)

let equal expr1 expr2 =
  check_arg (ctx expr1 == ctx expr2) "Expressions should be from the same context.";
  Cudd.Bdd.is_equal (bdd expr1) (bdd expr2)

let make_true ctx =
  { t_ctx = ctx;
    t_bdd = Cudd.Bdd.dtrue (Ctx.cudd ctx);
    t_term = None;
    t_nterm = None }
  
let make_false ctx =
  { t_ctx = ctx;
    t_bdd = Cudd.Bdd.dfalse (Ctx.cudd ctx);
    t_term = None;
    t_nterm = None }

let of_var ctx var =
  { t_ctx = ctx;
    t_bdd = Cudd.Bdd.ithvar (Ctx.cudd ctx) (Ctx.bdd_i_of_var ctx var);
    t_term = None;
    t_nterm = None}

let make_and expr1 expr2 =
  check_arg (ctx expr1 == ctx expr2) "Expressions should be from the same context.";
  { t_ctx = ctx expr1;
    t_bdd = Cudd.Bdd.dand (bdd expr1) (bdd expr2);
    t_term = None;
    t_nterm = None }

let make_or expr1 expr2 =
  check_arg (ctx expr1 == ctx expr2) "Expressions should be from the same context.";
  { t_ctx = ctx expr1;
    t_bdd = Cudd.Bdd.dor (bdd expr1) (bdd expr2);
    t_term = None;
    t_nterm = None }

let make_eq expr1 expr2 =
  check_arg (ctx expr1 == ctx expr2) "Expressions should be from the same context.";
  { t_ctx = ctx expr1;
    t_bdd = Cudd.Bdd.eq (bdd expr1) (bdd expr2);
    t_term = None;
    t_nterm = None }

let make_neq expr1 expr2 =
  check_arg (ctx expr1 == ctx expr2) "Expressions should be from the same context.";
  { t_ctx = ctx expr1;
    t_bdd = Cudd.Bdd.xor (bdd expr1) (bdd expr2);
    t_term = None;
    t_nterm = None }

let make_not expr =
  { t_ctx = ctx expr;
    t_bdd = Cudd.Bdd.dnot (bdd expr);
    t_term = None;
    t_nterm = None }

let constrain var value expr =
  let ctx = ctx expr in
  let bddVar = Cudd.Bdd.ithvar (Ctx.cudd ctx) (Ctx.bdd_i_of_var ctx var) in
  { t_ctx = ctx;
    t_bdd = Cudd.Bdd.constrain
      (bdd expr)
      (if value then bddVar else Cudd.Bdd.dnot bddVar);
    t_term = None;
    t_nterm = None }

module BddHash = struct
  type t = Cudd.Bdd.dt

  let equal = Pervasives.(=)

  let hash = Hashtbl.hash
end

module BddHashtbl = Hashtbl.Make(BddHash)

(** Creates a new Z3 term from an expression. *)
let make_term expr =
  let ctx = ctx expr in
  let bdd = bdd expr in
  let z3 = Ctx.z3 ctx in
  (** TODO: Maybe cache terms for BDDs within the context. *)
  let bdd_terms = BddHashtbl.create (Cudd.Bdd.size bdd) in
  let make_new_var_term i =
    Ctx.make_bvar_term ctx (Ctx.var_of_bdd_i ctx i)
  in
  let rec make_new_bdd_term bdd =
    (* NOTE: The 'inspect' api hides the complemented pointers. *)
    match Cudd.Bdd.inspect bdd with
    | Cudd.Bdd.Bool const -> Z3.Boolean.mk_val z3 const
    | Cudd.Bdd.Ite (var_i, then_bdd, else_bdd) ->
        let var_term = make_new_var_term var_i in
        if (Cudd.Bdd.is_true then_bdd) && (Cudd.Bdd.is_false else_bdd) then
          var_term
        else if (Cudd.Bdd.is_false then_bdd) && (Cudd.Bdd.is_true else_bdd) then
          Z3.Boolean.mk_not z3 var_term
        else
          Z3.Boolean.mk_ite
            z3
            var_term
            (make_bdd_term then_bdd)
            (make_bdd_term else_bdd)
  and make_bdd_term bdd =
    try
      BddHashtbl.find bdd_terms bdd
    with
    | Not_found ->
        let term = make_new_bdd_term bdd in
        BddHashtbl.replace bdd_terms bdd term;
        term
  in
  make_bdd_term bdd

let ensure_term expr =
  match expr.t_term with
  | Some term -> term
  | None ->
      let term = make_term expr in
      expr.t_term <- Some term;
      term  

let ensure_nterm expr =
  match expr.t_nterm with
  | Some nterm -> nterm
  | None ->
      let term = ensure_term expr in
      let nterm = Z3.Boolean.mk_not (Ctx.z3 (ctx expr)) term in
      expr.t_nterm <- Some nterm;
      nterm  

let rename f expr =
  let ctx = ctx expr in
  let cudd = Ctx.cudd ctx in
  let n_vars = Cudd.Man.get_bddvar_nb cudd in
  let perm = Array.make n_vars (-1) in
  (* NOTE: We are not making an effort to make 'perm' an actual permutation. *)
  (* TODO: Maybe check that there are no two variables mapped to the same one. *)
  for i = 0 to n_vars - 1 do
    perm.(i) <- i
  done;
  for i = 0 to n_vars - 1 do
    let var = Ctx.var_of_bdd_i ctx i in
    match f var with
    | Some new_var ->
        perm.(i) <- (Ctx.bdd_i_of_var ctx new_var)
    | None -> ()
  done;
  { t_ctx = ctx;
    t_bdd = Cudd.Bdd.permute (bdd expr) perm;
    t_term = None;
    t_nterm = None }

let format ?(format_var=Var.B.format) fmt expr =
  let ctx = ctx expr in
  pp_open_box fmt 0;
  Cudd.Bdd.print
    (fun fmt i -> format_var fmt (Ctx.var_of_bdd_i ctx i))
    fmt
    (bdd expr);
  pp_close_box fmt ()

let to_string ?format_var expr =
  asprintf "%a" (format ?format_var:format_var) expr
