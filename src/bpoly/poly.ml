(** Polyhedra. *)

open U
open Format
open FormatU
open PolyBase

module DynArray = Batteries.DynArray
module Zvec = Zvec.Rvar

type t = PolyBase.t

let terms_poly terms =
  terms.terms_poly

let terms_npoly terms =
  terms.terms_npoly

let terms_false_cond terms =
  terms.terms_false_cond

let terms_nfalse_cond terms =
  terms.terms_nfalse_cond

let ctx poly =
  poly.t_ctx

let eqs poly =
  poly.t_eqs

let ineqs poly =
  poly.t_ineqs

let eqs_len poly =
  DynArray.length (eqs poly)

let ineqs_len poly =
  DynArray.length (ineqs poly)

let get_eq poly i =
  DynArray.get (eqs poly) i

let get_ineq poly i =
  DynArray.get (ineqs poly) i

let false_cond poly =
  poly.t_false_cond

let iter_eqs fn poly =
  DynArray.iter fn (eqs poly)

let fold_eqs fn acc poly =
  DynArray.fold_left fn acc poly

let iter_ineqs fn poly =
  DynArray.iter fn (ineqs poly)

let fold_ineqs fn acc poly =
  DynArray.fold_left fn acc poly

let make_top ctx =
  { t_ctx = ctx;
    t_eqs = DynArray.create ();
    t_ineqs = DynArray.create ();
    t_false_cond = BoolExpr.make_false ctx;
    t_terms = None }

let make_bot ctx = 
  { t_ctx = ctx;
    t_eqs = DynArray.create ();
    t_ineqs = DynArray.create ();
    t_false_cond = BoolExpr.make_true ctx;
    t_terms = None }

let of_impl_array ?(remove_redundant=true) ctx eqs ineqs false_cond =
  let builder = Builder.make ctx in
  Builder.assume_not_b builder false_cond;
  Array.iter
    (fun eq -> Builder.assume_eq builder eq)
    eqs;
  Array.iter
    (fun ineq -> Builder.assume_ineq builder ineq)
    ineqs;
  Builder.to_poly ~remove_redundant:remove_redundant builder 

let format fmt poly =
  PolyBase.format fmt poly.t_ctx poly.t_eqs poly.t_ineqs poly.t_false_cond

let to_string poly =
  asprintf "%a" format poly

let equal poly1 poly2 =
  check_arg (ctx poly1 == ctx poly2) "Polyhedra should be of the same context.";
  (DynArrayU.set_equal ImplEq.equal (eqs poly1) (eqs poly2)) &&
  (DynArrayU.set_equal ImplIneq.equal (ineqs poly1) (ineqs poly2)) &&
  (BoolExpr.equal (false_cond poly1) (false_cond poly2))

let make_terms poly =
  let ctx = ctx poly in
  let z3 = Ctx.z3 ctx in
  let false_cond_term = BoolExpr.ensure_term poly.t_false_cond in
  let nfalse_cond_term = Z3.Boolean.mk_not z3 false_cond_term in
  let poly_terms = DynArray.fold_left
    (fun acc eq -> (ImplTerms.impl (ImplEq.ensure_terms eq))::acc)
    []
    poly.t_eqs
  in
  let poly_terms = DynArray.fold_left
    (fun acc ineq -> (ImplTerms.impl (ImplIneq.ensure_terms ineq))::acc)
    poly_terms
    poly.t_ineqs
  in
  let poly_terms =
    if not (BoolExpr.is_false poly.t_false_cond) then
      nfalse_cond_term :: poly_terms
    else
      poly_terms
  in
  let poly_term = match poly_terms with
  | [] -> Z3.Boolean.mk_true z3
  | [term] -> term
  | _ -> Z3.Boolean.mk_and z3 poly_terms
  in
  let npoly_term = Z3.Boolean.mk_not z3 poly_term in
  { terms_poly = poly_term;
    terms_npoly = npoly_term;
    terms_false_cond = false_cond_term;
    terms_nfalse_cond = nfalse_cond_term }

let ensure_terms poly =
  match poly.t_terms with
  | Some terms -> terms
  | None ->
      let terms = make_terms poly in
      poly.t_terms <- Some terms;
      terms

let equiv poly1 poly2 =
  check_arg
    ((ctx poly1) == (ctx poly2))
    "Polyhedra should be from the same context";
  if equal poly1 poly2 then
    true
  else begin
    let terms1 = terms_poly (ensure_terms poly1) in
    let terms2 = terms_poly (ensure_terms poly2) in
    let ctx = ctx poly1 in
    let z3 = Ctx.z3 ctx in
    let solver = Ctx.make_solver ctx in  
    Z3.Solver.add solver [Z3.Boolean.mk_distinct z3 [terms1; terms2]];
    Z3U.check_unsat solver
  end

let leq poly1 poly2 =
  (* TODO: Perhaps a cheaper pairwise inclusion check here first. *)
  check_arg
    ((ctx poly1) == (ctx poly2))
    "Polyhedra should be from the same context";
  let terms1 = terms_poly (ensure_terms poly1) in
  let nterms2 = terms_npoly (ensure_terms poly2) in
  let ctx = ctx poly1 in
  let z3 = Ctx.z3 ctx in
  let solver = Ctx.make_solver ctx in
  Z3.Solver.add solver [Z3.Boolean.mk_and z3 [terms1; nterms2]];
  Z3U.check_unsat solver

let eliminate_r var poly =
  let builder = Builder.of_poly poly in
  Builder.eliminate_r builder var;
  Builder.to_poly builder

let eliminate_array_r vars poly =
  let builder = Builder.of_poly poly in
  Builder.eliminate_array_r builder vars;
  Builder.to_poly builder

let eliminate_b var poly =
  let builder = Builder.of_poly poly in
  Builder.eliminate_b builder var;
  Builder.to_poly builder

let eliminate_iter_b iter vars poly =
  let builder = Builder.of_poly poly in
  Builder.eliminate_iter_b builder iter vars;
  Builder.to_poly builder

let convex_hull poly1 poly2 =
  let builder1 = Builder.of_poly poly1 in
  Builder.convex_hull_poly builder1 poly2;
  Builder.to_poly builder1

let rename_r rename_f poly =
  let builder = Builder.of_poly poly in
  Builder.rename_r builder rename_f;
  Builder.to_poly builder

let rename_b rename_f poly =
  let builder = Builder.of_poly poly in
  Builder.rename_b builder rename_f;
  Builder.to_poly builder

let assign_r var expr poly =
  let builder = Builder.of_poly poly in
  Builder.assign_r builder var expr;
  Builder.to_poly builder

let widen old_poly new_poly =  
  let builder = Builder.of_poly new_poly in
  Builder.widen_poly builder old_poly;
  Builder.to_poly builder

let assume_poly poly1 poly2 =
  let builder = Builder.of_poly poly1 in
  Builder.assume_poly builder poly2;
  Builder.to_poly builder

let is_bot poly =
  (* TODO: This should not require creating an empty polyhedron. *)
  leq poly (make_bot (ctx poly))