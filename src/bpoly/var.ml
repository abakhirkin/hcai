(** A functor to create wrappers for integer indices. *)

open U
open Format

module type VarT = sig
  val var_name: string

  val temp_name: string

  val make_z3_const_s: Z3.context -> string -> Z3.Expr.expr
end

module RatT = struct
  let var_name = "x"

  let temp_name = "tx"

  let make_z3_const_s = Z3.Arithmetic.Real.mk_const_s
end

module BoolT = struct
  let var_name = "b"

  let temp_name = "tb"

  let make_z3_const_s = Z3.Boolean.mk_const_s
end

(** Creates a module implementing a wrapper for an integer index, which we call
    a 'variable'.

    Variables with negative indices are considered 'temporary'.
    There are some additional helper functions to work with them. *)
module Make(T: VarT) = struct

  (** A variable.
      
      A variable is a wrapper of an integer index. *)
  type t = {
    t_id: int
  } [@@unboxed]

  (** Index of the variable.
    
      Nonnegative for normal variables, negative for temporary variables. *)
  let id var =
    var.t_id

  (** Creates a new variable. *)
  let make id = 
    { t_id = id }

  (** Creates a new temporary variable. *)
  let make_temp id = 
    check_arg (id >= 0) "Index should not be negative.";
    { t_id = -id - 1 }

  (** Whether two variables are equal. *)
  let equal var1 var2 =
    var1.t_id = var2.t_id

  (** Hash of the variable index. *)
  let hash var =
    var.t_id

  (** Compares the indices of the variables. *)
  let compare var1 var2 =
    var1.t_id - var2.t_id

  (** Returns the variable with the minumum index. *)
  let min var1 var2 =
    if id var1 <= id var2 then var1 else var2

  (** Returns the variable with the maximum index. *)
  let max var1 var2 =
    if id var1 >= id var2 then var1 else var2

  (** Variable with index [max_int]. *)
  let max_var = { t_id = max_int }

  (** Variable with index [min_int]. *)
  let min_var = { t_id = min_int }

  (** Variable with index [-1] (the first temporary variable). *)
  let temp0 = make_temp 0

  (** Whether the variable is temporary. *)
  let is_temp var =
    (id var) < 0

  (** Formats a variable. *)
  let format fmt var =
    pp_open_box fmt 0;
    let id = var.t_id in
    if id >= 0 then begin
      pp_print_string fmt T.var_name;
      pp_print_int fmt id
    end else begin (* if id < 0 *)
      pp_print_string fmt T.temp_name;
      pp_print_int fmt (-id - 1)
    end;
    pp_close_box fmt ()

  (** Creates a Z3 constant term for the variable. *)
  (* let make_term z3 var =
    let id = var.t_id in
    if id >= 0 then
      T.make_z3_const_s z3 (sprintf "%s%d" T.var_name id)
    else
      T.make_z3_const_s z3 (sprintf "%s%d" T.temp_name (-id - 1)) *)

end

(** Module that implements rational variables. *)
module R = Make(RatT)

(** Module that implements Boolean variables. *)
module B = Make(BoolT)

module Rhashtbl = Hashtbl.Make(R)
module Rhashset = Hashset.Make(R)

module Bhashtbl = Hashtbl.Make(B)
module Bhashset = Hashset.Make(B)