(** Implications with Boolean body and inequality in the head. *)

open U

type t = {
  t_cond: BoolExpr.t;
  t_ineq: Ineq.t;
  t_max_abs_coeff: Z.t;
  mutable t_terms: ImplTerms.t option
}

let ctx iineq =
  BoolExpr.ctx iineq.t_cond

let cond iineq =
  iineq.t_cond

let ineq iineq =
  iineq.t_ineq

let max_abs_coeff iieq =
  iieq.t_max_abs_coeff

let make cond ineq =
  { t_cond = cond;
    t_ineq = ineq;
    t_max_abs_coeff = Ineq.max_abs_coeff ineq;
    t_terms = None }

let close iineq =
  make (cond iineq) (Ineq.close (ineq iineq))

let of_ineq ctx ineq =
  make (BoolExpr.make_true ctx) ineq

let is_true iineq =
  (BoolExpr.is_false (cond iineq)) || (Ineq.is_true (ineq iineq))

let is_false iineq =
  (BoolExpr.is_true (cond iineq)) && (Ineq.is_false (ineq iineq))

let equal iineq1 iineq2 =
  check_arg (ctx iineq1 == ctx iineq2) "Inequalities should be from the same context.";
  (BoolExpr.equal (cond iineq1) (cond iineq2)) &&
  (Ineq.equal (ineq iineq1) (ineq iineq2))

let implies iineq1 iineq2 =
  check_arg (ctx iineq1 == ctx iineq2) "Inequalities should be from the same context.";
  (Ineq.implies (ineq iineq1) (ineq iineq2)) &&
  (BoolExpr.is_included (cond iineq2) (cond iineq1))

let eq_implies ieq1 iineq2 =
  check_arg
    (ImplEq.ctx ieq1 == ctx iineq2)
    "Equality and inequality should be from the same context.";
  (Ineq.eq_implies (ImplEq.eq ieq1) (ineq iineq2)) &&
  (BoolExpr.is_included (cond iineq2) (ImplEq.cond ieq1))

(** Creates a positive combination of two inequalities that eliminates a given
    variable. *)
let eliminate_r i iineq1 iineq2 =
  (* TODO: Maybe also fill ineq_terms if they are present in the operands. *)
  check_arg (ctx iineq1 == ctx iineq2) "Inequalities should be from the same context.";
  make
    (BoolExpr.make_and (cond iineq1) (cond iineq2))
    (Ineq.eliminate i (ineq iineq1) (ineq iineq2))

(** Creates a linear combination of an equality and an inequality that eliminates
    a given variable. *)
let eq_eliminate_r i ieq1 iineq2 =
  (* TODO: Maybe also fill ineq_terms if they are present in the operands. *)
  check_arg
    (ImplEq.ctx ieq1 == ctx iineq2)
    "Equality and inequality should be from the same context.";
  make
    (BoolExpr.make_and (ImplEq.cond ieq1) (cond iineq2))
    (Ineq.eq_eliminate i (ImplEq.eq ieq1) (ineq iineq2))

let rename_r ?free_var f iineq =
  make
    (cond iineq)
    (Ineq.rename ?free_var:free_var f (ineq iineq))

let inv_replace_r var aff_expr iineq =
  make (cond iineq) (Ineq.inv_replace var aff_expr (ineq iineq))

let rename_b f iineq =
  make (BoolExpr.rename f (cond iineq)) (ineq iineq)

let constrain_b var value iineq =
  make (BoolExpr.constrain var value (cond iineq)) (ineq iineq)

let make_terms iineq =
  let ctx = ctx iineq in
  let cond_term = BoolExpr.ensure_term (cond iineq) in
  let lincons_term = Ineq.make_term ctx (ineq iineq) in
  ImplTerms.make ctx cond_term lincons_term

let ensure_terms iineq =
  match iineq.t_terms with
  | Some terms -> terms
  | None ->
      let terms = make_terms iineq in
      iineq.t_terms <- Some terms;
      terms

let iter_vars_r f iineq =
  Ineq.iter_vars f (ineq iineq)
