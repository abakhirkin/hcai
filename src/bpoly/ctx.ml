open U
open Format

type var_terms_t = {
  vt_rvar_term: Var.R.t -> Z3.Expr.expr;
  vt_bvar_term: Var.B.t -> Z3.Expr.expr
}

type t = {
  t_cudd: Cudd.Man.dt;
  t_z3: Z3.context;
  mutable t_var_terms: var_terms_t
}

let cudd ctx =
  ctx.t_cudd

let z3 ctx =
  ctx.t_z3

let bdd_i_of_var ctx var =
  let id = Var.B.id var in
  if id >= 0 then
    id * 2
  else
    ((- id) * 2) - 1

let var_of_bdd_i ctx i =
  if i mod 2 = 0 then
    Var.B.make (i / 2)
  else
    Var.B.make (- ((i + 1) / 2))

let make_solver ctx =
  Z3.Solver.mk_solver_s (z3 ctx) "AUFLIRA"

let default_rvar_term ctx var =
  let z3 = z3 ctx in
  let id = Var.R.id var in
  if id >= 0 then
    Z3.Arithmetic.Real.mk_const_s z3 (sprintf "%s%d" "x" id)
  else
    Z3.Arithmetic.Real.mk_const_s z3 (sprintf "%s%d" "tx" (-id - 1))

let default_bvar_term ctx var =
  let z3 = z3 ctx in
  let id = Var.B.id var in
  if id >= 0 then
    Z3.Boolean.mk_const_s z3 (sprintf "%s%d" "b" id)
  else
    Z3.Boolean.mk_const_s z3 (sprintf "%s%d" "tb" (-id - 1))

let make_var_terms rvar_term bvar_term =
  { vt_rvar_term = rvar_term;
    vt_bvar_term = bvar_term }

let empty_var_terms =
  make_var_terms
    (fun _ -> raise ExceptionU.Not_reachable)
    (fun _ -> raise ExceptionU.Not_reachable)

let set_var_terms ctx var_terms =
  ctx.t_var_terms <- var_terms  

(* TODO: Need to be able to declare some variables as integer. *)
let make ?z3 ?var_terms () =
  let ctx = 
    { t_cudd = Cudd.Man.make_d ();
      t_z3 = begin match z3 with
        | Some z3 -> z3
        | None -> Z3.mk_context [ ("timeout", string_of_int (1000 * (Opt.solver_timeout ()))) ]
        end;
      t_var_terms = empty_var_terms }
  in
  let default_var_terms =
    make_var_terms
      (fun var -> default_rvar_term ctx var)
      (fun var -> default_bvar_term ctx var)
  in
  set_var_terms ctx
    (match var_terms with | Some var_terms -> var_terms | None -> default_var_terms);
  ctx

let make_rvar_term ctx var =
  ctx.t_var_terms.vt_rvar_term var

let make_bvar_term ctx var =
  ctx.t_var_terms.vt_bvar_term var

let format_rvar ctx fmt var =
  pp_print_string fmt (Z3.Expr.to_string (make_rvar_term ctx var))

let format_bvar ctx fmt var =
  pp_print_string fmt (Z3.Expr.to_string (make_bvar_term ctx var))