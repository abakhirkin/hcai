module type T = sig  
  module ClauseSet: ClauseSet.T
  module Domain: Domains.T with module Solver = ClauseSet.Solver
  module Ctx: AbstIntCtx.T with module ClauseSet = ClauseSet with module Domain = Domain

  module PredData : sig
    type t      
    val pred : t -> ClauseSet.Pred.t
    val space : t -> Domain.space_t
    val elem : t -> Domain.elem_t
    val set_elem : t -> Domain.elem_t -> unit
    val below_elem : t -> Domain.elem_t
  end

  type t

  val clause_set : t -> ClauseSet.t
  val solver : t -> ClauseSet.Solver.t
  val domain: t -> Domain.domain_t
  val pred_elem : t -> ClauseSet.Pred.t -> Domain.elem_t
  val set_pred_elem : t -> ClauseSet.Pred.t -> Domain.elem_t -> unit
  val pred_below_elem : t -> ClauseSet.Pred.t -> Domain.elem_t
  val pred_space : t -> ClauseSet.Pred.t -> Domain.space_t
  val iter_pred_data : (PredData.t -> unit) -> t -> unit
end