exception Not_reachable

exception Not_implemented

exception Not_supported of string

let not_supported msg =
  raise (Not_supported msg)

let unreachable () =
  failwith "Unreachable"

type invalid_term_t = {
  it_message: string;
  it_term_string: string
}

let invalid_term_message it =
  it.it_message

let invalid_term_term it =
  it.it_term_string

exception Invalid_term of invalid_term_t

let invalid_term message term_string =
  raise (Invalid_term { it_message = message; it_term_string = term_string })

let () =
  let term_max_length = 80 in
  Printexc.register_printer
    (fun ex ->
      match ex with
      | Not_implemented -> Some "Exception: Not implemented."
      | Not_supported message -> Some ("Exception: Not supported: " ^ message)
      | Invalid_term it ->
          Some ("Exception: Invalid term: " ^
                (invalid_term_message it) ^ "\nTerm: " ^
                (StringU.ellipsis_truncate "..." term_max_length (invalid_term_term it)))
      | _ -> None)

