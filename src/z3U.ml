open Format

let status_to_string status =
  match status with
  | Z3.Solver.SATISFIABLE -> "SAT"
  | Z3.Solver.UNSATISFIABLE -> "UNSAT"
  | Z3.Solver.UNKNOWN -> "UNKNOWN"

let check_unsat solver =
  if Opt.print_smt_queries () then
    eprintf "; Solver:@.%s@." (Z3.Solver.to_string solver)
  else ();
  let status = Z3.Solver.check solver [] in
  if Opt.print_smt_queries () then begin
    eprintf "; %s@." (status_to_string status);
    if status = Z3.Solver.SATISFIABLE then
      match Z3.Solver.get_model solver with
      | Some model ->
          eprintf "; Model:@.%s@." (Z3.Model.to_string model)
      | _ -> ()
    else ()
  end else ();
  match status with
  | Z3.Solver.UNSATISFIABLE -> true
  | _ -> false

