module DynArray = Batteries.DynArray

type 'a t = ('a, unit) Hashtbl.t

let create ?(random=false) size =
    Hashtbl.create size

let add set elem =
  Hashtbl.replace set elem ()

let of_array ?(random=false) arr =
    let set = create ~random:random (Array.length arr) in
    Array.iter (add set) arr;
    set

let of_list ?(random=false) l =
  let set = create ~random:random (List.length l) in
  List.iter (add set) l;
  set

let mem set elem =
  Hashtbl.mem set elem

let iter fn set =
    Hashtbl.iter (fun k _ -> fn k) set

exception Not_for_all
let for_all pred set =
  try
    iter (fun elem -> if not (pred elem) then raise Not_for_all else ()) set;
    true
  with
  | Not_for_all -> false

let equal set1 set2 =
  for_all (mem set2) set1 && for_all (mem set1) set2

module Make(HT: Hashtbl.HashedType) = struct
  module THashtbl = Hashtbl.Make(HT)

  type elt = HT.t

  type t = unit THashtbl.t

  let create ?(random=false) size =
    THashtbl.create size

  let clear set =
    THashtbl.clear set

  let reset set =
    THashtbl.reset set

  let copy set =
    THashtbl.copy set

  let add set elem =
    THashtbl.replace set elem ()

  let mem set elem =
    THashtbl.mem set elem

  let remove set elem =
    THashtbl.remove set elem

  let iter fn set =
    THashtbl.iter (fun k _ -> fn k) set

  let fold fn set acc =
    THashtbl.fold (fun k _ acc -> fn k acc) set acc

  let length set =
    THashtbl.length set

  let of_list ?(random=false) l =
    let set = create ~random:random (List.length l) in
    List.iter (add set) l;
    set

  let to_list set =
    THashtbl.fold (fun key _ keys -> key::keys) set []

  let to_dynarray set =
    let arr = DynArray.make (length set) in
    iter (fun elem -> DynArray.add arr elem) set;
    arr

  (** TODO: More functions from Hashtbl and set. *)

  let add_iter iter_fn col set =
    iter_fn
      (fun elem -> add set elem)
      col

end
