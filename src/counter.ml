open Format
open FormatU
open ExceptionU

module StringMutMap = MutableMap.Make(StringU.OrdHash)

module StringHashtbl = StringU.Hashtbl

type type_t = IntCounterT | TimeSpanCounterT

type int_box_t = {
  int_box_name: string;
  mutable int_box_val: int
}

let make_int_box name =
  { int_box_name = name;
    int_box_val = 0 }

let add_int counter v =
  counter.int_box_val <- counter.int_box_val + v

let clear_int box =
  box.int_box_val <- 0

let inc_int counter =
  add_int counter 1

let int_val box =
  box.int_box_val

type ts_box_t = {
  ts_box_name: string;
  mutable ts_box_time_span: int64
}

let make_time_span_box name =
  { ts_box_name = name;
    ts_box_time_span = Int64.zero }

let clear_time_span box =
  box.ts_box_time_span <- Int64.zero

let add_time_span counter v =
  counter.ts_box_time_span <- Int64.add counter.ts_box_time_span v

let add_duration counter fn =
  let clock = Oclock.realtime in
  let time_before = Oclock.gettime clock in
  let result = fn () in
  let time_after = Oclock.gettime clock in
  add_time_span counter (Int64.sub time_after time_before);
  result

let time_span_val_ms box =
  OclockU.to_ms box.ts_box_time_span

type t =
  | IntCounter of int_box_t
  | TimeSpanCounter of ts_box_t

let typ counter =
  match counter with
  | IntCounter _ -> IntCounterT
  | TimeSpanCounter _ -> TimeSpanCounterT

let counters = StringMutMap.make ()

let format fmt () =  
  format_iter2    
    ~print_sep:newline_sep 
    (fun fmt name counter ->
      pp_print_string fmt name;
      pp_print_string fmt ": ";
      match counter with
      | IntCounter box ->
        pp_print_int fmt (int_val box)
      | TimeSpanCounter box ->
        pp_print_float fmt (time_span_val_ms box);
        pp_print_string fmt " ms")
    StringMutMap.iter
    fmt
    counters  

let make_new typ name =
  let counter =
    match typ with
    | IntCounterT -> IntCounter (make_int_box name)
    | TimeSpanCounterT -> TimeSpanCounter (make_time_span_box name)
  in
  StringMutMap.add counters name counter;
  counter

let make typa name =
  try
    let counter = StringMutMap.find counters name in
    if (typ counter) <> typa then
      invalid_arg (sprintf "Counter \"%s\ already exists with a different type" name)
    else ();
    counter
  with
  | Not_found ->
      make_new typa name

let make_int name =
  match make IntCounterT name with
  | IntCounter box -> box
  | _ ->
      raise Not_reachable

let make_time_span name =
  match make TimeSpanCounterT name with
  | TimeSpanCounter box -> box
  | _ ->
     raise Not_reachable

let clear counter =
  match counter with
  | IntCounter box ->
      clear_int box
  | TimeSpanCounter box ->
      clear_time_span box

let clear_all () =
  StringMutMap.iter
    (fun _ counter -> clear counter)
    counters