open Format

let format format_val fmt o =
  pp_open_box fmt 0;
  begin match o with
  | None -> pp_print_string fmt "None"
  | Some v ->
      pp_print_string fmt "Some";
      pp_print_space fmt ();
      format_val fmt v
  end;
  pp_close_box fmt ()
