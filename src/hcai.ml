let () =
  Main.parse_args ();
  let _ = Main.analyse_file
    ~domain:(Opt.domain ())
    ~int_as_real:(Opt.int_as_real ())
    (Opt.file ()) in
  ()

