(** Interface to the Z3 SMT solver.
    
    Implements {!Solvers.SolverT}.
    
    Uses the official Z3 OCaml API. *)

open Smt2
open ExceptionU
open Format

module Hashtbl = Batteries.Hashtbl

type symbol_t = Z3.Symbol.symbol

type func_decl_t = Z3.FuncDecl.func_decl  

type t = Z3.context

module FuncDeclOrd = struct
  type t = func_decl_t

  let compare fd1 fd2 = (Z3.FuncDecl.get_id fd1) - (Z3.FuncDecl.get_id fd2)
end

let func_decl_name z3 func_decl =
  Z3.Symbol.to_string (Z3.FuncDecl.get_name func_decl)

let format_func_decl_name z3 f func_decl =
  pp_open_box f 0;
  pp_print_string f (Z3.Symbol.to_string (Z3.FuncDecl.get_name func_decl));
  pp_close_box f ()

module FuncDeclOrdHash = struct
  type t = func_decl_t

  let compare fd1 fd2 =
    (Z3.FuncDecl.get_id fd1) - (Z3.FuncDecl.get_id fd2)

  let equal fd1 fd2 =
    Z3.FuncDecl.equal fd1 fd2

  let hash fd =
    Z3.FuncDecl.get_id fd
end

let func_decl_equal fd1 fd2 = FuncDeclOrdHash.equal fd1 fd2

module FuncDeclHashtbl = Hashtbl.Make(FuncDeclOrdHash)

let func_decl_domain z3 func_decl =
  Z3.FuncDecl.get_domain func_decl

let func_decl_range z3 func_decl =
  Z3.FuncDecl.get_range func_decl

let mk ()
  = Z3.mk_context [ ("timeout", string_of_int (1000 * (Opt.solver_timeout ()))) ]

type term_t = Z3.Expr.expr

let format_term z3 f term = 
  pp_open_box f 0;
  pp_print_string f (Z3.Expr.to_string term);
  pp_close_box f ()

let term_to_string z3 term =
  Z3.Expr.to_string term

type sort_t = Z3.Sort.sort

let mk_func_decl z3 name domain range =
  Z3.FuncDecl.mk_func_decl z3 name domain range

let mk_fresh_func_decl z3 domain range =
  Z3.FuncDecl.mk_fresh_func_decl z3 "F" domain range

let mk_true_func_decl z3 =
  Z3.Expr.get_func_decl (Z3.Boolean.mk_true z3)

let mk_bool_sort z3 =
  Z3.Boolean.mk_sort z3

let mk_int_sort z3 =
  Z3.Arithmetic.Integer.mk_sort z3

let mk_real_sort z3 =
  Z3.Arithmetic.Real.mk_sort z3

let mk_symbol_smt2 z3 sym =
  (* Z3.Symbol.mk_string will quote the name when necessary. *)
  Z3.Symbol.mk_string z3 sym.sym_name

let mk_true z3 =
  Z3.Boolean.mk_true z3

let mk_false z3 =
  Z3.Boolean.mk_false z3

let mk_and z3 args =
  match args with
  | [] -> mk_true z3
  | [arg] -> arg
  | _ -> Z3.Boolean.mk_and z3 args
  
let mk_or z3 args =
  match args with
  | [] -> mk_false z3
  | [arg] -> arg
  | _ -> Z3.Boolean.mk_or z3 args

let mk_not z3 arg =
  Z3.Boolean.mk_not z3 arg

let mk_eq z3 terms =
  OperationsU.chainable_of_binop
    (Z3.Boolean.mk_and z3)
    (Z3.Boolean.mk_eq z3)
    terms

let mk_distinct z3 terms =
  Z3.Boolean.mk_distinct z3 terms
  
let mk_impl z3 terms =
  OperationsU.right_assoc_of_binop (Z3.Boolean.mk_implies z3) terms

let mk_xor z3 terms =
  OperationsU.left_assoc_of_binop (Z3.Boolean.mk_xor z3) terms

let mk_ite z3 cond then_term else_term =
  Z3.Boolean.mk_ite z3 cond then_term else_term

let is_true z3 term =
  Z3.Boolean.is_true term

let is_false z3 term =
  Z3.Boolean.is_false term

let is_int_numeral z3 term =
  Z3.Arithmetic.is_int_numeral term

let is_and z3 term =
  Z3.Boolean.is_and term

let is_or z3 term =
  Z3.Boolean.is_or term
  
let is_not z3 term =
  Z3.Boolean.is_not term
 
let is_eq z3 term = 
  Z3.Boolean.is_eq term

let is_impl z3 term =
  Z3.Boolean.is_implies term

let is_distinct z3 term =
  Z3.Boolean.is_distinct term

let is_apply z3 term =
  (Z3.Expr.get_num_args term) > 0

let is_interpreted_const z3 term =
  (Z3.Boolean.is_true term) ||
  (Z3.Boolean.is_false term) ||
  (Z3.Expr.is_numeral term)

let is_uninterpreted_const z3 term =
  (* NOTE: For Z3 numeral is not a constant. *)
  (Z3.Expr.is_const term) &&
  (not (Z3.Boolean.is_true term)) &&
  (not (Z3.Boolean.is_false term))

let term_args z3 term =
  Z3.Expr.get_args term

let term_numeral_s z3 term =
  Z3.Arithmetic.Integer.numeral_to_string term

let term_func_decl z3 term =
  Z3.Expr.get_func_decl term

let is_leq z3 term =
  Z3.Arithmetic.is_le term

let is_lt z3 term =
  Z3.Arithmetic.is_lt term

let is_geq z3 term =
  Z3.Arithmetic.is_ge term

let is_gt z3 term =
  Z3.Arithmetic.is_gt term

let is_plus z3 term =
  Z3.Arithmetic.is_add term

let is_minus z3 term =
  Z3.Arithmetic.is_sub term

let is_unary_minus z3 term =
  Z3.Arithmetic.is_uminus term

let is_mult z3 term =
  Z3.Arithmetic.is_mul term

let goal_of_term z3 term =
  let goal = Z3.Goal.mk_goal z3 false false false in
  Z3.Goal.add goal [term];
  goal

let term_of_goal z3 goal =
  mk_and z3 (Z3.Goal.get_formulas goal)

let term_of_apply_result z3 result =
  mk_and z3 (List.map (term_of_goal z3) (Z3.Tactic.ApplyResult.get_subgoals result))

let substitute z3 term from_list to_list =
  Z3.Expr.substitute term from_list to_list

let term_sort z3 term =
  Z3.Expr.get_sort term

let mk_const z3 func_decl =
  Z3.Expr.mk_const_f z3 func_decl

let mk_fresh_const z3 sort =  
  let prefix =
    if Z3.Sort.equal sort (Z3.Boolean.mk_sort z3) then "b" else "c"
  in
  Z3.Expr.mk_fresh_const z3 prefix sort

let mk_int z3 i =
  Z3.Arithmetic.Integer.mk_numeral_i z3 i

let mk_int_s z3 s =
  Z3.Arithmetic.Integer.mk_numeral_s z3 s

let mk_int_zero z3 =
  Z3.Arithmetic.Integer.mk_numeral_i z3 0

let mk_plus z3 terms =
  match terms with
  | [] -> mk_int_zero z3
  | [term] -> term
  | _ -> Z3.Arithmetic.mk_add z3 terms

let mk_minus z3 terms =
  Z3.Arithmetic.mk_sub z3 terms

let mk_unary_minus z3 term =
  Z3.Arithmetic.mk_unary_minus z3 term

let mk_mult z3 terms =
  Z3.Arithmetic.mk_mul z3 terms

let mk_leq z3 terms =
  OperationsU.chainable_of_binop
    (Z3.Boolean.mk_and z3)
    (Z3.Arithmetic.mk_le z3)
    terms

let mk_lt z3 terms =
  OperationsU.chainable_of_binop
    (Z3.Boolean.mk_and z3)
    (Z3.Arithmetic.mk_lt z3)
    terms

let mk_geq z3 terms =
  OperationsU.chainable_of_binop
    (Z3.Boolean.mk_and z3)
    (Z3.Arithmetic.mk_ge z3)
    terms

let mk_gt z3 terms =
  OperationsU.chainable_of_binop
    (Z3.Boolean.mk_and z3)
    (Z3.Arithmetic.mk_gt z3)
    terms

let mk_apply z3 func_decl terms =
  Z3.Expr.mk_app z3 func_decl terms

let is_numeric_term z3 term =
  let sort = Z3.Expr.get_sort term in
  (Z3.Sort.equal sort (Z3.Arithmetic.Integer.mk_sort z3)) ||
    (Z3.Sort.equal sort (Z3.Arithmetic.Real.mk_sort z3))

let is_bool_term z3 term = 
  Z3.Sort.equal (Z3.Expr.get_sort term) (Z3.Boolean.mk_sort z3)

(** Removes numeric not-equals from nnf. *)
(* TODO: This needs tests. *)
let rec remove_not_eq z3 term =  
  if is_not z3 term then begin
    let not_arg = match term_args z3 term with
    | [arg] -> arg
    | _ -> raise (Invalid_argument "Not-term should have one argument")
    in
    if is_eq z3 not_arg then begin
      let eq_args = term_args z3 not_arg in
      if List.for_all (is_numeric_term z3) eq_args then
        OperationsU.pairwise_of_binop
          (mk_and z3)
          (fun t1 t2 -> mk_or z3 [
            mk_lt z3 [t1; t2];
            mk_gt z3 [t1; t2]])
          eq_args
      else
        invalid_term "Non-numeric \"(not (=\" not supported." (term_to_string z3 term)
    end else if is_distinct z3 not_arg then begin
      let dist_args = term_args z3 not_arg in
      if List.for_all (is_numeric_term z3) dist_args then
        OperationsU.pairwise_of_binop
          (mk_or z3)
          (fun t1 t2 -> mk_eq z3 [t1; t2])
          dist_args
      else if List.for_all (is_bool_term z3) dist_args then
        OperationsU.pairwise_of_binop
          (mk_or z3)
          (fun t1 t2 ->
            mk_and z3 [
              mk_or z3 [mk_not z3 t1; t2];
              mk_or z3 [t1; mk_not z3 t2]
            ])
          dist_args
      else
        invalid_term "Non-numeric \"(not (distinct\" not supported." (term_to_string z3 term)
    end else
      term
  end else if is_distinct z3 term then begin    
    let args = term_args z3 term in
    if List.for_all (is_numeric_term z3) args then
      (* For a numeric distinct, assume that Z3's nnf tactic already removed ite-s. *)
      OperationsU.pairwise_of_binop
        (mk_and z3)
        (fun t1 t2 -> mk_or z3 [
          mk_lt z3 [t1; t2];
          mk_gt z3 [t1; t2]])
        args
    else if List.for_all (is_bool_term z3) args then
      OperationsU.pairwise_of_binop
        (mk_and z3)
        (fun t1 t2 -> remove_not_eq z3 (mk_xor z3 [t1; t2]))
        args
    else
      invalid_term "Only boolean and numeric distinct are supported." (term_to_string z3 term)
  end else if is_apply z3 term then
    let fd = term_func_decl z3 term in
    let args = term_args z3 term in
    mk_apply z3 fd (List.map (remove_not_eq z3) args)
  else
    term
    
let to_nnf z3 term =
  let goal = goal_of_term z3 term in
  let nnf_tactic = Z3.Tactic.mk_tactic z3 "nnf" in
  let result = Z3.Tactic.apply nnf_tactic goal None in
  let nnf_term = term_of_apply_result z3 result in
  (* printf "From Z3:@.%a@." (format_term z3) nnf_term; *)
  let nnf_term = remove_not_eq z3 nnf_term in
  (* printf "After remove_not_eq:@.%a@." (format_term z3) nnf_term; *)
  nnf_term  

type model_t = Z3.Model.model

let eval_bool ?(completion=false) z3 model term =
  match Z3.Model.eval model term completion with
  | None -> None
  | Some value ->
      if Z3.Boolean.is_true value then
        Some true
      else if Z3.Boolean.is_false value then
        Some false
      else if not (is_interpreted_const z3 value) then
        None
      else
        raise (Invalid_argument "Term did not evaluate to a Boolean value")

let get_model z3 term =
  (* TODO: Think how to better pass the logic here.
     AUFLIRA seems to be the smallest logic that includes QF_LIA and QF_LRA. *)
  let solver = Z3.Solver.mk_solver_s z3 "AUFLIRA" in
  Z3.Solver.add solver [term];
  (* Z3 docs claim that Z3.Solver.get_model returns None if solver status is 'unsat'. *)
  (* As of 4.5.1 this is false, and get_model throws instead. *)
  match Z3.Solver.check solver [] with
  | Z3.Solver.SATISFIABLE ->
      begin match Z3.Solver.get_model solver with
      | Some model -> Solvers.SatM model
      | None -> failwith "Term is SAT, but solver did not return a model."
      end
  | Z3.Solver.UNSATISFIABLE -> Solvers.UnsatM
  | Z3.Solver.UNKNOWN -> Solvers.UnknownM

let is_sat z3 term =
  let solver = Z3.Solver.mk_solver z3 None in
  Z3.Solver.add solver [term];
  match Z3.Solver.check solver [] with
  | Z3.Solver.SATISFIABLE -> Solvers.Sat
  | Z3.Solver.UNSATISFIABLE -> Solvers.Unsat
  | Z3.Solver.UNKNOWN -> Solvers.Unknown

let is_real_to_int z3 term =
  Z3.Arithmetic.is_real2int term

let is_int_to_real z3 term =
  Z3.Arithmetic.is_int2real term

let is_bool_func_decl z3 func_decl = 
  let range = Z3.FuncDecl.get_range func_decl in
  let bool_sort = Z3.Boolean.mk_sort z3 in
  Z3.Sort.equal range bool_sort
    
let is_int_func_decl z3 func_decl = 
  let range = Z3.FuncDecl.get_range func_decl in
  let int_sort = Z3.Arithmetic.Integer.mk_sort z3 in
  Z3.Sort.equal range int_sort

let is_real_func_decl z3 func_decl = 
  let range = Z3.FuncDecl.get_range func_decl in
  let real_sort = Z3.Arithmetic.Real.mk_sort z3 in
  Z3.Sort.equal range real_sort

let format_func_decl z3 f func_decl =
  pp_open_box f 0;
  pp_print_string f (Z3.FuncDecl.to_string func_decl);
  pp_close_box f ()

let format_model z3 fmt model =
  pp_open_box fmt 0;
  pp_print_string fmt (Z3.Model.to_string model);
  pp_close_box fmt ()

let mk_const_s z3 sort name =
  Z3.Expr.mk_const_s z3 name sort

let term_equal z3 term1 term2 =
  Z3.Expr.equal term1 term2