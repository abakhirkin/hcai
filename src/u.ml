
let check_arg cond msg =
  if not cond then
    invalid_arg msg
  else ()

let opt_binop op arg1 arg2 =
  match arg1 with
  | None -> arg2
  | Some v1 ->
      begin match arg2 with
      | None -> None
      | Some v2 -> Some (op v1 v2)
      end

let opt_min a1 a2 = opt_binop min a1 a2

let opt_max a1 a2 = opt_binop max a1 a2
