module Hashtbl = Batteries.Hashtbl

let ellipsis_truncate ellipsis length s =
  let ellipsis_length = String.length ellipsis in
  if ellipsis_length > length then
    raise (Invalid_argument "Cannot truncate to less that ellipsis length")
  else if (String.length s <= length) then s
  else (String.sub s 0 (length - ellipsis_length)) ^ ellipsis

module OrdHash = struct
  type t = string

  let compare s1 s2 = String.compare s1 s2

  let equal s1 s2 = (String.compare s1 s2) = 0

  let hash = Hashtbl.hash
end

module StringHashtbl = Hashtbl.Make(OrdHash)
