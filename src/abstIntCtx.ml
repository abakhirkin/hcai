module type T = sig
  module ClauseSet: ClauseSet.T
  module Domain: Domains.T with module Solver = ClauseSet.Solver

  module PredData : sig
    type t      
  end

  type t
end