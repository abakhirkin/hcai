%token LP
%token RP
%token UNDER
%token LET
%token FORALL
%token EXISTS
%token ASSERT
%token SET_INFO
%token <Num.num> NUMERAL
%token <Smt2.symbol_t> SYMBOL
%token <string> STRING
%token <Smt2.symbol_t> KEYWORD
%token EOF
(* Seahorn command tokens *)
%token DECLARE_REL
%token DECLARE_VAR
%token RULE
%token QUERY

%start <Smt2.command_t option> command sea_command
%start <Smt2.term_t> term

%%

command:
  | EOF { None }
  | LP; cmd = command_body; RP { Some cmd };

sea_command:
  | EOF { None }
  | LP; cmd = sea_command_body; RP { Some cmd };

common_command_body:
  | SET_INFO; attr = attribute { Smt2.SetInfoCmd attr };

command_body:
  | cmd = common_command_body { cmd }
  | ASSERT; t = term { Smt2.AssertCmd t };

sea_command_body:
  | cmd = common_command_body { cmd }
  | DECLARE_REL; name = SYMBOL; LP; ss = sort_star; RP
    { Smt2.DeclareRelCmd { Smt2.rel_name = name; Smt2.rel_sorts = ss } }
  | DECLARE_VAR; name = SYMBOL; s = sort
    { Smt2.DeclareVarCmd { Smt2.var_name = name; Smt2.var_sort = s } }
  | RULE; t = term { Smt2.RuleCmd t }
  | QUERY; t = term { Smt2.QueryCmd t };

term:
  | const = spec_constant { Smt2.mk_const_term const }
  | ident = qual_identifier { Smt2.mk_ident_term ident }
  | LP; t = term_body; RP { t };

term_body:
  | ident = qual_identifier; ts = terms { Smt2.mk_apply_term ident ts }
  | LET; LP; bs = var_bindings; RP; t = term
    { Smt2.mk_let_term bs t }
  | FORALL; LP; vs = sorted_vars; RP; t = term
    { Smt2.mk_forall_term vs t }
  | EXISTS; LP; vs = sorted_vars; RP; t = term
    { Smt2.mk_exists_term vs t }

terms: ts = rev_terms { List.rev ts };
rev_terms:
  | t = term { [t] }
  | ts = rev_terms; t = term { t :: ts };

spec_constant:
  | n = NUMERAL { Smt2.NumC n }
  | s = STRING { Smt2.StrC s };

qual_identifier:
  | ident = identifier { ident };

identifier:
  | name = SYMBOL { {Smt2.id_name = name; Smt2.id_indices = []} }
  | LP; UNDER; name = SYMBOL; indices = identifier_indices RP
    { {Smt2.id_name = name; Smt2.id_indices = indices} };

identifier_indices: indices = rev_identifier_indices { List.rev indices };
rev_identifier_indices:
  | index = identifier_index { [index] }
  | indices = rev_identifier_indices; index = identifier_index
    { index :: indices };

identifier_index:
  | n = NUMERAL { Smt2.NumI n }
  | s = SYMBOL { Smt2.SymI s };

attribute:
  | kw = KEYWORD { { Smt2.attr_name = kw; Smt2.attr_value = None } }
  | kw = KEYWORD; v = attribute_value
    { { Smt2.attr_name = kw; Smt2.attr_value = Some v } };

attribute_value:
  | const = spec_constant { Smt2.mk_const_attr_val const }
  | s = SYMBOL { Smt2.SymAV s };

var_bindings: bs = rev_var_bindings { List.rev bs };
rev_var_bindings:
  | b = var_binding { [b] }
  | bs = rev_var_bindings; b = var_binding { b::bs };

var_binding:
  | LP; s = SYMBOL; t = term; RP { {Smt2.bind_name = s; Smt2.bind_term = t } };

sort:
  | id = identifier { { Smt2.sort_name=id; Smt2.sort_args=[] } }
  | LP; id = identifier; ss = sort_plus; RP
    { { Smt2.sort_name=id; Smt2.sort_args=ss } };

sort_star: ss = rev_sort_star { List.rev ss };
rev_sort_star:
  | { [] }
  | ss = rev_sort_star; s = sort { s::ss };

sort_plus:
  | s = sort; ss = sort_star { s::ss };

sorted_vars: vs = rev_sorted_vars { List.rev vs };
rev_sorted_vars:
  | v = sorted_var { [v] }
  | vs = rev_sorted_vars; v = sorted_var { v::vs };

sorted_var:
  | LP; n = SYMBOL; s = sort; RP { {Smt2.var_name = n; Smt2.var_sort = s} };


