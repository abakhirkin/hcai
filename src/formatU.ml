open Format

module DynArray = Batteries.DynArray

let space_sep fmt =
  pp_print_space fmt ()

let newline_sep fmt =
  pp_print_newline fmt ()

let string_sep str fmt =
  pp_print_string fmt str

let format_iter
  ?(print_sep=space_sep) format_elem iter_fn fmt elems =
  pp_open_box fmt 0;
  let i = ref 0 in
  iter_fn
    (fun elem ->
      if !i <> 0 then
        print_sep fmt
      else ();
      format_elem fmt elem;
      i := !i + 1)
    elems;
  pp_close_box fmt ()

let format_iter2
  ?(print_sep=space_sep) format_elem iter_fn fmt elems =
  pp_open_box fmt 0;
  let i = ref 0 in
  iter_fn
    (fun elem1 elem2 ->
      if !i <> 0 then
        print_sep fmt
      else ();
      format_elem fmt elem1 elem2;
      i := !i + 1)
    elems;
  pp_close_box fmt ()

let format_array
  ?(print_sep=space_sep) format_elem fmt elems =
  format_iter ~print_sep:print_sep format_elem Array.iter fmt elems

let format_dynarray
  ?(print_sep=space_sep) format_elem fmt elems =
  format_iter ~print_sep:print_sep format_elem DynArray.iter fmt elems

let format_list
  ?(print_sep=space_sep) format_elem fmt elems =
  format_iter ~print_sep:print_sep format_elem List.iter fmt elems
