open Smt2
open ExceptionU
open Format
open FormatU

module Make(ClauseSet: ClauseSet.T) = struct  
  module Solver = ClauseSet.Solver
  module PredApp = ClauseSet.PredApp
  module ClauseBuilder = ClauseSet.Builder  

  type t = {
    t_solver: Solver.t;
    t_builder: ClauseBuilder.t;
    t_func_decls: Solver.func_decl_t IdentHashtbl.t;
    t_int_as_real: bool
  }

  let make ?(int_as_real=false) solver builder = 
    { t_solver = solver;
      t_builder = builder;
      t_func_decls = IdentHashtbl.create 16;
      t_int_as_real = int_as_real }

  let read_clause rd term =
    let is_pred_app term =
      let func_decl = Solver.term_func_decl rd.t_solver term in
      ClauseBuilder.is_pred rd.t_builder func_decl
    in  
    let mk_pred_app term =
      let func_decl = Solver.term_func_decl rd.t_solver term in
      let pred = ClauseBuilder.func_decl_pred rd.t_builder func_decl in
      if Solver.is_uninterpreted_const rd.t_solver term then
        PredApp.make pred []
      else if Solver.is_apply rd.t_solver term then
        PredApp.make pred (Solver.term_args rd.t_solver term)
      else
        invalid_term "Not supported." (Solver.term_to_string rd.t_solver term)
    in
    let mk_horn_clause body_term head_term =
      let body_list =
        if Solver.is_and rd.t_solver body_term then
          Solver.term_args rd.t_solver body_term
        else
          [body_term]
      in
      let (pred_apps, constrs) = List.partition is_pred_app body_list in
      ClauseBuilder.make_clause
        rd.t_builder
        (Array.of_list (List.map mk_pred_app pred_apps))
        (Solver.mk_and rd.t_solver constrs)
        (mk_pred_app head_term)
    in
    if Solver.is_impl rd.t_solver term then
      match Solver.term_args rd.t_solver term with
      | [body_term; head_term] -> mk_horn_clause body_term head_term
      | _ -> raise (Not_supported "Implication should have two arguments")
    else
      ClauseBuilder.make_clause rd.t_builder [||] (Solver.mk_true rd.t_solver) (mk_pred_app term)

  let mk_sort_smt2 rd sort =
    if sort = bool_sort then
      Solver.mk_bool_sort rd.t_solver
    else if sort = int_sort then begin
      if not rd.t_int_as_real then
        Solver.mk_int_sort rd.t_solver
      else
        Solver.mk_real_sort rd.t_solver
    end else if sort = real_sort then
      Solver.mk_real_sort rd.t_solver
    else
      raise (Not_supported "Sort should be Bool, Int or Real.")

  let mk_func_decl_smt2 rd fn = 
    let func_decl_name = Solver.mk_symbol_smt2 rd.t_solver fn.fun_name in
    let func_decl = Solver.mk_func_decl
      rd.t_solver
      func_decl_name
      (List.map (mk_sort_smt2 rd) fn.fun_sorts_from)
      (mk_sort_smt2 rd fn.fun_sort_to)
    in
    IdentHashtbl.replace rd.t_func_decls (mk_sym_ident fn.fun_name) func_decl;
    func_decl

  let rec mk_term_smt2_env rd let_env term =
    match (term_struct term) with
    | NumT n ->
        if Num.is_integer_num n then
          Solver.mk_int_s rd.t_solver (Num.string_of_num n)
        else
          (* TODO: Support rational numerals *)
          raise Not_implemented
    | StrT s -> raise (Not_supported "String literals")
    | IdentT ident ->
        if ident = true_id then
          Solver.mk_true rd.t_solver
        else if ident = false_id then
          Solver.mk_false rd.t_solver
        else begin
          try
            IdentMap.find ident let_env
          with
          | Not_found ->          
            try
              Solver.mk_const
                rd.t_solver
                (IdentHashtbl.find rd.t_func_decls ident)
            with
            | Not_found ->
                invalid_arg (asprintf "Undeclared identifier: %a" format_ident ident)
        end
    | ApplyT app ->
      let tail_terms = List.map (mk_term_smt2_env rd let_env) app.apply_tail in
      if app.apply_head = not_id then
        match tail_terms with
        | [ tail_term ] -> Solver.mk_not rd.t_solver tail_term
        | _ -> raise (Invalid_argument "Negation should have one argument")
      else if app.apply_head = impl_id then
        Solver.mk_impl rd.t_solver tail_terms
      else if app.apply_head = and_id then
        Solver.mk_and rd.t_solver tail_terms
      else if app.apply_head = or_id then
        Solver.mk_or rd.t_solver tail_terms
      else if app.apply_head = xor_id then
        Solver.mk_xor rd.t_solver tail_terms
      else if app.apply_head = eq_id then
        Solver.mk_eq rd.t_solver tail_terms
      else if app.apply_head = distinct_id then
        (* BUG: Distinct in Z3 requires that argument sorts are the same, but the reader can mix Int and Real. *)
        Solver.mk_distinct rd.t_solver tail_terms
      (* This is now done by a solver's to_nnf.        
        if List.for_all (Solver.is_bool_term rd.t_solver) tail_terms then
          OperationsU.pairwise_of_binop
            (Solver.mk_and rd.t_solver)
            (fun t1 t2 -> Solver.mk_xor rd.t_solver [t1; t2])
            tail_terms
        else if List.for_all (Solver.is_numeric_term rd.t_solver) tail_terms then
          OperationsU.pairwise_of_binop
            (Solver.mk_and rd.t_solver)
            (fun t1 t2 -> Solver.mk_or rd.t_solver [
              Solver.mk_lt rd.t_solver [t1; t2];
              Solver.mk_gt rd.t_solver [t1; t2]])
            tail_terms
        else
          not_supported "Only boolean and numeric distinct are supported."
      end *)
      else if app.apply_head = ite_id then
        match tail_terms with 
        | [ tail_t1; tail_t2; tail_t3] ->
            Solver.mk_ite rd.t_solver tail_t1 tail_t2 tail_t3
        | _ -> raise (Invalid_argument "Ite should have three arguments")
      else if app.apply_head = minus_id then
        match tail_terms with
        | [] -> raise (Invalid_argument "Minus should have at least one argument")
        | [tail_term] -> Solver.mk_unary_minus rd.t_solver tail_term
        | _ -> Solver.mk_minus rd.t_solver tail_terms
      else if app.apply_head = plus_id then
        Solver.mk_plus rd.t_solver tail_terms
      else if app.apply_head = mult_id then
        Solver.mk_mult rd.t_solver tail_terms
      else if app.apply_head = leq_id then
        Solver.mk_leq rd.t_solver tail_terms
      else if app.apply_head = lt_id then
        Solver.mk_lt rd.t_solver tail_terms
      else if app.apply_head = geq_id then
        Solver.mk_geq rd.t_solver tail_terms
      else if app.apply_head = gt_id then
        Solver.mk_gt rd.t_solver tail_terms
      else begin
        try
          Solver.mk_apply
            rd.t_solver
            (IdentHashtbl.find rd.t_func_decls app.apply_head)
            tail_terms
        with
        | Not_found -> raise (Not_supported
            (asprintf
              "Unsupported interpreted predicate: %a"
              Smt2.format_ident
              app.apply_head))
      end
    | LetT { let_binds; let_body } ->
        let body_env = List.fold_left
            (fun e bind -> IdentMap.add
              (mk_sym_ident bind.bind_name)
              (mk_term_smt2_env rd let_env bind.bind_term)
              e)
            let_env
            let_binds
        in
        mk_term_smt2_env rd body_env let_body
    | ForallT _ | ExistsT _ -> raise (Not_supported "Quantifiers")

  let mk_term_smt2 rd term =
    mk_term_smt2_env rd IdentMap.empty term

  let mk_goal_term rd term =
    ClauseBuilder.make_goal_term rd.t_builder term

  let read_command rd cmd =
    match cmd with
    | DeclareRelCmd rel ->
      let fn = fun_of_rel rel in
      let func_decl = mk_func_decl_smt2 rd fn in
      let _ = ClauseBuilder.make_pred rd.t_builder func_decl in
      ()
    | DeclareVarCmd var ->
      let fn = fun_of_var var in
      let _ = mk_func_decl_smt2 rd fn in
      ()
    | RuleCmd smt2_term ->
        let solver_term = mk_term_smt2 rd smt2_term in
        let _ = read_clause rd solver_term in
        ()
    | QueryCmd smt2_term ->
        (* NOTE: Query should a be an application of an uninterpreted predicate. *)
        let solver_term = mk_term_smt2 rd smt2_term in
        mk_goal_term rd solver_term
    | _ -> ()

  let read_lexbuf rd lexbuf =
    let rec loop () =
      match Smt2U.parse_sea_command lexbuf with      
      | None -> ()
      | Some cmd ->
          read_command rd cmd;
          loop ()
    in
    loop ()

end