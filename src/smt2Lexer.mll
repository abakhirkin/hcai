{
  open Batteries.Lexing
  open Smt2Parser

  exception Error of string

  type lexer_args_t = {
    args_sea: bool
  }

  let unquote_symbol symbol =
    String.sub symbol 1 ((String.length symbol) - 2)
  
  let remove_colon symbol =
    String.sub symbol 1 ((String.length symbol) - 1)
}

(* The standard instead defines a single lexeme white_space_char,
 * but our definition allows to count lines in the lexer. *)
let whitespace = ['\t' ' ']+
let newline = "\r\n" | '\n' | '\r'
let printable_or_whitespace = ['\t' '\n' '\r' '\032'-'\126' '\128'-'\255']
let digit = ['0'-'9']
let letter = ['a'-'z' 'A'-'Z']
let numeral = '0' | ['1'-'9'] digit*
let decimal = numeral '.' '0'* numeral
(* Characters that are allowed in symbols in addition to letters and digits. *)
let simple_sym_char = ['~' '!' '@' '$' '%' '^' '&' '*' '_' '-' '+' '=' '<' '>' '.' '?' '/']
let simple_symbol = (letter | digit | simple_sym_char)+
let quoted_symbol =
  "|" (printable_or_whitespace # ['|' '\\'])* "|"
let string_char = printable_or_whitespace # ['"']
let keyword = ':' simple_symbol
let comment = ';' ([^ '\r' '\n']*)

rule read args =
  parse
  | whitespace { read args lexbuf }
  | newline { new_line lexbuf; read args lexbuf }
  | comment { read args lexbuf }
  | "(" { LP }
  | ")" { RP }
  | "_" { UNDER }
  | "let" { LET }
  | "forall" { FORALL }
  | "exists" { EXISTS }
  | "assert" { ASSERT }
  | "set-info" { SET_INFO }
  | "declare-rel"
    { if args.args_sea then DECLARE_REL
      else SYMBOL (Smt2.mk_simple_symbol (lexeme lexbuf)) }
  | "declare-var"
    { if args.args_sea then DECLARE_VAR
      else SYMBOL (Smt2.mk_simple_symbol (lexeme lexbuf)) }
  | "rule"
    { if args.args_sea then RULE
      else SYMBOL (Smt2.mk_simple_symbol (lexeme lexbuf)) }
  | "query"
    { if args.args_sea then QUERY
      else SYMBOL (Smt2.mk_simple_symbol (lexeme lexbuf)) }
    (* TODO: Support decimal numerals. Num.num_of_string only works with integers *)
  | numeral { NUMERAL (Num.num_of_string (lexeme lexbuf)) }
  | simple_symbol { SYMBOL (Smt2.mk_simple_symbol (lexeme lexbuf)) }
  | quoted_symbol { SYMBOL (Smt2.mk_quoted_symbol (unquote_symbol (lexeme lexbuf))) }
  | '"' { STRING (read_string (Buffer.create 16) lexbuf) }
  | keyword { KEYWORD (Smt2.mk_simple_symbol (remove_colon (lexeme lexbuf))) }
  | _ { raise (Error ("Unexpected lexeme: " ^ lexeme lexbuf)) }
  | eof { EOF }

and read_string buf =
  parse
  | "\"\"" { Buffer.add_char buf '"'; read_string buf lexbuf }
  | '"' { Buffer.contents buf }
  | string_char+ { Buffer.add_string buf (Lexing.lexeme lexbuf); read_string buf lexbuf }

{
  let mk_lexer = read {args_sea = false}

  let mk_sea_lexer = read {args_sea = true}
}
