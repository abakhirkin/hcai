module type T = sig
  type elt
  type t
  
  val make: unit -> t

  val add: t -> elt -> unit

  val remove: t -> elt -> unit

  val max_elt: t -> elt
end

module Make(Ord: Set.OrderedType) = struct
  
  module Set = Set.Make(Ord)

  type elt = Set.elt

  type t = {
    mutable t_set: Set.t
  }

  let make () =
    { t_set = Set.empty }

  let add set elem =
    set.t_set <- Set.add elem set.t_set

  let remove set elem =
    set.t_set <- Set.remove elem set.t_set  

  let max_elt set =
    Set.max_elt set.t_set
end