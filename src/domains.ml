open Format

module type T = sig
  module Solver: Solvers.T

  type domain_t

  type space_t

  type elem_t

  (* TODO: Perhaps, domain should have a mutable API as well, like Bpoly.Builder. *)

  (* TODO: Perhaps, elem_t should be assumed to have a reference to its space_t. *)

  val domain_solver: domain_t -> Solver.t

  val make_domain: Solver.t -> domain_t

  val make_space: domain_t -> Solver.func_decl_t list -> space_t

  val space_union: space_t -> space_t -> space_t

  val mk_bot: space_t -> elem_t
  
  val mk_top: space_t -> elem_t

  val join: space_t -> elem_t -> elem_t -> elem_t
  
  val widen: space_t -> elem_t -> elem_t -> elem_t
  
  val meet: space_t -> elem_t -> elem_t -> elem_t

  val leq: space_t -> elem_t -> elem_t -> bool
  
  val equal: space_t -> elem_t -> elem_t -> bool

  val format_space: formatter -> space_t -> unit
  
  val move_project: space_t -> elem_t -> space_t -> elem_t

  val move_rename_array: space_t -> elem_t ->  (Solver.func_decl_t * Solver.func_decl_t ) array -> space_t -> elem_t  

  val term_of_elem: space_t -> elem_t -> Solver.term_t

  (* TODO: Perhaps this should go to path focusing module, and domain should provide
     basic operations to allow to implement this. *)
  val elem_of_expl_terms: space_t -> Solver.term_t list -> elem_t

  val format_elem: space_t -> formatter -> elem_t -> unit

  val to_string: space_t -> elem_t -> string

  val is_bot: space_t -> elem_t -> bool

end