open Ocamlbuild_plugin

let () =
  dispatch
    (function
    | Before_options ->
        Options.use_ocamlfind := true;
        Options.use_menhir := true

    | After_rules ->
        Pathname.define_context "src/bpoly/test" ["src/bpoly/test"; "src/bpoly"]

    | _ -> ())