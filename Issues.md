Issues
======

Non-critical and hogh-level issues in no particular order.

Parser
------

- Syntax and semantic errors in the input are not well reported.

- Dependency on `num` should be removed. Should use strings or Zarith to store
  numbers.

Clause Set
----------

- Perhaps, dependency on `ocamlgraph` should be removed, and the `ClauseSet.t`
  itself should be used as a graph.

Artihmetic
----------

- No support for division.

- No support for fractional constants.

Bddapron
--------

- Bddapron cannot compile with safe strings, which are the default starting with
  OCaml 4.06.

Bpoly domain
------------

- Current implementation of convex hull produces closed polyhedra.
  Need to track Farkas coefficients during projection that is done as part of
  convex hull and based on them, make some resulting constraints open.
  An issue with that approach: a constraint that would be made open in the end
  may be redundant w.r.t. closed constraints after an intermediate projection.

- Need to have separately integer and rational variables. 

- Constraints should be totally ordered in some way. Currently, we only
  guarantee that constraints with larger coefficients are removed first.

- Need to try to use ordered arrays instead of hashtables to store sparse
  vectors.

- SMT-based removal of redundant constraints is slow, and many rounds of
  elimination are useless.

- Post-condition operators are not well tested, since the path focsusing
  computation does not use them.

Libraries
---------

- The `oclock` package that is used to measure time (and does it more precisely
  than `Unix.gettimeofday`) causes a compilation warning, since it does not
  install its `.cmx` file in the library folder.