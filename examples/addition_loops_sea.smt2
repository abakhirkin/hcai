; #include "seahorn/seahorn.h"
;
; int nd();
;
; int main() {
;   int x = 0, y = nd();
;   while (1) {
;     if (nd()) break;
;     x += y;
;   }
;   if (x > 0) {
;     while (1) {
;       if (nd()) break;
;       y += x;
;     }
;     sassert (y >= 0);
;   }
; }
(set-info :original "/tmp/sea-pqN5Aa/addition_loops.pp.ms.o.bc")
(set-info :authors "SeaHorn v.0.1.0-rc3")
(declare-rel verifier.error (Bool Bool Bool ))
(declare-rel main@entry (Int ))
(declare-rel main@_bb (Int Int Int ))
(declare-rel main@_bb2 (Int Int Int ))
(declare-rel main@verifier.error.split ())
(declare-var main@%_14_0 Bool )
(declare-var main@%_10_0 Int )
(declare-var main@%_11_0 Int )
(declare-var main@%_12_0 Bool )
(declare-var main@%y.0.i.lcssa_1 Int )
(declare-var main@%_8_0 Bool )
(declare-var main@%_3_0 Int )
(declare-var main@%_4_0 Int )
(declare-var main@%_5_0 Bool )
(declare-var main@%_0_0 Int )
(declare-var @nd_0 Int )
(declare-var main@entry_0 Bool )
(declare-var main@%_1_0 Int )
(declare-var main@_bb_0 Bool )
(declare-var main@%x.0.i_0 Int )
(declare-var main@%x.0.i_1 Int )
(declare-var main@%_6_0 Int )
(declare-var main@_bb_1 Bool )
(declare-var main@%x.0.i_2 Int )
(declare-var main@_bb1_0 Bool )
(declare-var main@%x.0.i.lcssa_0 Int )
(declare-var main@%x.0.i.lcssa_1 Int )
(declare-var main@_bb2_0 Bool )
(declare-var main@%y.0.i_0 Int )
(declare-var main@%y.0.i_1 Int )
(declare-var main@%_13_0 Int )
(declare-var main@_bb2_1 Bool )
(declare-var main@%y.0.i_2 Int )
(declare-var main@verifier.error_0 Bool )
(declare-var main@%y.0.i.lcssa_0 Int )
(declare-var main@verifier.error.split_0 Bool )
(rule (verifier.error false false false))
(rule (verifier.error false true true))
(rule (verifier.error true false true))
(rule (verifier.error true true true))
(rule (main@entry @nd_0))
(rule (=> (and (main@entry @nd_0)
         true
         (= main@%_0_0 @nd_0)
         (=> main@_bb_0 (and main@_bb_0 main@entry_0))
         main@_bb_0
         (=> (and main@_bb_0 main@entry_0) (= main@%x.0.i_0 0))
         (=> (and main@_bb_0 main@entry_0) (= main@%x.0.i_1 main@%x.0.i_0)))
    (main@_bb @nd_0 main@%_1_0 main@%x.0.i_1)))
(rule (=> (and (main@_bb @nd_0 main@%_1_0 main@%x.0.i_0)
         true
         (= main@%_3_0 @nd_0)
         (= main@%_5_0 (= main@%_4_0 0))
         (= main@%_6_0 (+ main@%x.0.i_0 main@%_1_0))
         (=> main@_bb_1 (and main@_bb_1 main@_bb_0))
         main@_bb_1
         (=> (and main@_bb_1 main@_bb_0) main@%_5_0)
         (=> (and main@_bb_1 main@_bb_0) (= main@%x.0.i_1 main@%_6_0))
         (=> (and main@_bb_1 main@_bb_0) (= main@%x.0.i_2 main@%x.0.i_1)))
    (main@_bb @nd_0 main@%_1_0 main@%x.0.i_2)))
(rule (let ((a!1 (and (main@_bb @nd_0 main@%_1_0 main@%x.0.i_0)
                true
                (= main@%_3_0 @nd_0)
                (= main@%_5_0 (= main@%_4_0 0))
                (= main@%_6_0 (+ main@%x.0.i_0 main@%_1_0))
                (=> main@_bb1_0 (and main@_bb1_0 main@_bb_0))
                (=> (and main@_bb1_0 main@_bb_0) (not main@%_5_0))
                (=> (and main@_bb1_0 main@_bb_0)
                    (= main@%x.0.i.lcssa_0 main@%x.0.i_0))
                (=> (and main@_bb1_0 main@_bb_0)
                    (= main@%x.0.i.lcssa_1 main@%x.0.i.lcssa_0))
                (=> main@_bb1_0 (= main@%_8_0 (> main@%x.0.i.lcssa_1 0)))
                (=> main@_bb1_0 main@%_8_0)
                (=> main@_bb2_0 (and main@_bb2_0 main@_bb1_0))
                main@_bb2_0
                (=> (and main@_bb2_0 main@_bb1_0) (= main@%y.0.i_0 main@%_1_0))
                (=> (and main@_bb2_0 main@_bb1_0)
                    (= main@%y.0.i_1 main@%y.0.i_0)))))
  (=> a!1 (main@_bb2 @nd_0 main@%y.0.i_1 main@%x.0.i.lcssa_1))))
(rule (=> (and (main@_bb2 @nd_0 main@%y.0.i_0 main@%x.0.i.lcssa_0)
         true
         (= main@%_10_0 @nd_0)
         (= main@%_12_0 (= main@%_11_0 0))
         (= main@%_13_0 (+ main@%y.0.i_0 main@%x.0.i.lcssa_0))
         (=> main@_bb2_1 (and main@_bb2_1 main@_bb2_0))
         main@_bb2_1
         (=> (and main@_bb2_1 main@_bb2_0) main@%_12_0)
         (=> (and main@_bb2_1 main@_bb2_0) (= main@%y.0.i_1 main@%_13_0))
         (=> (and main@_bb2_1 main@_bb2_0) (= main@%y.0.i_2 main@%y.0.i_1)))
    (main@_bb2 @nd_0 main@%y.0.i_2 main@%x.0.i.lcssa_0)))
(rule (let ((a!1 (and (main@_bb2 @nd_0 main@%y.0.i_0 main@%x.0.i.lcssa_0)
                true
                (= main@%_10_0 @nd_0)
                (= main@%_12_0 (= main@%_11_0 0))
                (= main@%_13_0 (+ main@%y.0.i_0 main@%x.0.i.lcssa_0))
                (=> main@verifier.error_0
                    (and main@verifier.error_0 main@_bb2_0))
                (=> (and main@verifier.error_0 main@_bb2_0) (not main@%_12_0))
                (=> (and main@verifier.error_0 main@_bb2_0)
                    (= main@%y.0.i.lcssa_0 main@%y.0.i_0))
                (=> (and main@verifier.error_0 main@_bb2_0)
                    (= main@%y.0.i.lcssa_1 main@%y.0.i.lcssa_0))
                (=> main@verifier.error_0
                    (= main@%_14_0 (> main@%y.0.i.lcssa_1 (- 1))))
                (=> main@verifier.error_0 (not main@%_14_0))
                (=> main@verifier.error.split_0
                    (and main@verifier.error.split_0 main@verifier.error_0))
                main@verifier.error.split_0)))
  (=> a!1 main@verifier.error.split)))
(query main@verifier.error.split)

