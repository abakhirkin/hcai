(declare-rel P1 (Int Int))
(declare-rel P2 (Int Int))
(declare-rel E ())
(declare-var x Int)
(declare-var y Int)

(rule (=> (= x 0) (P1 x y)))
(rule (=> (P1 x y) (P1 (+ x y) y)))
(rule (=> (and (P1 x y) (> x 0)) (P2 x y)))
(rule (=> (P2 x y) (P2 x (+ y x))))
(rule (=> (and (P2 x y) (< y 0)) E))

(query E)

