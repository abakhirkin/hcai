(declare-rel P (Int Int))
(declare-var x Int)
(declare-var y Int)

(rule (P 0 0))
(rule (=>
  (and (P x y) (< x 100))
  (P (+ x 1) (+ y 1))))

(query (and (P x y) (distinct x y)))
