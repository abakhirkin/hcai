(declare-rel init (Int Int Int Int Int)) ; m n x y a[x,y]
(declare-rel loopi (Int Int Int Int Int Int)) ; m n i x y a[x,y]
(declare-rel loopj (Int Int Int Int Int Int Int)) ; m n i j x y a[x,y]
(declare-rel write (Int Int Int Int Int Int Int)) ; m n i j x y a[x,y]
(declare-rel incrj (Int Int Int Int Int Int Int)) ; m n i j x y a[x,y]
(declare-rel incri (Int Int Int Int Int Int)) ; m n i x y a[x,y]
(declare-rel end (Int Int Int Int Int)) ; m n x y a[x,y]
(declare-rel error ())

(declare-var m Int)
(declare-var n Int)
(declare-var x Int)
(declare-var y Int)
(declare-var i Int)
(declare-var j Int)
(declare-var axy Int)
(declare-var aij Int)

(rule
  (=> (and (<= 0 x) (< x m) (<= 0 y) (< y n)) (init m n x y axy)))

(rule
  (=> (init m n x y axy) (loopi m n 0 x y axy)))

(rule
  (=> (and (< i m) (loopi m n i x y axy))
      (loopj m n i 0 x y axy)))

(rule
  (=> (and (>= i m) (loopi m n i x y axy))
      (end m n x y axy)))

(rule
  (=> (and (< j n) (loopj m n i j x y axy))
      (write m n i j x y axy)))

(rule
  (=> (and (>= j n) (loopj m n i j x y axy))
      (incri m n i x y axy)))

(rule
  (=> (and (write m n i j x y axy)
           (not (and (= i x) (= j y))))
      (incrj m n i j x y axy)))

(rule
  (=> (write m n i j i j aij)
      (incrj m n i j i j 42)))

(rule
  (=> (incrj m n i j x y axy)
      (loopj m n i (+ j 1) x y axy)))

(rule
  (=> (incri m n i x y axy)
      (loopi m n (+ i 1) x y axy)))

(rule
  (=> (and (end m n x y axy) (distinct axy 42)) error))

(query error)

