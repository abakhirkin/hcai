(declare-rel init (Int Int Int Int Int Bool Bool)) ; n k a[k] l a0[l]
(declare-rel read1 (Int Int Int Int Int Int Bool Bool)) ; n i k a[k] l a0[l]
(declare-rel read2 (Int Int Int Int Int Int Int Bool Bool Bool Bool)) ; n i tmp1 k a[k] l a0[l]
(declare-rel write1 (Int Int Int Int Int Int Int Int Bool Bool Bool Bool)) ; n i tmp1 tmp2 k a[k] l a0[l]
(declare-rel write2 (Int Int Int Int Int Int Int Bool Bool Bool Bool Bool Bool)) ; n i tmp1 k a[k] l a0[l]
(declare-rel end (Int Int Int Int Int Int Bool Bool Bool Bool Bool Bool Bool Bool)) ; n k a[k] l a0[l]
(declare-rel error ())

(declare-var n Int)
(declare-var i Int)
(declare-var j Int)
(declare-var k Int)
(declare-var ak Int)
(declare-var l Int)
(declare-var a0l Int)
(declare-var tmp1 Int)
(declare-var tmp2 Int)

(declare-var b0 Bool)
(declare-var b0_0 Bool)
(declare-var b0_1 Bool)

(declare-var b1 Bool)
(declare-var b1_0 Bool)
(declare-var b1_1 Bool)

(declare-var b2 Bool)
(declare-var b2_0 Bool)
(declare-var b2_1 Bool)

(declare-var b3 Bool)
(declare-var b3_0 Bool)
(declare-var b3_1 Bool)

(declare-var b4 Bool)
(declare-var b4_0 Bool)
(declare-var b4_1 Bool)

(declare-var b5 Bool)
(declare-var b5_0 Bool)
(declare-var b5_1 Bool)

(declare-var b6 Bool)
(declare-var b6_0 Bool)
(declare-var b6_1 Bool)

(declare-var b7 Bool)
(declare-var b7_0 Bool)
(declare-var b7_1 Bool)

(rule
  (=> (and (<= 0 k) (< k n))
      (init n k ak k ak true b1)))

(rule
  (=> (and (<= 0 k) (< k n) (<= 0 l) (< l n) (distinct k l))
      (init n k ak l a0l false (< k l))))

(rule
  (let ((j (- n (+ i 1))))
    ; Not sure if should be i<j or i<=j
    (=> (and (>= i 0) (< i j) (init n k ak l a0l b0 b1))
        (read1 n i k ak l a0l b0 b1))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (distinct i k)
             (read1 n i k ak l a0l b0_0 b1_0)
             (read1 n i i tmp1 l a0l b0_1 b1_1))
        (read2 n i tmp1 k ak l a0l b0_0 b1_0 b0_1 b1_1))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (read1 n i i tmp1 l a0l b0 b1)
        (read2 n i tmp1 i tmp1 l a0l b0 b1 b0 b1))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (distinct j k)
             ; Same b2 and b3 because they relate i and l.
             (read2 n i tmp1 k ak l a0l b0_0 b1_0 b2 b3)
             (read2 n i tmp1 j tmp2 l a0l b0_1 b1_1 b2 b3))
        (write1 n i tmp1 tmp2 k ak l a0l b2 b3 b0_1 b1_1))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (read2 n i tmp1 j tmp2 l a0l b0 b1 b2 b3))
        (write1 n i tmp1 tmp2 j tmp2 l a0l b2 b3 b0 b1))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (write1 n i tmp1 tmp2 k ak l a0l b0 b1 b2 b3) (distinct i k))
        (write2 n i tmp1 k ak l a0l b0 b1 b2 b3 false (< i k)))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (write1 n i tmp1 tmp2 i ak l a0l b0 b1 b2 b3)
        (write2 n i tmp1 i tmp2 l a0l b0 b1 b2 b3 true b5))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (write2 n i tmp1 k ak l a0l b0 b1 b2 b3 b4 b5) (distinct j k))
        (end n i k ak l a0l b0 b1 b2 b3 b4 b5 false (< j k)))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (write2 n i tmp1 j ak l a0l b0 b1 b2 b3 b4 b5)
        (end n i j tmp1 l a0l b0 b1 b2 b3 b4 b5 true b7))))

(rule
  (let ((j (- n (+ i 1))))
    (=> (and (>= i 0) (< i n) (end n i i ak j a0l b0 b1 b2 b3 b4 b5 b6 b7) (distinct ak a0l)) error)))

(query error)

;(assert (forall ((n Int) (i Int) (ai Int) (j Int) (a0j Int))
;  (not (end n i ai j a0j))))
 
