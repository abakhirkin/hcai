(declare-rel init (Int Int Int Int Int)) ; m n x y a[x,y]
(declare-rel loopi (Int Int Int Int Int Int Bool)) ; m n i x y a[x,y]
(declare-rel loopj (Int Int Int Int Int Int Int Bool Bool)) ; m n i j x y a[x,y]
(declare-rel write (Int Int Int Int Int Int Int Bool Bool)) ; m n i j x y a[x,y]
(declare-rel incrj (Int Int Int Int Int Int Int Bool Bool)) ; m n i j x y a[x,y]
(declare-rel incri (Int Int Int Int Int Int Bool)) ; m n i x y a[x,y]
(declare-rel end (Int Int Int Int Int Bool)) ; m n x y a[x,y]
(declare-rel error ())

(declare-var m Int)
(declare-var n Int)
(declare-var x Int)
(declare-var y Int)
(declare-var i Int)
(declare-var j Int)
(declare-var axy Int)
(declare-var aij Int)

(declare-var bi Bool)
(declare-var bj Bool)

(rule
  (=> (and (<= 0 x) (< x m) (<= 0 y) (< y n)) (init m n x y axy)))

(rule
  (=> (init m n x y axy) (loopi m n 0 x y axy false)))

(rule
  (=> (and (< i m) (loopi m n i x y axy bi))
      (loopj m n i 0 x y axy bi false)))

(rule
  (=> (and (>= i m) (loopi m n i x y axy bi))
      (end m n x y axy bi)))

(rule
  (=> (and (< j n) (loopj m n i j x y axy bi bj))
      (write m n i j x y axy bi bj)))

(rule
  (=> (and (>= j n) (loopj m n i j x y axy bi bj))
      (incri m n i x y axy bi)))

(rule
  (=> (and (write m n i j x y axy bi bj)
           (and (not (= i x)) (not (= j y))))
      (incrj m n i j x y axy bi bj)))

(rule
  (=> (and (write m n i j x y axy bi bj)
           (and (= i x) (not (= j y))))
      (incrj m n i j x y axy bi bj)))
(rule
  (=> (and (write m n i j x y axy bi bj)
           (and (not (= i x)) (= j y)))
      (incrj m n i j x y axy bi true)))

(rule
  (=> (and (write m n i j x y aij bi bj) (= x i) (= j y))
      (incrj m n i j x y 42 true bj)))

(rule
  (=> (incrj m n i j x y axy bi bj)
      (loopj m n i (+ j 1) x y axy bi bj)))

(rule
  (=> (incri m n i x y axy bi)
      (loopi m n (+ i 1) x y axy bi)))

(rule
  (=> (and (end m n x y axy bi) (not (= axy 42))) error))

(query error)

