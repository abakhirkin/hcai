; A version of addition_loops.smt2 with transitions written in a different way.
; Instead of (=> (P1 x y) (P1 (+ x y) y)),
; we write (=> (P1 (- x y) y) (P1 x y))
(declare-rel P1 (Int Int))
(declare-rel P2 (Int Int))
(declare-rel E ())
(declare-var x Int)
(declare-var y Int)

(rule (=> (= x 0) (P1 x y)))
(rule (=> (P1 (- x y) y) (P1 x y)))
(rule (=> (and (P1 x y) (> x 0)) (P2 x y)))
(rule (=> (P2 x (- y x)) (P2 x y)))
(rule (=> (and (P2 x y) (< y 0)) E))

(query E)

