A Path Focusing Abstract Interpreter for Horn Clauses
=====================================================

Requirements
------------

Linux, OCaml 4.02 or 4.04 (required by Bddapron 2.2.4), Opam.
If you're not sure how to install OCaml and Opam, see section [How to Install 
OCaml and Opam](#how-to-install-ocaml-and-opam) below. 

Setup
-----

1. Install native dependencies (required by Apron):
  - GMP (`libgmp-dev` for Debian, `gmp-devel` for Fedora),
  - MPFR (`libmpfr-dev` for Debian, `mpfr-dev` for Fedora).

2. Install dependencies via Opam:
   Make sure to pin Bddapron to version 2.2.4, later versions will not work.
```bash
opam pin bddapron 2.2.4
opam install ocamlbuild ocamlfind batteries menhir ocamlgraph bddapron zarith mlcuddidl ounit oclock num
```
The package `mlcuddidl` carries its own version of CUDD, no need to install CUDD
separately.

3. Install Z3 4.5.0 with ML interface. Avoid other versions, they may not work.
   Before building Z3, make sure you install OCaml and Ocamlfind, since they are
   required to build the ML interface. Other packages like Bddapron, you can
   install while Z3 is compiling.
```bash
git clone https://github.com/Z3Prover/z3.git
cd z3
git checkout z3-4.5.0
python scripts/mk_make.py --prefix=<PREFIX> --ml
cd build
make
make install
```
Make sure that the system can load shared libraries from `<PREFIX>/lib`.
You may need to add it to the linker configuration (e.g., `/etc/ld.so.conf.d`,
`$LD_LIBRARY_PATH`, etc).

4. Go to the tool folder, build the native binary and tests:
```bash
./build.ml
```
Warning 58 for `Oclock` module can be ignored.

5. Run tests:
```bash
./tests.native
```

Basic Usage
-----------

See [Usage](Usage.md).

Known Issues
------------

- [Bugs](Bugs.md)
- [High-level or benign issues](Issues.md)

References
----------

1. A. Bakhirkin, D. Monniaux.
   Combining Forward and Backward Abstract Interpretation of Horn Clauses.
   SAS 2017. [[HAL]](https://hal.archives-ouvertes.fr/hal-01551447)

   Please cite this paper if you use the tool in your research.

2. A. Bakhirkin, D. Monniaux
   Extending Constraint-Only Representation of Polyhedra with Boolean Constraints.
   SAS 2018. [[HAL]](https://hal.archives-ouvertes.fr/hal-01841837)

How to Install OCaml and Opam
-----------------------------

We suggest you do not install OCaml from your distro's repository and use Opam 
instead. You will need Opam anyway. If you are not sure how to install Opam, do 
the following

1. Install gcc, g++, make, m4, patch, and unzip.

2. Go to https://github.com/ocaml/opam/releases/latest and download the binary 
   for your system.

3. Rename the binary to `opam`, make it executable, and put it somewhere in the 
   `$PATH`.

4. Run `opam init --comp 4.04.0`. This will install a
   suitable version of OCaml 4.04 for the current user. When prompted, allow the
   script to modify the config files. After it finishes, run
   ``eval $(opam env)`` as suggested.

5. If you were previously using a version of OCaml other than 4.02 or 4.04, you
   will need to install a new switch with `opam switch create 4.04.0`.
   If you previously compiled and installed the ML interface of Z3 while
   using a different compiler, you will need to re-compile and re-install it.
   
   
