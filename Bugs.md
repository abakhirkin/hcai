Bugs
====

Bugs and serious issues in no particular order.

- Reading a file that contains a `distinct` predicate fails when the arguments
  of `distinct` have different sorts (e.g., `Real` and `Int`). As a workaround,
  use `(not (=` instead.

- Recent versions of Z3 (including 4.6.0 release) timeout on queries resuting
  from Seahorn-translated programs. Does not seem to appear with 4.5.0.

- Apparently we use Bddapron API incorrectly when renaming variables.

- Segfault in Cudd for `swap_single` example.